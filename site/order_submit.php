<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>订单 -品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/backet.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/order_submit.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container" data-modify="<?php echo $result['modify'];?>">
    <div class="wrapper">
        <div class="progress_box">
            <div class="progress progress_2">
                <div class="step-1">
                    <span>1.我的购物车</span>
                </div>
                <div class="step-2 complete">
                    <span>2.填写核对订单信息</span>
                </div>
                <div class="step-3">
                    <span>3.成功提交订单</span>
                </div>
                <div class="step-4">
                    <span>4评价订单</span>
                </div>
            </div>
        </div>
    </div>
    <div id="order">
        <div class="wrapper">
            <div id="goods">
                <h4>核对订单</h4>
                <table>
                    <thead>
                    <tr>
                        <th class="goods_name">商品名称</th>
                        <th class="goods_price">单价</th>
                        <th class="goods_quatity">数量</th>
                        <th class="subtotal">小计</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td colspan="4">
                            <div class="add_price">
                                商品总价<span class="goods_origin_price"><?php echo $result['order_amount'];?></span>元 +
                                运费<span class="trans_price"><?php echo $result['freight'];?></span>元
                                等等等等 = 总价 ￥
                                <span class="total_price">
                                    <?php echo $result['order_amount']+$result['freight'];?>
                                </span>元
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php foreach ($result['goods_in_order'] as $key => $value) {?>
                        <tr>
                            <td><?php echo $value['name'];?>(<?php echo $value['attribute_name'];?>)</td>
                            <td><?php echo $value['selling_price'];?></td>
                            <td><?php echo $value['number'];?></td>
                            <td><?php echo $value['selling_price']*$value['number'];?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <form id="order_form" method="POST" action="<?php e_page('order','complete');?>" >
                <div id="address" data-address="<?php echo $result['address_id'];?>">
                    <h4>收货地址</h4>
                    <div class="normal">
                        <input type="radio" name="address" class="choose" value="0" />
                        <input type="hidden" name="id" id="addressId" class="id"/>
                        <span class="receiver"></span>
                        <span class="province"></span>
                        <span class="city"></span>
                        <span class="district"></span>
                        <span class="street"></span>
                        <span class="postal_code"></span>
                        <span class="mobile_phone"></span>
                        <span class="fixed_phone"></span>
                        <a class="default" href="javascript:">设为默认地址</a>
                        <a class="modify" href="javascript:">编辑</a>
                        <a class="delete" href="javascript:">删除</a>
                        <div class="modify">
                            <input type="hidden" name="id" id="idInput" class="id"/>
                            <label for="receiverInput"><span>*</span>收件人</label>
                            <input type="text" name="receiver" id="receiverInput" class="receiver" placeholder="请填写收件人"/>
                            <br/>
                            <label><span>*</span>收货区域</label>
                            <select name="province" id="provinceInput" class="province"></select>
                            <select name="city" id="cityInput" class="city"></select>
                            <select name="district" id="districtInput" class="district"></select>
                            <br/>
                            <label for="streetInput"><span>*</span>详细地址</label>
                            <input type="text" name="street" id="streetInput" class="street" placeholder="请填写详细地址"/>
                            <br/>
                            <label for="postal_codeInput"><span>*</span>邮编号码</label>
                            <input type="text" name="postal_code" id="postal_codeInput" class="postal_code" placeholder="请填写邮编号码"/>
                            <br/>
                            <label for="mobile_phoneInput"><span>*</span>手机</label>
                            <input type="text" name="mobile_phone" id="mobile_phoneInput" class="mobile_phone" placeholder="请填写手机"/>
                            <span>或</span>
                            <label for="fix_phoneInput"><span>*</span>固定电话</label>
                            <input type="text" name="fixed_phone" id="fixed_phoneInput" class="fixed_phone" placeholder="请填写固定电话"/>
                            <a class="modifyConfirm button" href="javascript:">确认收货地址</a>
                        </div>
                    </div>
                    <div id="currentAddress"></div>
                    <div class="add">
                        <a class="new" href="javascript:">新增地址</a>
                    </div>
                    <div id="remarkBox">
                        <label for="remarkInput"><span></span>订单备注</label>
                        <textarea type="text" name="remark" id="remarkInput" class="remark" placeholder="请填写订单备注"><?php echo $result['remark'];?></textarea>
                    </div>
                </div>
                <div id="pay">
                    <h4>支付方式</h4>
                    <input type="radio" name="pay_method" id="pay_method_cash" value="cash" checked/>
                    <label for="pay_method_cash">货到付款</label>
                    <input type="radio" name="pay_method" id="pay_method_alipay" value="alipay" disabled/>
                    <label for="pay_method_alipay" class="disabled">支付宝付款</label>
                </div>
                <div class="sub_order">
                    <input type="submit" name="pay" value="确认支付"/>
                    <input type="hidden" name="order_id" value="<?php echo $result['id']; ?>" />
                </div>
            </form>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>	
</body>
</html>
