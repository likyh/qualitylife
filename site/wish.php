<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>愿望单 -品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/backet.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/wish.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <div id="having_choose">
    	<div class="wrapper">
            <form action="<?php e_page("order","create");?>" method="post" id="buy_form">
            <div class="title">
                <div class="choose_all_box">
                    <input type="checkbox" id="cart_choose_all_first" class="cart_choose_all">
                    <label for="cart_choose_all_first">全选</label>
                </div>
                <div class="name_box">商品名称</div>
                <div class="price_box">单价</div>
                <!--<div class="amount_box">数量</div>-->
                <!--<div class="total_box">小计</div>-->
            </div>
            <div class="choosed_product_box">
                <div class="choosed_product">
                    <div class="choose">
                        <input type="checkbox" name="choose[]" value="true">
                    </div>
                    <div class="image">
                        <img src="#">
                    </div>
                    <div class="name_box">
                        <a href="#">
                            <span class="full_name">
                                婴幼儿玩具————洗澡必备游艇鸭子恐龙企鹅
                            </span>
                        </a>
                    </div>
                    <div class="price_box">
                        <span class="origin_price">￥<span>99.00</span></span>
                        <span class="selling_price">￥<span>49.00</span></span>
                    </div>
                    <div class="operate_box">
                        <a href="#" class="delete">删除</a>
                    </div>
                </div>
            </div>
            <div class="last_row">
                <div class="change_product">
                    <div class="choose_all_box">
                        <input type="checkbox" id="cart_choose_all_end" class="cart_choose_all">
                        <label for="cart_choose_all_end">全选</label>
                    </div>
                    <div class="move_box">
                        <a href="#" class="delete">删除</a>
                    </div>
                </div>
                <div class="row_right">
                    <!--
                    <div class="add_price">
                        运费<span class="trans_price">0</span>元 +
                        会员折扣<span class="member_price">0</span>元 +
                        等等等等 = 总价 ￥<span class="total_price">0.00</span>元
                    </div>
                    
                    <div class="confirm_box">
                        <input type="submit" value="加入购物车">
                    </div>
                    -->
                </div>
            </div>
            </form>
    	</div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>	
</body>
</html>







