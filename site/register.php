<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot']; ?>"/>
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/login.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/register.js"></script>
</head>
<div id="container" class="register">
    <div id="userbar">
        <div class="wrapper">
            <?php import_part("Custom.module","userBar");?>
        </div>
    </div>
    <div id="contain">
        <div class="wrapper">
            <div class="main_box">
                <div id="login_showText">
                    <p>美好·优质·生活</p>
                    <p>品质家生活</p>
                </div>
                <div class="reg_box">
                    <div class="log_or_reg">
                        <a class="" href="<?php e_page("user","login");?>">登陆</a>
                        <a class="selected" href="<?php e_page("user","register");?>">注册</a>
                    </div>
                    <div class="input">
                        <form action="<?php e_page("user","registerSubmit");?>" class="log_form" method="POST">
                            <div class="email">
                                <label for="user_username">邮箱账号</label>
                                <input id="user_username" name="regName" type="text" placeholder="请输入邮箱账号">
                            </div>
                            <div class="tel">
                                <label for="user_tel">手机号码</label>
                                <input id="user_tel" name="tel" type="text" placeholder="请输入手机号码">
                            </div>
                            <div class="code">
                                <label for="user_code">验证码</label>
                                <input id="user_code" name="confirmCode" type="text" placeholder="6位验证码">
                                <img class="active" src="style/image/right.png">
                                <img class="inactive" src="style/image/wrong.png">
                                <a class="get_code" href="javascript:">获取验证码</a>
                            </div>
                            <div class="set_pass">
                                <label for="user_password">设置密码</label>
                                <input id="user_password" name="password" type="password" placeholder="请输入密码">
                            </div>
                            <div class="wrong_info">
                                <!--错误信息显示位置-->
                            </div>
                            <div class="confirm_reg">
                                <input class="confirm" type="submit" value="立即注册">
                            </div>
                            <p>点击注册，即表示您同意并遵守品质家的<a href="#">用户协议</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>