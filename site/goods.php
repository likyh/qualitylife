<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <base href="<?php echo $system['siteRoot']; ?>"/>
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title><?php echo $result['name']; ?> - 品质家生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/detail.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugin/jquery.cookie.js"></script>
    <script type="text/javascript">
        $.cookie('backUrl',window.location.href,{ path: "/"});
    </script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/goods.js"></script>
    <script type="text/javascript" src="script/collect.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <div id="contain">
    <div class="wrapper">
        <div class="product">
            <div class="top_row">
                <div class="list_map">
                    <?php foreach ($result['class'] as $key => $value) { ?>
                        <a class="map_<?php echo $key;?>" href="#"><?php echo $value['name'];?></a>
                    <?php } ?>
                </div>
                <div class="share">
                    <a href="javascript:">分享</a>
                    <div id="shareBox">
                        <div class="bshare-custom icon-medium-plus"><a title="分享到QQ空间" class="bshare-qzone"></a><a title="分享到新浪微博" class="bshare-sinaminiblog"></a><a title="分享到人人网" class="bshare-renren"></a><a title="分享到腾讯微博" class="bshare-qqmb"></a><a title="分享到网易微博" class="bshare-neteasemb"></a><a title="更多平台" class="bshare-more bshare-more-icon more-style-addthis"></a><span class="BSHARE_COUNT bshare-share-count">0</span></div><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=1&amp;lang=zh"></script><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
                    </div>
                </div>
            </div>
            <div class="product_intro">
                <div class="slide_pic">
                    <?php if($result['picture']){?>
                        <?php foreach ($result['picture'] as $key => $value) {?>
                            <img class="show_pic show_pic_<?php echo $key;?>" data-img="<?php echo $key;?>" src="<?php echo $value['url']; ?>">
                        <?php } ?>
                        <div id="small_pic_box">
                            <?php foreach ($result['picture'] as $key => $value) {?>
                                <img class="small_pic" data-img="<?php echo $key;?>" src="<?php echo $value['url']; ?>">
                            <?php } ?>
                        </div>
                    <?php }?>
                    <div class="wish_sheet">
                            <div class="icon"></div>
                            <a class="wish_pre" href="#">加入愿望单</a>
                            <a class="wish_after" href="<?php e_page('wish','index');?>">未许愿</a>
                    </div>
                </div>
                
                <div class="kind_and_price">
                    <form method="POST" action="<?php e_page('order','directBuy');?>">
                        <div class="product_name" data-gopenId="<?php echo $result['gopen_id'];?>">
                            <h1><?php echo $result["name"]; ?></h1>
                            <input type="hidden" id="gopen_id" name="gopen_id" value="<?php echo $result['gopen_id'];?>">
                            <img class="special_hot" src="style/image/hot.png">
                            <img class="special_new" src="style/image/new.png">
                        </div>
                        <div class="price_box">
                            <h2>价格</h2>
                            <div class="origin_price">￥<?php echo $result["original_price"]; ?></div>
                            <div class="now_proce">￥<?php echo $result["selling_price"]; ?></div>
                            <div class="labels">
                                <?php foreach ($result['label'] as $key => $value) { ?>
                                    <div class="label">
                                        <?php echo $value['name']; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="trans">
                            <h2><label for="choose_place">配送</label></h2>
                        <span>至
                            <select name="province" id="provinceInput" class="province"></select>
                            <select name="city" id="cityInput" class="city"></select>
                            <select name="district" id="districtInput" class="district"></select>
                            邮费<span class="trans_price"> 6 </span>元
                        </span>
                        </div>
                        <hr class="product_basic_info"/>
                        <div class="classify">
                            <h2>产品分类</h2>
                            <?php foreach($result['attribute'] as $key => $value) {?>
                                <div class="kind kind_<?php echo $key; ?>" data-attributeId="<?php echo $value['id'];?>" data-stock="<?php echo $value['stock']; ?>">
                                    <a href="#"><?php echo $value['name']; ?></a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="amount_box">
                            <h2><label for="amount"></label>数量</h2>
                            <div class="choose_amount">
                                <div class="minus">-</div>
                                <div class="now_amount">
                                    <input type="text" id="amount" name="amount" value="1" />
                                </div>
                                <div class="plus">+</div>
                            </div>
                            <span class="surplus">库存:<span id="surnumber"><?php echo $result["stock"]; ?></span></span>
                        </div>
                        <div class="comment">
                            <h2>评分</h2>
                            <div class="stars" data-score="<?php echo $result['score']['product_ave'];?>">
                                <img class="active" src="style/image/star_light.png">
                                <img class="inactive" src="style/image/star.png">
                            </div>
                        </div>
                        <div class="confirm">
                            <a href="#" class="buy_now">立即购买</a>
                            <a href="#" class="in_car">加入购物车</a>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        <div class="sidebar">
            <div class="classify_box">
                <div class="sideTitle">
                    <span>收藏网站</span>
                </div>
                <a href="http://www.pzjsh.com" id="collect" title="品质家生活">
                    <img src="style/image/bookmark.png" alt="品质家生活">
                </a>
                <ul class="classify">
                    <div class="sideTitle">
                    <span>宝贝分类</span>
                    </div>
                <?php foreach ($result['subclasses'] as $key => $value) { ?>
                    <li>
                        <a href="<?php e_page('goods','list',"category_id={$value['id']}");?>"><?php echo $value['name'];?></a>
                    </li>
                <?php } ?>
                </ul>
            </div>
            <div class="related">
                <?php import_part("Custom.goods", "relatedShow"); ?>
            </div>
        </div>
        <div class="product_detail">
            <div class="detail_list">
                <ul class="list">
                    <li><a class="detail_title selected" href="javascript:" data-page="detail_intro">商品介绍</a></li>
                    <li><a class="detail_title" href="javascript:" data-page="detail_comment">所有评论</a></li>
                    <li><a class="detail_title" href="javascript:" data-page="detail_trans">快递说明</a></li>
                    <li><a class="detail_title" href="javascript:" data-page="detail_service">售后服务</a></li>
                    <li><a class="detail_title" href="javascript:" data-page="detail_readme">买家必读</a></li>
                </ul>
            </div>
            <div class="detail detail_intro">
                <div class="standard standard_1">
                    <?php if(isset($result["parameter"]['brand'])&&$result["parameter"]['brand']!=null){ ?>
                    <h4>品名</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]['brand']. "<br />" . $result['name'];?>
                    </div>
                    <?php } ?>
                </div>
                <div class="standard standard_2">
                    <?php if(isset($result["parameter"]['no'])&&$result["parameter"]['no']!=null){ ?>
                    <h4>型号</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]['no']; ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="standard standard_4">
                    <?php if(isset($result["parameter"]['material'])&&$result["parameter"]['material']!=null){ ?>
                    <h4>材质</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["material"]; ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="standard standard_5">
                    <?php if(isset($result["parameter"]['packaging'])&&$result["parameter"]['packaging']!=null){ ?>
                    <h4>包装</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["packaging"]; ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="standard standard_6">
                    <?php if(isset($result["parameter"]['color'])&&$result["parameter"]['color']!=null){ ?>
                    <h4>颜色</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["color"]; ?>
                    </div>
                    <?php } ?>
                </div>
                
                <div class="standard standard_7">
                    <?php if(isset($result["parameter"]['storage'])&&$result["parameter"]['storage']!=null){ ?>
                    <h4>存储方式</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["storage"]; ?>
                    </div>
                    <?php } ?>
                </div>
        
                <div class="standard standard_7">
                    <?php if(isset($result["parameter"]['guarantee'])&&$result["parameter"]['guarantee']!=null){ ?>
                    <h4>保质期</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["guarantee"]; ?>
                    </div>
                    <?php } ?>
                </div>

                <div class="standard standard_7">
                    <?php if(isset($result["parameter"]['origin'])&&$result["parameter"]['origin']!=null){ ?>
                    <h4>原产地</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["origin"]; ?>
                    </div>
                    <?php } ?>
                </div>

                <div class="standard standard_7">
                    <?php if(isset($result["parameter"]['packaging'])&&$result["parameter"]['packaging']!=null){ ?>
                    <h4>包装</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["packaging"]; ?>
                    </div>
                    <?php } ?>
                </div>

                <div class="standard standard_7">
                    <?php if(isset($result["parameter"]['volume'])&&$result["parameter"]['volume']!=null){ ?>
                    <h4>体积/容量</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["volume"]; ?>
                    </div>
                    <?php } ?>
                </div>
                
                <div class="standard standard_7">
                    <?php if(isset($result["parameter"]['usage'])&&$result["parameter"]['usage']!=null){ ?>
                    <h4>用法</h4>
                    <div class="Parameters">
                        <?php echo $result["parameter"]["usage"]; ?>
                    </div>
                    <?php } ?>
                </div>



                <div class="standard standard_8">
                    <?php if(isset($result["detail"])&&$result["detail"]!=null){ ?>
                    <h4>详情</h4>
                    <div class="Parameters">
                        <?php echo $result['detail'];?>
                    </div>
                    <?php } ?>
                </div>



            </div>
            <div class="detail detail_comment">
                <!-- 所有评论 -->
                <div class="user_comment_box">
                    <div class="user_comment">
                        <div class="user_brief">
                            <img class="user_icon" src="style/image/user.png" >
                            <p class="user_name" data-userId=""></p>
                        </div>
                        <div class="comment_content">
                            <div class="comment_content_info">
                                <div class="stars" data-score="">
                                    <img class="active" src="style/image/star_light.png">
                                    <img class="inactive" src="style/image/star.png">
                                </div>
                                <time class="comment_time"></time>
                            </div>
                            <div class="comment_content_text"></div>
                            <div class="reply_content">
                                <p class="replay_content_main"></p>
                                <time class="reply_time"></time>
                            </div>
                        </div>
                    </div>
                </div>
                <!--翻页-->
                <div class="comment_pages" data-count="<?php echo $result['score']['count'];?>">
                    <a class="comment_pages_pre comment_page"  href="javascript:">上一页</a>
                    <a class="comment_part_pre comment_part"  href="javascript:">...</a>
                    <a class="comment_pages_num comment_page"  href="javascript:" data-page-num="1">1</a>
                    <a class="comment_part_next comment_part" href="javascript:">...</a>
                    <a class="comment_pages_next comment_page" href="javascript:">下一页</a>
                </div>
            </div>
            <div class="detail detail_trans">
                <article>
                <?php if(isset($result["delivery"])&&!empty($result["delivery"])){ 
                    echo $result["delivery"]['content'];}
                ?>
                </article>
            </div>
            <div class="detail detail_service">
                <!-- 售后服务 -->
                <article>
                <?php if(isset($result["service"])&&!empty($result["service"])){ 
                    echo $result["service"]['content'];}
                ?>
                </article>
            </div>
            <div class="detail detail_readme">
                <!-- 买家必读 -->
                <article>
                <?php if(isset($result["buyread"])&&!empty($result["buyread"])){ 
                    echo $result["buyread"]['content'];}
                ?>
                </article>
            </div>
        </div>
    </div>
    </div>
    <?php import_part("custom.module","footer");?>
</div>
</body>
</html>