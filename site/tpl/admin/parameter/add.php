<form action="<?php e_page('parameter','addAfter'); ?>" method="POST">
	<fieldset>
		<legend>添加信息</legend>
		<input type="hidden" value="<?php echo $result['gopen_id']?>" name="gopen_id">
		<br/>
		<label for="brand">品牌</label>
		<input type="text" name="brand" id="brand" placeholder="请输入品牌" />
		<br/>
		<label for="no">货号</label>
		<input type="text" name="no" id="no" placeholder="请输入货号" />
		<br/>
		<label for="listing_date">上市年月</label>
		<input type="text" name="listing_date" id="listing_date" placeholder="请输入上市年月" />
		<br/>
		<label for="material">材料</label>
		<input type="text" name="material" id="material" placeholder="请输入材料" />
		<br/>
		<label for="season">季节</label>
		<input type="text" name="season" id="season" placeholder="请输入季节" />
		<br/>
		<label for="color">颜色</label>
		<input type="text" name="color" id="color" placeholder="请输入颜色" />
		<br/>
		<label for="storage">存储方式</label>
		<input type="text" name="storage" id="storage" placeholder="请输入存储方式" />
		<br/>
		<label for="guarantee">保质期</label>
		<input type="text" name="guarantee" id="guarantee" placeholder="请输入保质期" />
		<br/>
		<label for="origin">原产地</label>
		<input type="text" name="origin" id="origin" placeholder="请输入原产地" />
		<br/>
		<label for="packaging">包装</label>
		<input type="text" name="packaging" id="packaging" placeholder="请输入包装" />
		<br/>
		<label for="volume">体积/容量</label>
		<input type="text" name="volume" id="volume" placeholder="请输入体积容积" />
		<br/>
		<label for="usage">用法</label>
		<input type="text" name="usage" id="usage" placeholder="请输入用法" />
		<br/>
		<input type="submit" name='submit' value="增加属性" />
	</fieldset>
</form>