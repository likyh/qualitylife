
<form action="<?php e_page('parameter','updateAfter'); ?>" method="POST">
	<fieldset>
		<legend>修改信息</legend>
		<input type="hidden" name="id" value="<?php echo $result['parameter']['id']?>">
		<label for="brand">品牌</label>
		<input type="text" name="brand" id="brand" value="<?php echo $result['parameter']['brand']?>" />
		<br/>
		<label for="no">货号</label>
		<input type="text" name="no" id="no" value="<?php echo $result['parameter']['no']?>"/>
		<br/>
		<label for="listing_date">上市年月</label>
		<input type="text" name="listing_date" id="listing_date" value="<?php echo $result['parameter']['listing_date']?>" />
		<br/>
		<label for="material">材料</label>
		<input type="text" name="material" id="material"  value="<?php echo $result['parameter']['material']?>"/>
		<br/>
		<label for="season">季节</label>
		<input type="text" name="season" id="season"  value="<?php echo $result['parameter']['season']?>"/>
		<br/>
		<label for="color">颜色</label>
		<input type="text" name="color" id="color" value="<?php echo $result['parameter']['color']?>"/>
		<br/>
		<label for="storage">存储方式</label>
		<input type="text" name="storage" id="storage"  value="<?php echo $result['parameter']['storage']?>"/>
		<br/>
		<label for="guarantee">保质期</label>
		<input type="text" name="guarantee" id="guarantee" value="<?php echo $result['parameter']['guarantee']?>"/>
		<br/>
		<label for="origin">原产地</label>
		<input type="text" name="origin" id="origin" value="<?php echo $result['parameter']['origin']?>"/>
		<br/>
		<label for="packaging">包装</label>
		<input type="text" name="packaging" id="packaging" value="<?php echo $result['parameter']['packaging']?>" />
		<br/>
		<label for="volume">体积/容量</label>
		<input type="text" name="volume" id="volume"  value="<?php echo $result['parameter']['volume']?>"/>
		<br/>
		<label for="usage">用法</label>
		<input type="text" name="usage" id="usage"  value="<?php echo $result['parameter']['usage']?>"/>
		
		<input type="submit" name='submit' value="提交修改" />
	</fieldset>
</form>