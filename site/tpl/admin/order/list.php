
<table id="dataTable">
    <thead>
        <tr>
            <th>id</th>
            <th>订单金额</th>
            <th>付款金额</th>
            <th>下单时间</th>
            <th>付款时间</th>
            <th>运费</th>
            <th>快递名称</th>
            <th>快递数量</th>
            <th>订单状态</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    <?php  foreach ($result['order'] as $key=>$value){ ?>
    <tr>
        <td><?php echo $value['id']?></td>
        <td><?php echo $value['order_amount']?></td>
        <td><?php echo $value['payment_amount']?></td>
        <td><?php echo $value['create_time']?></td>
        <td><?php echo $value['payment_time']?></td>
        <td><?php echo $value['freight']?></td>
        <td><?php echo $value['express_name']?></td>
        <td><?php echo $value['express_num']?></td>
        <td><?php 
            switch ($value['state']) {
                case 1:
                    echo "未付款";
                    break;
                case 2:
                    echo "已付款";
                    break;
                case 3:
                    echo "未发货";
                    break;
                case 4:
                    echo "已发货";
                    break;
                case 5:
                    echo "交易完成";
                    break;
                case 6:
                    echo "订单取消";
                    break;
            }
        ?></td>
        <td><a href="<?php e_page("order","update",array('id'=> $value['id'],'user_id'=>$value['user_id'],'address_id'=>$value['address_id']))?>">订单管理</a></td>
    </tr>
    <?php }?>
</tbody>
</table>
