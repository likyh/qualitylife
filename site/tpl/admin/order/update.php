<form action="<?php e_page('order','updateAfter'); ?>" method="POST">
    <input type="hidden" value="<?php echo $result['id']?>" name="id">
    <fieldset>
        <legend>购买用户</legend>
        <label for="name">用户名称</label>
        <?php echo $result['user']['username']?>
        <br/>
        <label for="name">联系方式</label>
        <?php echo $result['user']['tel']?>
    </fieldset>
    <fieldset>
        <legend>快递信息</legend>
        <label for="name">收件人</label>
        <?php echo $result['address']['receiver']?>
        <br/>
        <label for="name">联系方式</label>
        <?php echo $result['user']['tel']?>
        <br/>
        <label for="name">收件地址</label>
     
        <?php echo $result['address']['province'].'省 '.$result['address']['city'].'市 '.$result['address']['district'].' '.$result['address']['street']?>
        <br/>
    </fieldset>
    <fieldset>
        <legend>商品信息</legend>
        <?php foreach($result['goods'] as $key=>$value){?>
            <label for="name">商品名称</label>
            <?php echo $value['name']?>
            <br/>
            <label for="name">商品型号</label>
            <?php echo $value['attribute_name']?>
            <br/>
            <label for="name">购买数量</label>
            <?php echo $value['number']?>
            <br/>
            <label for="name">评价状态</label>
            <?php 
                switch ($value['state']) {
                    case 1:
                        echo "用户未评论";
                        break;
                    case 2:
                        echo "用户已评论";
                        break;
                    case 3:
                        echo "卖家未回复";
                        break;
                    case 1:
                        echo "卖家已回复";
                        break;
                }
            ?>
            <br/>
        <?php }?>
    </fieldset>
    <fieldset>
        <legend>订单状态</legend>
        
                <?php if(1==$result['order']['state']){?>
                    <label for="one">未付款</label>
                    <input type="radio" value="1" checked="checked" name="state" id="one">   
                <?php }else{?>
                    <label for="one">未付款</label>
                    <input type="radio" value="1"  name="state" id="one">    
                <?php }?>

                <?php if(2==$result['order']['state']){?>
                    <label for="two">已付款</label>
                    <input type="radio" value="2" checked="checked" name="state" id="two">   
                <?php }else{?>
                    <label for="two">已付款</label>
                    <input type="radio" value="2"  name="state" id="two">   
                <?php }?>


                <?php if(3==$result['order']['state']){?>
                    <label for="three">未发货</label>
                    <input type="radio" value="3" checked="checked" name="state" id="three">   
                <?php }else{?>
                    <label for="three">未发货</label>
                    <input type="radio" value="3"  name="state" id="three">    
                <?php }?>


                <?php if(4==$result['order']['state']){?>
                    <label for="four">已发货</label>
                    <input type="radio" value="4" checked="checked" name="state" id="four">   
                <?php }else{?>
                    <label for="four">已发货</label>
                    <input type="radio" value="4"  name="state" id="four">    
                <?php }?>

                <?php if(5==$result['order']['state']){?>
                    <label for="five">交易完成</label>
                    <input type="radio" value="5" checked="checked" name="state" id="five">   
                <?php }else{?>
                    <label for="five">交易完成</label>
                    <input type="radio" value="5"  name="state" id="five">    
                <?php }?>

                <?php if(6==$result['order']['state']){?>
                    <label for="six">订单取消</label>
                    <input type="radio" value="6" checked="checked" name="state" id="six">   
                <?php }else{?>
                    <label for="six">订单取消</label>
                    <input type="radio" value="6"  name="state" id="six">    
                <?php }?>
      
    </fieldset>
    <input type="submit" value="提交修改订单状态" name="submit">
</form>

