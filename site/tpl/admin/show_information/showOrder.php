<table id="dataTable">
	<thead>
		<tr>
			<th>id</th>
			<th>gopen_id</th>
			<th>商品名</th>
			<th>售价</th>
			<th>原价</th>
			<th>种类</th>
			<th>图片id</th>
			<th>标签id</th>
			<th>详情</th>
			<th>库存</th>
			<th>销量</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($r['goods'] as $key => $value) {
				echo "<tr>";
				echo "<td>".$value['id']."</td>";
				echo "<td>".$value['gopen_id']."</td>";
				echo "<td>".$value['name']."</td>";
				echo "<td>".$value['selling_price']."</td>";
				echo "<td>".$value['original_price']."</td>";
				echo "<td>".$value['category_id']."</td>";
				echo "<td>".$value['picture_ids']."</td>";
				echo "<td>".$value['label_ids']."</td>";
				echo "<td>".$value['detail']."</td>";
				echo "<td>".$value['stock']."</td>";
				echo "<td>".$value['volume']."</td>";
				echo "</tr>";
			}
		 ?>
	</tbody>
</table>