
<table id="dataTable">
	<thead>
		<tr>
			<!--<th>id</th>-->
			<th>id</th>
			<th>商品名</th>
			<th>售价</th>
			<th>原价</th>
			<th>库存</th>
			<th>销量</th>
			<th>属性管理</th>
			<th>参数管理</th>
			<th>上下架</th>
			<th>删除</th>			
		</tr>
	</thead>

	<tbody>
		<?php  foreach ($result['goods'] as $key=>$value){ ?>
		    <tr>
		        <td><?php echo $value['id']?></td>
		        <td><?php echo mb_substr($value['name'], 0, 15, 'utf-8').'...'; ?></td>
		        <td><?php echo $value['selling_price']?></td>
		        <td><?php echo $value['original_price']?></td>
		        <td><?php echo $value['stock']?></td>
		        <td><?php echo $value['volume']?></td>
		        <td> <a href="<?php e_page("attribute","getGoodsAttr",array('id'=> $value['id']))?>">查看属性</a>　　　<a href="<?php e_page("attribute","addGoodsAttr",array('id'=> $value['id']))?>">增加属性</a></td>
			
			<?php if($value['parameter_id']){?>
				<td><a href="<?php e_page("parameter","update",array('id'=> $value['id']))?>">参数管理</a></td>
			<?php }else{?>
				<td> <a href="<?php e_page("parameter","add",array('id'=> $value['id']))?>">增加参数</a></td>　
			<?php  }?>
		        　　　
		        <td><?php if($value['enable']==1){?> 已上架 <a  style="color:red" href="<?php e_page("goods","is_up",array('id'=> $value['id'],"action"=>"down"))?>"> 下架</a><?php }?>
		        	    <?php if($value['enable']==0){?> 已下架 <a  style="color:green" href="<?php e_page("goods","is_up",array('id'=> $value['id'],"action"=>"up"))?>"> 上架</a><?php }?></td>
		        <td><a href="<?php e_page("goods","update",array('id'=> $value['id']))?>">修改</a> 　　　<a onclick="return confirm('你真的要删除吗？') ? true : false"  href="<?php e_page("goods","delete",array('id'=> $value['id']))?>">删除</a></td>
		    </tr>
		<?php }?>

		
	</tbody>


</table>
<?php
/** @var Page $page */
$page=$r['page'];
 echo $page->getPageHtml(); 
 ?>