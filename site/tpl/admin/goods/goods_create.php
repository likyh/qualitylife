<form action="<?php e_page('goods','modify'); ?>" post="GET">
    <fieldset>
        <label for="gopen_id">欲修改商品ID</label>
        <input type="text" name="gopen_id" id="gopen_id" value="<?php if(isset($result['goodsInfo']['gopen_id'])) echo $result['goodsInfo']['gopen_id'];?>"/>
    </fieldset>
    <fieldset>
        <legend>修改信息</legend>
        <label for="name">商品名</label>
        <input type="text" name="name" id="name" value="<?php if(isset($result['goodsInfo']['name'])) echo $result['goodsInfo']['name'];?>"/>
        <br/>
        <label for="selling_price">售价</label>
        <input type="text" name="selling_price" id="selling_price" value="<?php if(isset($result['goodsInfo']['selling_price'])) echo $result['goodsInfo']['selling_price'];?>"/>
        <br/>
        <label for="original_price">原价</label>
        <input type="text" name="original_price" id="original_price" value="<?php if(isset($result['goodsInfo']['original_price'])) echo $result['goodsInfo']['original_price'];?>"/>
        <br/>
    
        <label for="category">种类</label>
        <div id="cateBox">
            <select name="category" class="category" data-level="1">
                <option value="1" class="categoryOption">有车族</option>
            </select>
        </div>
        
        </br>
        <label for="picture_ids">图片</label>
        <input type="hidden" name="picture_ids" id="picture_ids" value="<?php if(isset($result['goodsInfo']['picture_ids'])) echo $result['goodsInfo']['picture_ids'];?>"/>
        <div id="picture">
            <?php foreach ($result['goodsInfo']['picture'] as $key => $value) { ?>
                <div class="pic<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>">
                    <img style="width:100px;"src="resource/<?php echo $value['url'];?>" data-id="<?php echo $value['id'];?>">
                    <a href="javascript:;" class="deletePic" data-id="<?php echo $value['id'];?>">删除</a>
                    <input type="hidden" name="picture[]" value="<?php echo $value['id'];?>" />
                </div>
            <?php } ?>
        </div>
        <a href="javascript:;" id="addPicButton">添加图片</a>
        <div class="addPicBox" style="display:none;">
            <?php foreach ($result['picture'] as $key => $value) { ?>
                <div class="pic<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>">
                    <img style="width:100px;"src="resource/<?php echo $value['url'];?>" data-id="<?php echo $value['id'];?>">
                    <a href="javascript:;" class="addPic" data-id="<?php echo $value['id'];?>">添加</a>
                    <input type="hidden" name="picture[]" value="<?php echo $value['id'];?>"disabled />
                </div>
            <?php } ?>
        </div>
        </br>
    
        <label>标签</label>
        <?php foreach ($r['labels'] as $key => $value) { ?>
        <label for="label<?php echo $value['id'];?>"><?php echo $value['name']?></label>
        <input type="checkbox" name="label[]" id="label<?php echo $value['id'];?>" value="<?php echo $value['id'];?>"
        <?php 
        if(isset($result['goodsInfo']['label'])){
            foreach ($result['goodsInfo']['label'] as $p => $q) {
                if($value['id']==$q['id']){
                    echo "checked";
                    break;
                }
            }
        }
        ?>/>
        <?php } ?>
    </fieldset>
    <fieldset>
        <label for="detail">详情</label>
        <input type="text" name="detail" id="detail" value="<?php if(isset($result['goodsInfo']['detail'])) echo $result['goodsInfo']['detail'];?>"/>
        <br/>
        <label for="stock">库存</label>
        <input type="text" name="stock" id="stock" value="<?php if(isset($result['goodsInfo']['stock'])) echo $result['goodsInfo']['stock'];?>"/>
        <br/>
        <label for="volume">销量</label>
        <input type="text" name="volume" id="volume" value="<?php if(isset($result['goodsInfo']['volume'])) echo $result['goodsInfo']['original_price'];?>"/>
    </fieldset>
    <input type="submit" name="submit" />
    <input type="hidden" name='submitted' value="1" />
</form>
<script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
    <?php if(!empty($result['message']['result'])){ ?>
        alert("<?php echo $result['message']['result'];?>");
    <?php } ?>
    var categoryData = <?php echo json_encode($result['category']);?>;
    var goodsCateData = <?php echo json_encode($result['goodsInfo']['category']);?>;
    console.log(categoryData);
    console.log(goodsCateData);
    var $optionTpl = $('.categoryOption').remove();
    var $selectTpl = $('.category').remove();
    var $cateBox = $('#cateBox');
    var cateLevel = 0;
    var levelTree = [];//记录历史路径的
    appendSeletion(categoryData,goodsCateData);
    lastCate();
    console.log(levelTree);


    function appendSeletion(data,goodsData){
        //selection
        if(data.length==0){
            return;
        }
        cateLevel += 1;
        var $select = $selectTpl.clone()
        .attr('data-level', cateLevel)
        .addClass('select'+cateLevel);
        $cateBox.append($select);

        //option
        var ifExist = 0;//是否有默认值
        var existData;
        for (var i in data) {
            //console.log(data[i]);
            var $option = $optionTpl.clone().attr({
                'value': data[i].id,
                'num':i
            }).text(data[i].name);
            $select.append($option);

            for(var j in goodsData){
                //console.log(goodsData[j]);
                if(goodsData[j].id==data[i].id){
                    ifExist = 1;
                    existData = data[i];
                    levelTree.push(i);
                    $option.attr('selected', 'selected');
                }
            }
        };
        if(ifExist == 0){
            existData = data[0];
        }
        //console.log(existData);
        if(existData!=undefined){
            if(existData.children!=undefined){
                if(existData.children.length!=0){
                    appendSeletion(existData.children,goodsData);
                }
            }
        }
    }

    $('body').on('change','.category',function(event) {
        var level = $(this).attr('data-level');
        var id = $(this).find(":selected").attr('num');
        var data=categoryData;
        console.log('change');
        $('.category').each(function(index, el) {
            if($(this).attr('data-level')>level){
                $(this).remove();
                console.log('level');
            }
        });
        for(var i in levelTree){
            if(i<level-1){
                data = categoryData[levelTree[i]].children;
                console.log(data);
            }
        }
        data = data[id];
        console.log(data);
        cateLevel = parseInt(level);
        if(data.children.length!=0){
            appendSeletion(data.children,goodsCateData);
        }
        lastCate();
        //alert(level);
    });

    function lastCate(){
        //给最后一个类别改名输入
        $('#cateBox select').attr('name', 'category');
        $('#cateBox select:last').attr('name', 'lastCategory');
    }


    //下面是图片操作
    $('body').on('click','.deletePic',function(){
        $(this).closest('div').remove();
    });
    $('#addPicButton').click(function(event) {
        $('.addPicBox').show();
    });
    $('.addPic').click(function(event) {
        var $picBox = $(this).closest('div');
        var id = $picBox.attr('data-id');
        var ifExist = 0;
        $('#picture div').each(function(index, el) {
            if($(this).attr('data-id')==id){
                alert("已存在该图片");
                ifExist = 1;
                return;
            }
        });
        if(ifExist==1){
            return;
        }
        var $newPicBox = $picBox.remove().clone();
        $newPicBox.find('a.addPic').text('删除').attr({'class': 'deletePic'});
        $newPicBox.find('input').attr('disabled', false);
        $('#picture').append($newPicBox);
    });
</script>






