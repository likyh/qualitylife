<script type="text/javascript">
    function choose(i){

        var check=document.getElementById(i);
        if (check.checked==true) {
            check.checked=false;
        }else{
            check.checked=true;
        }

    }
</script>
<form action="<?php e_page('goods','updateAfter'); ?>"  method="POST">
    
    <fieldset>
        <legend>新增商品</legend>
        <input type="hidden" name="gopen_id" value="<?php echo $result['good']['gopen_id']?>">
        <label for="name">商品名</label>
        <input type="text" name="name" id="name"  value="<?php echo $result['good']['name']?>"/>
        <br/>
        <label for="selling_price">售价</label>
        <input type="text" name="selling_price" id="selling_price" value="<?php echo $result['good']['selling_price']?>"/>
        <br/>
        <label for="original_price">原价</label>
        <input type="text" name="original_price" id="original_price"  value="<?php echo $result['good']['original_price']?>"/>
        <br/>
    </fieldset>
    <fieldset>
        <label for="category">种类</label>
        <div id="cateBox">
            <select name="category" class="category" data-level="1" style="width:150px;margin: -150px 0 0 100px;"> 
                <?php foreach ($result['category'] as $key => $value) {?>
                    <optgroup label="<?php echo $key?>">
                        <?php foreach ($value as $k => $v) {?>
                                <?php if($v['id']==$result['good']['category_id']){?>
                                    <option value="<?php echo $v['id']?>" selected="selected"><?php echo $v['name']?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $v['id']?>"><?php echo $v['name']?></option>
                                <?php }?>
                        <?php } ?>
                    </optgroup>
                <?php }?>
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label for="delivery">快递说明</label>
        <div id="cateBox">
            <select name="delivery" class="category" data-level="1" style="width:150px;margin: -100px 0 0 100px;"> 
                        <?php foreach ($result['delivery'] as $key => $value) {?>
                            
                           <?php if($value['id']==$result['good']['delivery_id']){?>

                                <option value="<?php echo $value['id']?>" selected="selected"><?php echo $value['name']?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                            
                            <?php }?>


                        <?php } ?>
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label for="service">售后服务</label>
        <div id="cateBox">
            <select name="service" class="category" data-level="1" style="width:150px;margin: -100px 0 0 100px;"> 
                        <?php foreach ($result['service'] as $key => $value) {?>
                            <?php if($value['id']==$result['good']['service_id']){?>

                                <option value="<?php echo $value['id']?>" selected="selected"><?php echo $value['name']?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                            
                            <?php }?>
                        <?php } ?>
            </select>
        </div>
    </fieldset>


    <fieldset>
        <label for="buyread">买家必读</label>
        <div id="cateBox">
            <select name="buyread" class="category" data-level="1" style="width:150px;margin: -100px 0 0 100px;"> 

                        <?php foreach ($result['buyread'] as $key => $value) {?>

                            <?php if($value['id']==$result['good']['buyread_id']){?>

                                <option value="<?php echo $value['id']?>" selected="selected"><?php echo $value['name']?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                            
                            <?php }?>


                        <?php } ?>
            </select>
        </div>
    </fieldset>


    <fieldset>
        <label>标签</label>
        <?php foreach ($result['label'] as $key => $value) { ?>

            <label for="label<?php echo $value;?>"><?php echo $value?></label>
            <input type="checkbox" name="label[]" id="label<?php echo $value;?>" value="<?php echo $key;?>" <?php
                foreach ($result['good']['label_ids'] as $k => $v) {
                    if ($key==$v) {
                        echo "checked='checked'";
                    }
                }
            ?>>

        <?php }?>
    </fieldset>

    <fieldset>
        <label for="detail">详情</label>
        <script id="aTextInput" name="detail" class="editor" type="text/plain" style="width:1200px;height:600px;">
        <?php echo $result['good']['detail']?>
        </script>
        <br/>
        <label for="stock">库存</label>
        <input type="text" name="stock" id="stock" value="<?php echo $result['good']['stock']?>"/>
        <br/>
        <label for="volume">销量</label>
        <input type="text" name="volume" id="volume" value="<?php echo $result['good']['volume']?>"/>
    </fieldset>
    

    <fieldset>
        <?php foreach ($result['picture'] as $key => $value) { ?>
        
        <img src="<?php echo $value['url']?>" style="width:100px; height: 100px; margin: 25px;" for="<?php echo $value['id'];?>"  onclick="choose(<?php echo $value['id']; ?>);">
        <input type="checkbox" name="picture[]" id="<?php echo $value['id'];?>" value="<?php echo $value['id'];?>" <?php
            foreach ($result['good']['picture_ids'] as $k => $v) {
                if ($v==$value['id']) {
                    echo "checked='checked'";
                }
                
            }
        ?>>
        <?php }?>

    </fieldset>
   
    <input type="submit" name="submit"  value="修改商品" />
 
</form>







