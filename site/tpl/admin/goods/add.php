<script type="text/javascript">
    function choose(i){

        var check=document.getElementById(i);
        if (check.checked==true) {
            check.checked=false;
        }else{
            check.checked=true;
        }

    }
</script>
<form action="<?php e_page('goods','addAfter'); ?>"  method="POST">
    
    <fieldset>
        <legend>新增商品</legend>
        <label for="name">商品名</label>
        <input type="text" name="name" id="name" />
        <br/>
        <label for="selling_price">售价</label>
        <input type="text" name="selling_price" id="selling_price" />
        <br/>
        <label for="original_price">原价</label>
        <input type="text" name="original_price" id="original_price" />
        <br/>
    </fieldset>
    <fieldset>
        <label for="category">种类</label>
        <div id="cateBox">
            <select name="category" class="category" data-level="1" style="width:150px;margin: -150px 0 0 100px;"> 
                <?php foreach ($result['category'] as $key => $value) {?>
                    <optgroup label="<?php echo $key?>">
                        <?php foreach ($value as $k => $v) {?>
                            <option value="<?php echo $v['id']?>"><?php echo $v['name']?></option>
                        <?php } ?>
                    </optgroup>
                <?php }?>
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label for="delivery">快递说明</label>
        <div id="cateBox">
            <select name="delivery" class="category" data-level="1" style="width:150px;margin: -100px 0 0 100px;"> 
                        <?php foreach ($result['delivery'] as $key => $value) {?>
                            <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                        <?php } ?>
            </select>
        </div>
    </fieldset>

    <fieldset>
        <label for="service">售后服务</label>
        <div id="cateBox">
            <select name="service" class="category" data-level="1" style="width:150px;margin: -100px 0 0 100px;"> 
                        <?php foreach ($result['service'] as $key => $value) {?>
                            <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                        <?php } ?>
            </select>
        </div>
    </fieldset>


    <fieldset>
        <label for="buyread">买家必读</label>
        <div id="cateBox">
            <select name="buyread" class="category" data-level="1" style="width:150px;margin: -100px 0 0 100px;"> 
                        <?php foreach ($result['buyread'] as $key => $value) {?>
                            <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                        <?php } ?>
            </select>
        </div>
    </fieldset>


    <fieldset>
        <label>标签</label>
        <?php foreach ($result['label'] as $key => $value) { ?>
        <label for="label<?php echo $value;?>"><?php echo $value?></label>
        <input type="checkbox" name="label[]" id="label<?php echo $value;?>" value="<?php echo $key;?>">
        <?php }?>
    </fieldset>

    <fieldset>
        <label for="detail">详情</label>
        <script id="aTextInput" name="detail" class="editor" type="text/plain" style="width:1200px;height:600px;">
        </script>
        <br/>
        <label for="stock">库存</label>
        <input type="text" name="stock" id="stock" />
        <br/>
        <label for="volume">销量</label>
        <input type="text" name="volume" id="volume" />
    </fieldset>
    

    <fieldset>
        <?php foreach ($result['picture'] as $key => $value) { ?>
        
        <img src="<?php echo $value['url']?>" style="width:100px; height: 100px; margin: 25px;" for="<?php echo $value['id'];?>"  onclick="choose(<?php echo $value['id']; ?>);">
        <input type="checkbox" name="picture[]" id="<?php echo $value['id'];?>" value="<?php echo $value['id'];?>">
        <?php }?>

    </fieldset>
   
    <input type="submit" name="submit"  value="增加商品" />
 
</form>







