<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <link rel="stylesheet" type="text/css" href="style/user_order.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/user_order.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?> 
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <?php import_tpl("tpl/user/nav.php");?>
        <div class="content">
            <div class="welcome_page">
                <h3>您的订单</h3>
                <div class="h4_BG">
                    <h4>所有订单</h4>
                </div>
                <div class="order">
                    <?php foreach($result['list'] as $i){ ?>
                    <div class="orderItem" data-orderId="<?php echo $i['id'];?>">
                        <h5><?php echo $i['create_time']; ?> | 订单号：<?php echo $i['id'];?> <a class="checkOrder" href="<?php e_page('order','detail',array('id'=>$i['id']));?>">查看订单</a></h5>
                        
                        <ul>
                        <?php foreach($i['goods'] as $g){ ?>
                            <li>
                                <span class="picture"><img src="<?php echo $g['picture'];?>" alt="<?php echo $g['name'];?>"/></span>
                                <div class="name"><span class="name" data-goodsId="<?php echo $g['id'];?>"><?php echo $g['name'];?></span></div>
                                <div class="price">
                                    <span class="original_price"><?php echo $g['original_price'];?></span>
                                    <span class="selling_price"><?php echo $g['selling_price'];?></span>
                                    <span class="labels"><?php echo $g['labels']?></span>
                                </div>
                                <div class="amount"><p class="amount_price"><?php echo $i['payment_amount'];?></p> <p>(运费<?php echo $i['freight'];?>)</p></div>
                                <div class="order_state" data-state="<?php echo $i['state'];?>">
                                    <?php echo $i['stateInfo']?>
                                    <div class="transState" style="display:<?php echo ($i['state']==4||$i['state']==5)?'block':'none';?>">
                                        <a href="javascript:;" class="transButton" data-name="<?php echo $i['express_name'];?>" data-num="<?php echo $i['express_num'];?>">查看物流</a>
                                        <div class="transBox">
                                            <table>
                                                <tr>
                                                    <td class="time"></td>
                                                    <td class="where"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="comment"><a class="comment_button" href="javascript:;" data-goodsId="<?php echo $g['id'];?>" data-comment="<?php echo $g['in_order']['state'];?>">评价订单</a></div>
                                <div class="comment_box">
                                    <div class="product_score">
                                        <p>产品评价</p>
                                        <div class="product_score_choose" data-score="5">
                                            <img class="score_star active product_score_1" data-score="1" src="style/image/star_light.png">
                                            <img class="score_star active product_score_2" data-score="2" src="style/image/star_light.png">
                                            <img class="score_star active product_score_3" data-score="3" src="style/image/star_light.png">
                                            <img class="score_star active product_score_4" data-score="4" src="style/image/star_light.png">
                                            <img class="score_star active product_score_5" data-score="5" src="style/image/star_light.png">
                                        </div>
                                    </div>
                                    <div class="service_score">
                                        <p>服务评价</p>
                                        <div class="service_score_choose" data-score="5">
                                            <img class="score_star active service_score_1" data-score="1" src="style/image/star_light.png">
                                            <img class="score_star active service_score_2" data-score="2" src="style/image/star_light.png">
                                            <img class="score_star active service_score_3" data-score="3" src="style/image/star_light.png">
                                            <img class="score_star active service_score_4" data-score="4" src="style/image/star_light.png">
                                            <img class="score_star active service_score_5" data-score="5" src="style/image/star_light.png">
                                        </div>
                                    </div>
                                    <textarea class="comment_area"></textarea>
                                    <input class="comment_confirm" type="button" value="提交评论"/>
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="page">
                <?php $current = $result['page']->getCurrentPage(); $total = $result['page']->getCurrentPage(); ?>
                <a class="pre_page" href="
                <?php if($current>1){
                    e_page("userInfo","complaint",array("page"=>$current-1));
                }else{
                    echo "javascript:;";
                }?>">&lt; 上一页</a>
                <a class="next_page" href="
                <?php if($current<$total){
                    e_page("userInfo","complaint",array("page"=>$current+1));
                }else{
                    echo "javascript:;";
                }?>">下一页 &gt;</a>
            </div>
        </div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>