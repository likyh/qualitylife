<div id="userHeader">
    <div class="wrapper">
        <div class="user">
            <div class="icon"><img src="style/image/user.png" alt="用户头像"/></div>
            <div class="operate">
                <div class="intro">
                    <p>
                        欢迎您，<br />
                        <a href="#"><?php echo $_SESSION['user']['info']['username'];?></a>
                        <br>
                    </p>
                </div>
                <div class="button">
                    <ul class="button_list">
                        <li class="log"><a href="<?php e_page("user","logout");?>">退出登录</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="recommend">
        </div>
    </div>
</div>