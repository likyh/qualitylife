<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/user_detail.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <?php import_tpl("tpl/user/nav.php");?>
        <div class="content">
            <div class="welcome_page">
                <h3>用户信息</h3>
                <!--
                <div class="username_box">
                    <div class="username_show">
                        您当前的用户名为：
                        <span><?php echo $result['username'];?></span>
                    </div>
                    <a class="username_tochange" href="javascript:;">修改用户名</a>
                    <div class="username_edit">
                        <input id="username_change" type="text" placeholder="请输入新的用户名" />
                        <input id="username_change_confirm" type="button" value="确认修改" />
                    </div>
                </div>
                <div class="tel_box">
                    <div class="tel_show">
                        您当前绑定的手机号为：
                        <span><?php echo $result['tel'];?></span>
                    </div>
                    <a class="tel_tochange" href="javascript:;">修改手机号</a>
                    <div class="tel_edit">
                        <input id="tel_change" type="text" placeholder="请输入新的手机号" />
                        <input id="tel_change_confirm" type="button" value="确认修改" />
                    </div>
                </div>
                -->
                <div class="h4_BG">
                    <h4 class="pass_tochange" href="javascript:;">修改密码</h4>
                </div>
                <div class="pass_box">
                    <!--
                    <div class="pass_show">
                        您当前绑定的手机号为：
                        <span><?php echo $result['pass'];?></span>
                    </div>-->
                    
                    <div class="pass_edit">
                        <p>
                            <label for="pass_change_username">用户名：</label>
                            <input id="pass_change_username" type="text" placeholder="请输入您的用户名" />
                        </p>
                        <p>
                            <label for="pass_change_oldPass">旧密码：</label>
                            <input id="pass_change_oldPass" type="text" placeholder="请输入旧的密码" />
                        </p>
                        <p>
                            <label for="pass_change_newPass">新密码：</label>
                            <input id="pass_change_newPass" type="text" placeholder="请输入新的密码" />
                        </p>
                        <p>
                            <label for="pass_change_rePass">确认新密码：</label>
                            <input id="pass_change_rePass" type="text" placeholder="请再输一遍新的密码" />
                        </p>
                        <input id="pass_change_confirm" type="button" value="确认修改" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













