<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <link rel="stylesheet" type="text/css" href="style/user_order.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/user_order.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <?php import_tpl("tpl/user/nav.php");?>
        <div class="content">
        <div class="welcome_page">
            <h3>您的留言</h3>
            <div class="h4_BG">
                <h4>添加留言</h4>
            </div>
            <form class="add_complaint" action="<?php e_page("userInfo","complaintSubmit");?>" type="GET">
                <textarea name="content" placeholder="在此输入留言"></textarea>
                <input id="complaint_submit" type="submit" />
            </form>
            <div class="h4_BG">
                <h4>历史留言</h4>
            </div>
            <div class="old_complaint">
                <?php foreach ($result['list'] as $key => $value) { ?>
                <p>留言时间 ：<time><?php echo $value['create_time'];?></time></p>
                <p class="complaint_content">
                    <?php echo $value['content'];?>
                </p>
                <?php if(!empty($value['reply'])){ ?>
                <div class="replay">
                    <h5>商家回复</h5>
                    <p><?php echo $value['reply'];?></p>
                    <time><?php echo $value['reply_time'];?></time>
                </div>
                <?php }} ?>
            </div>
            <div class="page">
                <?php $current = $result['page']['current']; $total = $result['page']['total']; ?>
                <a class="pre_page" href="
                <?php if($current>1){
                    e_page("userInfo","complaint",array("page"=>$current-1));
                }else{
                    echo "javascript:;";
                }?>">上一页</a>
                <a class="next_page" href="
                <?php if($current<$total){
                    e_page("userInfo","complaint",array("page"=>$current+1));
                }else{
                    echo "javascript:;";
                }?>">下一页</a>
            </div>
        </div>
        </div>
        
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













