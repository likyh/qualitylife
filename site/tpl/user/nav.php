<div class="user_info_nav_box">
    <ul class="ser_info_nav">
        <li><a href="<?php e_action("index");?>">欢迎页</a></li>
        <li><a href="<?php e_action("detail");?>">用户信息</a></li>
        <li><a href="<?php e_action("order");?>">我的订单</a></li>
        <li><a href="<?php e_action("address");?>">收货地址</a></li>
        <li><a href="<?php e_page("wish","index");?>">我的收藏</a></li>
        <li><a href="<?php e_action("complaint");?>">我的留言</a></li>
        <li><a href="<?php e_action("coupon");?>">我的优惠卡券</a></li>
        <li><a href="<?php e_action("comment");?>">我的评论</a></li>
    </ul>
</div>