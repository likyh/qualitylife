<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <link rel="stylesheet" type="text/css" href="style/user_order.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/user_order.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <?php import_tpl("tpl/user/nav.php");?>
        <div class="content">
            <div class="welcome_page">
            <h3>您的优惠券</h3>
            <div id="couponBox">
                <?php foreach ($result['list'] as $key => $value) { ?>
                <div class="coupon">
                    <h4><?php echo $value['coupon']['name'];?></h4>
                    <div class="state">
                        <?php if($value['state']==1){
                            echo "可用";
                        }else{
                            echo "已使用";
                        }?>
                    </div>
                    <div class="limit_value">
                        <p><?php echo "满".$value['coupon']['limit']."送".$value['coupon']['limit'];?></p>
                    </div>
                    <div class="note">
                        <?php echo $value['coupon']['note'];?>
                    </div>
                    <div class="time">
                        使用时间<time><?php echo $value['coupon']["start_time"];?></time>
                        至<time><?php echo $value['coupon']["end_time"];?></time>
                    </div>
                <?php } ?>
            </div>
            </div>
        </div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













