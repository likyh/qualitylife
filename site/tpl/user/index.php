<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <?php import_tpl("tpl/user/nav.php");?>
        <div class="content">
            <div class="welcome_page">
                <h3>欢迎您回到 品质家生活 官方网店</h3>
                <div class="login_info">
                    <p>您最近登录时间：<time><?php echo $result['last_login'];?></time></p>
                    <p>您的等级是：
                        <?php echo $result['email_check']==1?"注册用户":"未认证用户";?>
                    </p>
                    <p>您的手机已通过认证</p>
                </div>
                <div class="h4_BG"><h4>您的账户</h4></div>
                <div class="user_account">
                    <p><span>余额：</span>￥0.00</p>
                    <p><span>现金券：</span>共计0个，价值￥0.00</p>
                    <p><span>积分：</span><?php echo $result['point'];?>积分</p>
                </div>
                <div class="h4_BG"><h4>用户提醒</h4></div>
                <div class="user_remind">
                    <p>您最近三十天内提交了0个订单</p>
                </div>
            </div>
        </div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













