<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/order_submit.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <?php import_tpl("tpl/user/nav.php");?>
        <div class="content">
            <div class="welcome_page">
            <h3>收货地址</h3>
            <div id="address">
                <div class="normal">
                    <input type="radio" name="address" class="choose" value="0" />
                    <input type="hidden" name="id" id="addressId" class="id"/>
                    <span class="receiver"></span>
                    <span class="province"></span>
                    <span class="city"></span>
                    <span class="district"></span>
                    <span class="street"></span>
                    <span class="postal_code"></span>
                    <span class="mobile_phone"></span>
                    <span class="fixed_phone"></span>
                    <a class="default" href="javascript:">设为默认地址</a>
                    <a class="modify" href="javascript:">编辑</a>
                    <a class="delete" href="javascript:">删除</a>
                    <div class="modify">
                        <input type="hidden" name="id" id="idInput" class="id"/>
                        <label for="receiverInput"><span>*</span>收件人</label>
                        <input type="text" name="receiver" id="receiverInput" class="receiver" placeholder="请填写收件人"/>
                        <br/>
                        <label><span>*</span>收货区域</label>
                        <select name="province" id="provinceInput" class="province"></select>
                        <select name="city" id="cityInput" class="city"></select>
                        <select name="district" id="districtInput" class="district"></select>
                        <br/>
                        <label for="streetInput"><span>*</span>详细地址</label>
                        <input type="text" name="street" id="streetInput" class="street" placeholder="请填写详细地址"/>
                        <br/>
                        <label for="postal_codeInput"><span>*</span>邮编号码</label>
                        <input type="text" name="postal_code" id="postal_codeInput" class="postal_code" placeholder="请填写邮编号码"/>
                        <br/>
                        <label for="mobile_phoneInput"><span>*</span>手机</label>
                        <input type="text" name="mobile_phone" id="mobile_phoneInput" class="mobile_phone" placeholder="请填写手机"/>
                        <span>或</span>
                        <label for="fix_phoneInput"><span>*</span>固定电话</label>
                        <input type="text" name="fixed_phone" id="fixed_phoneInput" class="fixed_phone" placeholder="请填写固定电话"/>
                        <a class="modifyConfirm button" href="javascript:">确认收货地址</a>
                    </div>
                </div>
                <div id="currentAddress"></div>
                <div class="add">
                    <a class="new" href="javascript:">新增地址</a>
                </div>
            </div>
        </div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













