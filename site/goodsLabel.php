<?php //预定义
    $page  = $result['page']['currentPage'];
    $total = $result['page']['totalPage'];
    $page_start = floor($page/10)*10+1;
    $page_end = (ceil($page/10))*10;

    $order_method = $result['order']['method'];
    $order_value  = $result['order']['value']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <title><?php echo $result['title'];?></title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/list.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugin/jquery.cookie.js"></script>
    <script type="text/javascript">
        $.cookie('backUrl',window.location.href,{ path: "/"});
    </script>
    <script type="text/javascript" src="script/goods_list.js"></script>
    <script type="text/javascript" src="script/collect.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
   	<div id="contain">
   		<div class="wrapper">
            <div class="classify_box">
                <div class="sideTitle">
                    <span>收藏网站</span>
                </div>
                <a href="http://www.pzjsh.com" id="collect" title="品质家生活">
                    <img src="style/image/bookmark.png" alt="品质家生活">
                </a>
                <div class="sideTitle">
                    <span>宝贝分类</span>
                </div>
                <ul class="classify">
                <?php foreach ($result['subclasses'] as $key => $value) { ?>
                    <li>
                        <a href="<?php e_page('goods','list',"category_id={$value['id']}");?>"><?php echo $value['name'];?></a>
                    </li>
                <?php } ?>
                </ul>
            </div>
            
            <div class="product_list">
                <div class="top_row">
                    <div class="list_map">
                        <a class="map_1" href="<?php e_page('goods','label',"label={$result['label']['id']}");?>">
                            <?php echo $result['label']['name'];?></a>
                    </div>
                </div>

                <div class="goods_show">
                    <?php foreach ($result['goodslist'] as $i => $value) {?>
                    <div class="goods">
                        <a href="<?php e_page('goods','detail',"gopen_id={$value['gopen_id']}");?>">
                            <img class="show_image" src="<?php echo $result['goodslist'][$i]['picture'];?>" alt="<?php echo $result['goodslist'][$i]['name'];?>">
                        </a>
                        <div class="ProductInfo">
                            <a class="show_name" href="<?php e_page('goods','detail',"gopen_id={$value['gopen_id']}");?>">
                                <?php echo $result['goodslist'][$i]['name'];?></a>
                            
                            <div class="price">
                                <?php if(isset($value['original_price'])&&!empty(['original_price'])){ ?>
                                <span class="origin_price">￥<?php echo $result['goodslist'][$i]['original_price'];?></span>
                                <?php } ?>
                                <p><?php echo $result['goodslist'][$i]['selling_price'];?></p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="page_change">
                    <?php if($page!=1){ ?>
                    <a class="pre_page" href="<?php e_page("goods","label",array("label"=>$result['label']['id'],"page"=>$page-1,"$order_method"=>$order_value));?>"><上一页</a>
                    <?php }else{ ?>
                    <span class="pre_page"><上一页</span>

                    <?php } if($page_start>10){ ?>
                    <a class="page_part_pre" href="<?php e_page("goods","label",array("label"=>$result['label']['id'],"page"=>$page_start-10,"$order_method"=>$order_value));?>">...</a>
                    <?php } ?>
            
                    <?php for($i=$page_start;($i<=$total)&&($i<=$page_end);$i++){//页面数字显示 ?>
                        <a class="page_num" href="<?php e_page("goods","label",array("label"=>$result['label']['id'],"page"=>$i,"$order_method"=>$order_value));?>">
                            <?php echo $i==$page?"[{$i}]":$i;?>
                        </a>
                    <?php } ?>
            
                    <?php if($page_end<$total){ ?>
                        <a class="page_part_next" href="<?php e_page("goods","label",array("label"=>$result['label']['id'],"page"=>$page_start+10,"$order_method"=>$order_value));?>">...</a>
                    <?php } ?>
                    
                    <?php if($page<$total){ ?>
                    <a class="next_page" href="<?php e_page("goods","label",array("label"=>$result['label']['id'],"page"=>$page+1,"$order_method"=>$order_value));?>">下一页></a>
                    <?php }else{echo "<span class='next_page'>下一页></span>";} ?>
                </div>
            </div>
   		</div>
   	</div>
   	<?php import_part("custom.goods","hotShow");?>
    <?php import_part("custom.module","footer");?>
</div>
</body>
</html>














            