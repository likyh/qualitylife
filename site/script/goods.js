$(document).ready(function(){
	cartMode.init(root);
    var gopen_id = $('form .product_name').attr('data-gopenid');

    //平均得分
    $score_tpl = $('form .stars');
    score_val = $score_tpl.attr('data-score');
    $star_active = $score_tpl.find('img.active').remove();
    $star_inactive = $score_tpl.find('.inactive').remove();
    for (var i = 1; i <= score_val; i++) {
        $active_img = $star_active.clone();
        $score_tpl.append($active_img);
    };
    for (var i = 1; i <= (5-score_val); i++) {
        $inactive_img = $star_inactive.clone();
        $score_tpl.append($inactive_img);
    };
    

	$('form .classify a').each(function() {
		$(this).css("outline","thin solid #ccc");
		$(this).attr('class', 'no-selected');
	});
	$('form .classify a').click(function(){
		$('form .classify a').each(function() {
			$(this).css("outline","thin solid #ccc");
			$(this).attr('class', 'no-selected');
		});
		$(this).css('outline','solid #caff4c');
		$(this).attr('class', 'selected');
        $('#input_attr').remove();
        var attribute_id = $(this).parent('.kind').attr('data-attributeid');
        var input_attr = "<input type='hidden' id='input_attr' name='attribute_id' value="+attribute_id+">";
        $('form .classify').append(input_attr);
        var stock = $(this).parent('.kind').attr('data-stock');
        $('#surnumber').text(stock);
		return false;
	});
    //加入愿望单
    function checkWish(){
        wishMode.check(gopen_id,function(d){
            console.log(d);
            if(d.state==200){
                $('.wish_pre').hide();
                $('.wish_sheet .wish_after').addClass('wished').text('已许愿').show();    
            }
            
        });
    }
    checkWish();
    $('.wish_sheet .wish_pre').click(function(event) {
        console.log(gopen_id);
        wishMode.add(gopen_id,function(d){
            console.log(d);
            if(d.state!=200){
                alert(d.message);
                if(d.state==403){
                    $.cookie('backUrl',window.location.href,{ path: "/"});
                    window.location.href = root+'user/login';
                }
            }else{
                checkWish();
            }
        });
        return false;
    });
	$('form .confirm .in_car').click(function() {
        var attribute_id;
        var number;
        var attribute_selected=false;
        $('form .classify a').each(function(){
        	if($(this).attr('class')=='selected'){
        		attribute_selected=true;
        	}
        });
        if(!attribute_selected){
        	alert("请选择产品分类");
            return false;
        }
        attribute_id = $('form .classify a.selected').parent().attr('data-attributeid');
        number = $('#amount').val();
        //alert(gopen_id);
        //alert(attribute_id);
        //alert(number);
        cartMode.add(gopen_id,attribute_id,number);
        return false;
	});
    $('form .confirm .buy_now').click(function() {
    /*
        var gopen_id = $('form .product_name').attr('data-gopenid');
        var province = $('#provinceInput').val();
        var city = $('#cityInput').val();
        var district = $('#districtInput').val();
        var number = $('#amount').val();
        var attribute = $('.classify .selected').parent('.kind').attr('data-attributeid');
        alert('0');
        //return false;
        $.ajax({
            url:root+"order/directBuy",
            type:'POST',
            async:false,
            data: {
                gopen_id     : gopen_id,  
                province     : province,    
                city         : city,      
                district     : district,
                number       : number,      
                attribute_id : attribute_id
            },
            error:function(){
                alert('1');
                return false;
            },
            success:function(){
                alert('2');
                return false;
            }
        });
    */  
        var attribute_selected=false;
        $('form .classify a').each(function(){
            if($(this).attr('class')=='selected'){
                attribute_selected=true;
            }
        });
        if(!attribute_selected){
            alert("请选择产品分类");
            return false;
        }else{
            $('form').submit();
            return false;
        }
    });
    //数量加减操作
    $('form .amount_box .minus').click(function(){
        var number = parseInt($('#amount').val());
        if(number>1) $('#amount').val(number-1);
    });
    $('form .amount_box .plus').click(function(){
        var stock  = parseInt($('#surnumber').text());
        var number = parseInt($('#amount').val());
        if(number<stock) $('#amount').val(number+1);
    });

    //配送地址
    $modify = $('form').find('.trans span');
    var pro;
    var city;
    var str;
    getDefault(function(d){
        if(d.city==undefined){
            setArea($modify.find("#provinceInput"),'0','江苏','province');
            setArea($modify.find("#cityInput"),'19','徐州','city');
            setArea($modify.find("#districtInput"),'1908','徐州','town');
        }else{
            pro  = d.province;
            city = d.city;
            str  = d.district;
            listArea(pro,'province',function(d){data_pro = d;});
            listArea(city,'city',function(d){data_city = d;});
            listArea(str,'town',function(d){data_str= d;});
            setArea($modify.find("#provinceInput"),'0'              ,data_pro.data.name ,'province');
            setArea($modify.find("#cityInput")    ,data_pro.data.id ,data_city.data.name,'city');
            setArea($modify.find("#districtInput"),data_city.data.id,data_str.data.name,'town');
        }
    });

    $modify.find("#provinceInput").change(function(){
        setArea($modify.find("#cityInput"),$(this).find(":selected").data("id"),1,1,function(){
            setArea($modify.find("#districtInput"),$modify.find("#cityInput").find(":selected").data("id"));
        });
    });
    $modify.find("#cityInput").change(function(){
        setArea($modify.find("#districtInput"),$(this).find(":selected").data("id"));
    });

    //商品具体信息切换
    comment_has_start = 0;//评论初始化状态
    
    $comment_tpl = $('.user_comment').remove();
    $('.detail').hide();
    $('.detail_intro').show();
    $('.detail_list').find('a.detail_title').click(function(e){
        $('a.detail_title').removeClass('selected');
        $(this).addClass('selected');
        var page = $(e.target).attr('data-page');
        $('.detail').each(function() {
            if($(this).hasClass(page)){
                $(this).show(); 
            }else{
                $(this).hide();
            }
        });
        
        if(page=='detail_comment'){
            if(comment_has_start==0){
                comment_start(1);
            }
        }
        return false;
    });
    $('#contain > div > div.product_detail > div.detail_list > ul > li:nth-child(2) > a').trigger('click');
    $page_box = $('.comment_pages');
    $page_pre = $('.comment_pages_pre');
    $part_pre = $('.comment_part_pre');
    $page_next= $('.comment_pages_next');
    $part_next= $('.comment_part_next');
    $page_num = $('.comment_pages_num').remove();
    var comment_number = $page_box.attr('data-count');
    var page_per_num = 10;//每页显示评论数
    var page_per_part_num = 5;//页面每段页数
    var page_total_num = Math.ceil(comment_number/page_per_num);//页面总数
    var part_total_num = Math.ceil(page_total_num/page_per_part_num);//段总数
    var comment_current_page = 1;//初始页数为1
    var comment_current_part = 1;//初始段数为1
    var part_page_start = (comment_current_part-1)*page_per_part_num+1;
    var part_page_end   = comment_current_part*page_per_part_num;

    comment_part_list();
    comment_page_set(comment_current_page,comment_current_part);
    $('.comment_pages a.comment_page').click(function(){
        change_page = $(this).attr('data-page-num');
        if(change_page!=comment_current_page){
            $('.user_comment').remove();
            comment_current_page = change_page;
            comment_start(comment_current_page);
            if(comment_current_page>part_page_end){
                comment_current_part += 1;
                comment_part_list();
            }else if(comment_current_page<part_page_start){
                comment_current_part -= 1;
                comment_part_list();
            }
            comment_page_set(comment_current_page,comment_current_part);
        }
    });
    $('.comment_pages a.comment_part').click(function(){
        comment_current_part = $(this).attr('data-part-num');
        comment_part_list();
        comment_current_page = part_page_start;
        comment_page_set(comment_current_page,comment_current_part);
    });

    //图片缩略图
    $('.small_pic').hover(function() {
        var img = $(this).attr('data-img');
        $('.show_pic').hide();
        $('.show_pic_'+img).show();
    }, function() {
        /* Stuff to do when the mouse leaves the element */
    });
    $('.show_pic:first').show();

    //分享列表
    $('#contain > div > div.product > div.top_row > div.share > a').mouseenter(function(event) {
        $('#shareBox').slideDown('300');
        //$(this).hide();
        $('#shareBox').mouseleave(function(subevent) {
            $(this).hide();
        });
    });

    function comment_part_list(){
        $('.comment_pages_num').remove();
        part_page_start = (comment_current_part-1)*page_per_part_num+1;
        part_page_end   = comment_current_part*page_per_part_num;
        for(var i=part_page_start; i<=part_page_end&&i<=page_total_num; i++){
            $page_num_clone = $page_num.clone();
            $part_next.before($page_num_clone);
            $page_num_clone.text(i).attr('data-page-num',i);
        }
    }
    function comment_page_set(comment_current_page,comment_current_part){
        //上一页
        console.log(comment_current_part);
        var page_pre_num = parseInt(comment_current_page)-1;
        if(page_pre_num<1){
            $page_pre.text('已经是第一页');
            $page_pre.attr('data-page-num', 1);
        }else{
            $page_pre.text('<上一页');
            $page_pre.attr('data-page-num', page_pre_num);
        }
        //下一页
        var page_next_num = parseInt(comment_current_page)+1;
        if(page_next_num>page_total_num){
            $page_next.text('已经是最后一页');
            $page_next.attr('data-page-num', page_total_num);
        }else{
            $page_next.text('下一页>');
            $page_next.attr('data-page-num', page_next_num);
        }
        //上一段
        var part_pre_num = parseInt(comment_current_part)-1;
        if(part_pre_num<1){
            $part_pre.text('');
            $part_pre.attr('data-part-num', 1);
        }else{
            $part_pre.text('...');
            $part_pre.attr('data-part-num', part_pre_num);
        }
        //下一段
        var part_next_num = parseInt(comment_current_part)+1;
        if(part_next_num>part_total_num){
            $part_next.text('');
            $part_next.attr('data-part-num', part_total_num);
        }else{
            $part_next.text('...');
            $part_next.attr('data-part-num', part_next_num);
        }

        $('.comment_pages_num').css('color','#535353');
        $('.comment_pages_num[data-page-num='+comment_current_page+']').css('color', '#7d995d');
    }
    //初始化评论区
    function comment_start(comment_page){
        //评论区
        comment_has_start = 1;
        var gopen_id = $('form .product_name').attr('data-gopenid');
        //var comment_page = 1;
        getComment(gopen_id,comment_page,function(d){
            d= d.data || null;
            for(var i in d){
                $comment = $comment_tpl.clone();
                $('.user_comment_box').append($comment);
                $comment.find('.user_name').text(d[i].username).attr('data-userId',d[i].user_id);
                $comment.find('.stars').attr('data-score',d[i].product_quality_score);
                $comment.find('.comment_time').text(d[i].comment_time);
                $comment.find('.comment_content_text').text(d[i].comment);
                if(d[i].reply!=null)
                    $comment.find('.replay_content_main').text(d[i].reply);
                if(d[i].reply_time!=null)
                    $comment.find('.reply_time').text(d[i].reply_time);
            }
            //评价分
            $('.user_comment').each(function(index, el) {
                var $score_tpl = $(this).find('.stars');
                var score_val = $score_tpl.attr('data-score');
                var $star_active = $score_tpl.find('img.active').remove();
                var $star_inactive = $score_tpl.find('.inactive').remove();
                for (var i = 1; i <= score_val; i++) {
                    $active_img = $star_active.clone();
                    $score_tpl.append($active_img);
                }
                for (var i = 1; i <= (5-score_val); i++) {
                    $inactive_img = $star_inactive.clone();
                    $score_tpl.append($inactive_img);
                }
            });
        });
    }
    
});



