$(function(){
	getData = {
		sendCode:function(tel,callback){
			$.ajax({
				url: root+'user/sendCode',
				type: 'GET',
				dataType: 'JSON',
				data: {tel: tel},
				success:callback,
				error:function(){
					alert('响应超时，请重试');
				}
			});
		},
		checkCode:function(tel,code,callback){
			$.ajax({
				url: root+'user/checkCode',
				type: 'GET',
				dataType: 'json',
				data: {
					tel: tel,
					code:code
				},
				success:callback,
				error:function(){
					alert('响应超时，请重试');
				}
			});
		}
	};
	state = {
		$right:$('.code img.active').hide(),
		$wrong:$('.code img.inactive').show(),
		$button:$('.code a.get_code'),
		$telInput:$('#user_tel'),
		$codeInput:$('#user_code'),
		toRight:function(){
			this.$right.show();
			this.$wrong.hide();
		},
		toWrong:function(){
			this.$right.hide();
			this.$wrong.show();
		},
		clickBind:function(){
			this.$button.click(function(){
				var tel = state.$telInput.val();
				if(!(/^1\d{10}$/.test(tel))){
					alert('请输入正确的手机号');
					return false;
				}
				console.log(tel);
				getData.sendCode(tel,function(d){
					console.log(d);
					if(d.data.state){
						state.countTime();
					}else{
						alert(d.data.message);
					}
				});
				
				return false;
			})
		},
		inputBind:function(){
			this.$codeInput.on('input propertychange',function(){
				var code = state.$codeInput.val();
				if(code.length==6){
					var tel = state.$telInput.val();
					getData.checkCode(tel,code,function(d){
						if(d.data){
							state.toRight();
						}else{
							state.toWrong();
						}
					})
				}else{
					state.toWrong();
				}
			});
		},
		countTime:function(){
			var totalTime = 60;//总时间
			time();
			//this.$button.text('重新获取:'+totalTime+'秒');
			this.$button.unbind('click');
			this.$button.css('background-color', '#ddd');
			function time(){
				var t = setTimeout(function(){
					if(totalTime==0){
						clearTimeout(t);
						state.$button.text('获取验证码');
						state.clickBind();
						this.$button.css('background-color', '');
					}else{
						totalTime -= 1;
						state.$button.text('重新获取:'+totalTime+'秒');
						time();
					}
				},1000)
			}
		},
		init:function(){
			this.clickBind();
			this.inputBind();
		}
	};
	state.init();
})