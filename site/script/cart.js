/**
 * Created by linyh on 2014/11/18.
 */
$(document).ready(function(){
    function updateGoods($current, id, data){
        $current.attr("data-id",id);
        $current.attr("data-cart-id",data.id);
        $current.find(".image img").attr("src",data.picture);
        $current.find(".name_box .full_name").text(data.name)
        .parent().attr('href', root+'goods/detail?gopen_id='+data.gopen_id);
        $current.find(".choose input").attr("value",data.id);
        $current.find(".name_box span.model").text(data.attribute);
        updataPrice($current, data);
        $current.find(".wish").attr('data-gopenid',data.gopen_id);
    }
    function updataPrice($current, data){
        $current.find(".price_box .origin_price span").text(data.original_price);
        $current.find(".price_box .selling_price span").text(data.selling_price);
        $current.find(".now_amount input").val(data.number);
        $current.find(".total_box .total").text(data.selling_price*data.number);
        updateTotalPrice();
    }
    function updateTotalPrice(){
        var total=0;
        $productBox.find(".check").each(function(){
            total+=Number($(this).find(".total_box .total").text());
        });
        $cart.find(".add_price .total_price").text(total);
    }
    function goodsCheckToggle($goods,bool){
        var $checkBox=$goods.find(".choose input");
        if(typeof bool=='undefined'){
            $goods.toggleClass("check");
            $checkBox.prop("checked",$goods.hasClass("check"));
        }else{
            if(bool){
                $goods.addClass("check");
            }else{
                $goods.removeClass("check");
            }
            $checkBox.prop("checked",bool);
        }
    }
    function goodsCheckAllToggle(bool){
        $productBox.find(".choosed_product").each(function(k,d){
            goodsCheckToggle($(d),bool);
        });
    }
    function goodsDelete($current){
        $current.find(".price_box .origin_price span").text(data.original_price);
        updateTotalPrice();
    }
    function changeNum(data,number){
        if(parseInt(data.number)==0&&parseInt(number)<0 || parseInt(number)==0){
            // 数量没有变化
        }else{
            data.number=parseInt(data.number)+parseInt(number);
            cartMode.numChange(data.id,data.number);
        }
    }
    function directChangeNum(data,number){
        data.number = number;
        cartMode.numChange(data.id,number);
    }
    var $cart=$("#having_choose");
    var $productBox=$(".choosed_product_box");
    var $productTpl=$productBox.find(".choosed_product");
    var cartData;
    $productTpl.remove();
    cartMode.init(root);
    cartMode.list(function(data){
        if(data&& data.state==200){
            cartData=data.data;
            for(var i in cartData){
                var $current=$productTpl.clone();
                updateGoods($current, i, data.data[i]);
                $productBox.append($current);
            }
            $('.choosed_product_box a.wish').each(function(index, el) {
                console.log($(this));
                checkWish($(this));
            });
        }else{
            alert(data.message);
            userMode.goLoginPage();
        }

        //初始默认
        $(".cart_choose_all").prop('checked', true);
        goodsCheckAllToggle(true);
        updateTotalPrice();
    });
    // 点击按钮修改数量
    $productBox.on("click",".choosed_product .minus, .choosed_product .plus",function(){
        var $current=$(this).closest(".choosed_product");
        var data=cartData[$current.attr("data-id")];
        var number=0;
        if($(this).hasClass("minus")){
            number=-1;
        }else if($(this).hasClass("plus")){
            number=1;
        }
        changeNum(data,number);
        updataPrice($current,data);
    });
    //手动修改数量 
    $productBox.on("input propertychange",".choosed_product .now_amount input",function(){
        var $current=$(this).closest(".choosed_product");
        var data=cartData[$current.attr("data-id")];
        var number = $(this).val();
        
        directChangeNum(data,number);
        updataPrice($current,data);
    });
    // 选择按钮
    $productBox.on("click",".choosed_product .choose input",function(){
        goodsCheckToggle($(this).closest(".choosed_product"));
        updateTotalPrice();
        $(".cart_choose_all").prop('checked', false);
    });
    $(".cart_choose_all").change(function(){
        //console.log($(this).prop("checked"));
        goodsCheckAllToggle($(this).prop("checked"));
        $(".cart_choose_all").prop("checked",$(this).prop("checked"));
        updateTotalPrice();
    });
    // 删除按钮
    $productBox.on("click",".choosed_product .operate_box .delete",function(){
        var $c=$(this).closest(".choosed_product");
        cartMode.remove($c.attr("data-cart-id"));
        $c.remove();
        updateTotalPrice();
        return false;
    });
    $(".move_box .delete").click(function(){
        //alert('a');
        $('.choosed_product.check').each(function() {
            cartMode.remove($(this).attr("data-cart-id"));
            $(this).remove();
        });
        updateTotalPrice();
        return false;
    });

    // 确认按钮
    $(".confirm_box input").click(function(){
        var total=0;
        $productBox.find(".check").each(function(){
            total+=Number($(this).find(".total_box .total").text());
        });
        $cart.find(".add_price .total_price").text(total);
    });

    //愿望单
    function checkWish($wish){
        var gopen_id = $wish.attr('data-gopenid');
        wishMode.check(gopen_id,function(d){
            console.log(d);
            if(d.state==200){
                $wish.text('已许愿').addClass('wished');
                $wish.click(function(event) {
                    return false;
                });
            }
        });
    }
    $('form .choosed_product_box').on('click','.wish',function(e) {
        var gopen_id = $(e.target).attr('data-gopenid');
        wishMode.add(gopen_id,function(d){
            console.log(d);
            if(d.state!=200){
                alert(d.message);
            }else{
                var $box = $(e.target).closest('.choosed_product');
                cartMode.remove($box.attr('data-cart-id'));
                $box.remove();
            }
        });
        return false;
    });
    $('.last_row .wish').on('click',function(){
        $('.wish').each(function(index, el) {
            if(!$(el).hasClass('wished')){
                var gopen_id = $(el).attr('data-gopenid');
                wishMode.add(gopen_id,function(d){
                    console.log(d);
                    if(d.state!=200){
                        alert(d.message);
                    }else{
                        checkWish($(e.target));
                    }
                });
            }
        });
    });
});