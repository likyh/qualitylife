$(document).ready(function() {
	$userBar = $('#userBar');

	//用户条滚动
	function resizeUserBar(event) {
		//console.log($(this).scrollTop());
		if($(this).scrollTop()>$('#ad').height()){
			$userBar.css('position', 'fixed');
			$('.head').css('margin','35px auto 0 auto');
		}else{
			$userBar.css('position', 'static');
			$('.head').css('margin','0 auto 0 auto');
		}
	}
	$(window).scroll(resizeUserBar);
	resizeUserBar();

	//透明图
	$('.textBox').click(function(event) {
		if(!$(this).hasClass('active')){
			var label = $(this).attr('data-label');
			$('.recommendBox').hide();
			$('.recommend_'+label).show();

			$('.textBox').removeClass('active');
			$(this).addClass('active');
		}
	});
	$('.opacityBox').hover(function() {
		$(this).children('.textBox').toggle();
		$(this).addClass('active');
	}, function() {
		$(this).children('.textBox').toggle();
		$(this).removeClass('active');
	});
	
	//收藏
	//var $share = $('.recProduct .share');
	//share.init();
	
	
	var share = {
		$shareButton:$('.recProduct .share'),
		init:function(){
			this.check();
			this.bind();
		},
		check:function(){
			//this.shareButton.each(function(index, el) {
			//	if(el.hasClass('active'))
			//});
			wishMode.list(function(d){
				//console.log(d);
				//console.log(share.$shareButton);
				$(share.$shareButton).each(function(index, el) {
					//console.log(el);
					for (var i = d.data.list.length - 1; i >= 0; i--) {
						if(d.data.list[i].gopen_id==$(el).closest('.recProduct').attr('data-id')){
							share.change($(el));
						}
					};
				});
			});
		},
		bind:function(){
			$(share.$shareButton).click(function(event){
				var gopen_id = $(this).closest('.recProduct').attr('data-id');
				wishMode.add(gopen_id,function(d){
					console.log(d);
					alert(d.message);
					if(d.state==200){
						share.change($(event.target));
					}
				});
			});
		},
		change:function($target){
			$target.addClass('active').text('已收藏');
		}
	};
	//console.log(share);
	share.init();
});