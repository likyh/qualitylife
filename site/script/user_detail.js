$(document).ready(function() {
	userInfoMode.init();
	$('#username_change_confirm').click(function(event) {
		var username = $(this).siblings('#username_change').val();
		if(username==''){
			alert("请输入您要修改的用户名");
			return;
		}
		userInfoMode.editInfo('username',username,function(d){
			if(d.state==200){
				alert('修改成功');
				$('.username_show span').text(username);
			}else{
				alert(d.message);
			}
		});
	});
	$('#tel_change_confirm').click(function(event) {
		var tel = $(this).siblings('#tel_change').val();
		var re = /^1\d{10}$/;
		if(tel==''||!re.test(tel)){
			alert("请输入正确的手机号");
			return;
		}
		userInfoMode.editInfo('tel',tel,function(d){
			if(d.state==200){
				alert('修改成功');
				$('.tel_show span').text(tel);
			}else{
				alert(d.message);
			}
		});
	});
	$('#pass_change_confirm').click(function(event) {
		$input_box = $('.pass_edit');
		var username = $input_box.find('#pass_change_username').val();
		var oldPass  = $input_box.find('#pass_change_oldPass').val();
		var newPass  = $input_box.find('#pass_change_newPass').val();
		var rePass   = $input_box.find('#pass_change_rePass').val();
		if(username==''||oldPass==''||newPass==''||rePass==''){
			alert("请填写完整");
			return;
		}
		if(newPass!=rePass){
			alert("两次填写的新密码不一致");
			return;
		}
		console.log(oldPass);
		userInfoMode.alterPass(username,oldPass,newPass,function(d){
			console.log(d);
			if(d.state==200){
				alert("修改成功，请重新登陆");
				window.location.href=root+"user/login"; 
			}
		});
	});
});