/**
 * Created by linyh on 2014/11/18.
 */

var SimpleDataFormat= function(data,$tpl,except){
    // 数据快速填充函数
    for(var i in data){
        if(typeof except[i]=="undefined"|| except[i]!==true){
            if(typeof data[i]!="undefined"){
                if(data[i]!=null){
                    $tpl.find("."+i).text(data[i]).val(data[i]);    
                }else{
                    $tpl.find("."+i).text('未填写').val('');    
                }
                
            }
        }
    }
};
var commentMode = {
    addComment:function(goods_id,order_id,product,service,comment,callback){
        $.ajax({
            url: root+'userInfo/addComment',
            type: 'GET',
            dataType: 'JSON',
            data: {
                goods_id : goods_id,
                order_id : order_id,
                product  : product ,
                service  : service ,
                comment  : comment 
            },
            success:callback,
            error:function(){
                alert("请求超时");
                console.log('conmentError');
            }
        });
    }
};
var userInfoMode = {
    root:"",
    init:function(root){
        this.root=root;
    },
    editInfo:function(type,newInfo,callback){
        $.ajax({
            url: root+'userInfo/editInfo',
            type: 'POST',
            dataType: 'json',
            data: {
                newInfo:newInfo,
                type:type
            },
            success:callback,
            error:function(){
                alert("请求超时");
                console.log('editError');
            }
        });
    },
    alterPass:function(username,oldPass,newPass,callback){
        $.ajax({
            url: root+'userInfo/alterPass',
            type: 'POST',
            dataType: 'json',
            data: {
                username:username,
                oldPass:oldPass,
                newPass:newPass,
            },
            success:callback,
            error:function(){
                alert("请求超时");
                console.log('passError');
            }
        });
    }
}
var cartMode={
    root:"",
    init:function(root){
        this.root=root;
    },
    list:function(callback){
        var listUrl=root+"cart/list";
        $.ajax({
            url: listUrl,
            dataType:'json',
            success: callback
        });
    },
    remove:function(cartId){
        var url=root+"cart/remove";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                cart_id:cartId
            },
            success: function(d){
                if(d.state!=200){
                    alert(d.message);
                }
            }
        });
    },
    numChange:function(cartId,num){
        var url=root+"cart/numChange";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                cart_id:cartId,
                num:num
            },
            success: function(d){
                if(d.state!=200){
                    alert(d.message);
                }
            }
        });
    },
    add:function(gopen_id,attribute_id,number){
        var url=root+"cart/add";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                gopen_id:gopen_id,
                attribute_id:attribute_id,
                number:number
            },
            success: function(d){
                alert(d.message);
            }
        });
    }
};
var wishMode={
    root:"",
    init:function(root){
        this.root=root;
    },
    list:function(callback){
        var listUrl=root+"wish/list";
        $.ajax({
            url: listUrl,
            dataType:'json',
            success: callback,
            error:function(){
                alert('hehe');
            }
        });
    },
    remove:function(gopen_id){
        var url=root+"wish/remove";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                gopen_id:gopen_id
            },
            success: function(d){
                if(d.state!=200){
                    alert(d.message);
                    console.log('removeInfo');
                }
            }
        });
    },
    add:function(gopen_id,callback){
        var url=root+"wish/add";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                gopen_id:gopen_id,
            },
            success: callback,
            error:function(){
                alert("请求超时");
                console.log('addError');
            }
        });
    },
    check:function(gopen_id,callback){
        var url=root+"wish/checkWish";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                gopen_id:gopen_id,
            },
            success: callback,
            error:function(){
                alert("请求超时");
                console.log('checkError');
            }
        });
    },
};
var userMode={
    root:"",
    init:function(root){
        this.root=root;
    },
    goLoginPage:function(){
        var url=root+"user/login";
        window.location.replace(url);
    }
};
var addressMode={
    userAddress:function(callback){
        var url=root+"address/userAddress";
        $.ajax({
            url: url,
            dataType:'json',
            success: callback
        });
    },
    userAddressAction:function(address_id,receiver,province,city,district,street,postal_code,mobile_phone,fixed_phone,callback){
        //这个就是可增可改的了，暂时就算修改必填也都要填
        var url = root+"address/userAddressAction";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                address_id  :address_id  ,   
                receiver    :receiver    ,
                province    :province    ,
                city        :city        ,
                district    :district    ,
                street      :street      ,
                postal_code :postal_code ,
                mobile_phone:mobile_phone,
                fixed_phone :fixed_phone 
            },
            success: callback
        });
    },
    userSetDefault:function(address_id,callback){
        var url = root+"address/userSetDefault";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                address_id:address_id
            },
            success: callback 
        });
    },
    userDelete:function(address_id,callback){
        var url=root+"address/userDelete";
        $.ajax({
            url: url,
            dataType:'json',
            data:{
                address_id:address_id
            },
            success: callback
        });
    }
};
function setArea(id,pid,name,level,callback){
    $.ajax({
        type:"GET",
        url:root+"address/city",
        dataType:"json",
        data:"pid="+pid,
        success:function(data){
            if(!data.data) return null;
            var $select=$(id).empty();
            for(var i in data.data){
                var d=data.data[i];
                if(d.name==name&&d.level==level){
                    $select.append($("<option selected>"+ d.name+"</option>").val(d.name).data("id",d.id));    
                }else{
                    $select.append($("<option>"+ d.name+"</option>").val(d.name).data("id",d.id));    
                }
            }
            callback();
        },
        error:function(){
            alert("请求超时");
            console.log('setAreaError');
        }
    });
}
function listArea(name,level,callback){
    $.ajax({
        type:"GET",
        url:root+"address/cityList",
        dataType:"json",
        async:false, 
        data:{
            name:name,
            level:level
        },
        success:callback
    });
}
function getDefault(callback){
    $.ajax({
        type:"GET",
        url: root+"address/getDefault",
        dataType: 'json',
        success:callback
    });
}

//获取评论
function getComment(gopen_id,comment_page,callback){
    $.ajax({
        url: root+'goods/comment',
        type: 'GET',
        dataType: 'JSON',
        data: {
            gopen_id:gopen_id,
            comment_page:comment_page
        },
        success:callback,
        error:function(){
            alert("error");
        }
    });
}









