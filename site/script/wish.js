/**
 * Created by linyh on 2014/11/18.
 */
$(document).ready(function(){
    function updateGoods($current, id, data){
        $current.attr("data-id",id);
        $current.find(".image img").attr("src",data.picture);
        $current.find(".name_box .full_name").text(data.goods_name).
        parent().attr('href', root+'goods/detail?gopen_id='+data.gopen_id);
        $current.find(".choose input").attr("value",data.gopen_id);
        $current.find(".origin_price span").text(data.original_price);
        $current.find(".selling_price span").text(data.selling_price);
    }
    function goodsCheckToggle($goods,bool){
        var $checkBox=$goods.find(".choose input");
        if(typeof bool=='undefined'){
            $goods.toggleClass("check");
            $checkBox.prop("checked",$goods.hasClass("check"));
        }else{
            if(bool){
                $goods.addClass("check");
            }else{
                $goods.removeClass("check");
            }
            $checkBox.prop("checked",bool);
        }
    }
    function goodsCheckAllToggle(bool){
        $productBox.find(".choosed_product").each(function(k,d){
            goodsCheckToggle($(d),bool);
        });
    }
    function goodsDelete($current){
        $current.find(".price_box .origin_price span").text(data.original_price);
        updateTotalPrice();
    }
    var $cart=$("#having_choose");
    var $productBox=$(".choosed_product_box");
    var $productTpl=$productBox.find(".choosed_product");
    var cartData;
    $productTpl.remove();
    wishMode.init(root);
    wishMode.list(function(data){
        console.log(data);
        if(data&& data.state==200){
            cartData=data.data.list;
            for(var i in cartData){
                var $current=$productTpl.clone();
                updateGoods($current, i, data.data.list[i]);
                $productBox.append($current);
            }
        }else{
            alert(data.message);
            userMode.goLoginPage();
        }

        //初始默认
        $(".cart_choose_all").prop('checked', true);
        goodsCheckAllToggle(true);
    });
    // 选择按钮
    $productBox.on("click",".choosed_product .choose input",function(){
        goodsCheckToggle($(this).closest(".choosed_product"));
        updateTotalPrice();
        $(".cart_choose_all").prop('checked', false);
    });
    $(".cart_choose_all").change(function(){
        //console.log($(this).prop("checked"));
        goodsCheckAllToggle($(this).prop("checked"));
        $(".cart_choose_all").prop("checked",$(this).prop("checked"));
        updateTotalPrice();
    });
    // 删除按钮
    $productBox.on("click",".choosed_product .operate_box .delete",function(){
        var $c=$(this).closest(".choosed_product");
        wishMode.remove($c.find('.choose input').val());
        $c.remove();
        return false;
    });
    $(".move_box .delete").click(function(){
        //alert('a');
        $('.choosed_product.check').each(function() {
            var $c=$(this).closest(".choosed_product");
            wishMode.remove($c.find('.choose input').val());
            $c.remove();
            $(this).remove();
        });
        return false;
    });

    // 确认按钮
    $(".confirm_box input").click(function(){
        var total=0;
        $productBox.find(".check").each(function(){
            total+=Number($(this).find(".total_box .total").text());
        });
        $cart.find(".add_price .total_price").text(total);
    });
});