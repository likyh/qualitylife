var addressView={
    $addressTpl:null,
    $modifyTpl:null,
    addressArray:{},
    init:function($addressBox,$addressTpl,$modifyTpl){
        this.$addressTpl=$addressTpl;
        this.$modifyTpl=$modifyTpl;
        $addressBox.on("click",".normal span",this.choose);
        $addressBox.on("click",".normal a.modify",this.modifyStart);
        $addressBox.on("click",".normal a.modifyConfirm",this.modifyConfirm);
        $addressBox.on("click",".normal a.default",this.setDefault);
        $addressBox.on("click",".normal a.delete",this.deleteAddress);
        $('#address .add a.new').click(this.addAdressStart);
    },
    add:function(data){
        var $tmp=this.$addressTpl.clone();
        SimpleDataFormat(data,$tmp,{"default":true});
        $tmp.data("data",data);
        $tmp.find('.choose').val(data.id);
        if(data.default==1){
            $tmp.addClass("default");
            $tmp.find(".default").hide();
        }else{
            $tmp.removeClass("default");
            $tmp.find(".default").show();
        }
        this.addressArray[data.id]=$tmp;
        //console.log($tmp);
        return $tmp;
    },
    choose:function(e){
        $(e.target).closest(".normal").find("input.choose").prop("checked",true);
    },
    cityChange:function($modify){
        if($modify.parent().hasClass('add')){
            setArea($modify.find("#provinceInput"),'0','江苏','province');
            setArea($modify.find("#cityInput"),'19','徐州','city');
            setArea($modify.find("#districtInput"),'1908','徐州','town');
        }else{
            var pro  = $modify.siblings('.province').text();
            var city = $modify.siblings('.city').text();
            var str  = $modify.siblings('.district').text();
            listArea(pro,'province',function(d){
                data_pro = d;
            });
            listArea(city,'city',function(d){
                data_city= d;
            });
            listArea(str,'town',function(d){
                data_str= d;
            });
            console.log(data_pro);
            setArea($modify.find("#provinceInput"),'0'              ,data_pro.data.name ,'province');
            setArea($modify.find("#cityInput")    ,data_pro.data.id ,data_city.data.name,'city');
            setArea($modify.find("#districtInput"),data_city.data.id,data_str.data.name,'town');
        }
        
        $modify.find("#provinceInput").change(function(){
            setArea($modify.find("#cityInput"),$(this).find(":selected").data("id"),1,1,function(){
                setArea($modify.find("#districtInput"),$modify.find("#cityInput").find(":selected").data("id"));
            });
        });
        $modify.find("#cityInput").change(function(){
            setArea($modify.find("#districtInput"),$(this).find(":selected").data("id"));
        });
    },
    addAdressStart:function(e){
        if($(e.target).siblings('div.modify').size()==0){
            $tmp = addressView.$modifyTpl.clone();
            $(e.target).parent('.add').append($tmp);
            $('#address .add .modifyConfirm').click(addressView.modifyConfirm);

            var $modify = $(e.target).siblings('div.modify');
            addressView.cityChange($modify);
        }
    },
    modifyStart:function(e){
        var $current=$(e.target).closest(".normal");
        if($current.find("div.modify").size()==0){
            $current.siblings().find("div.modify").remove();
            var $tmp=addressView.$modifyTpl.clone();
            SimpleDataFormat($current.data("data"),$tmp,{"default":true});
            $current.append($tmp);

            var $modify =$current.find('div.modify');
            addressView.cityChange($modify);
        }else{
            $current.find("div.modify").remove();
        }
    },
    modifyConfirm:function(e){
        var $current = $(e.target).closest("div.modify").remove();
        
        var address_id     = $current.find('#idInput')          .val();
        var receiver       = $current.find('#receiverInput')    .val();
        var province       = $current.find('#provinceInput')    .val();
        var city           = $current.find('#cityInput')        .val();
        var district       = $current.find('#districtInput')    .val();
        var street         = $current.find('#streetInput')      .val();
        var postal_code    = $current.find('#postal_codeInput') .val();
        var mobile_phone   = $current.find('#mobile_phoneInput').val();
        var fixed_phone    = $current.find('#fixed_phoneInput') .val();
        addressMode.userAddressAction(address_id,receiver,province,city,district,street,postal_code,mobile_phone,fixed_phone,function(d){
            if(d.state!=200){
                alert(d.message);
            }else{
                addressView.refresh();
            }
        });
    },
    setDefault:function(e){
        var $current = $(e.target).closest(".normal");
        var address_id = $current.find('#addressId').val();
        addressMode.userSetDefault(address_id,function(d){
            if(d.state!=200){
                alert(d.message);
            }else{
                $current.siblings().removeClass('default')
                .find('.default').show();
                $current.addClass('default').
                find('input.choose').prop("checked",true)
                .parent().find('.default').hide();
                alert("修改成功");
            }
        });
            
    },
    deleteAddress:function(e){
        var $current = $(e.target).closest(".normal");
        var address_id = $current.find('#addressId').val();
        addressMode.userDelete(address_id,function(d){
            if(d.state==200){
                $current.remove();
            }else{
                alert(d.state);
            }
        });
    },
    refresh:function(){
        var $address=$("#address");
        var $addressBox=$address.find("#currentAddress");
        var $modifyTpl=$address.find("div.modify").remove();
        var $addressTpl=$address.find("div.normal").remove();
        addressMode.userAddress(function(d){
            if(d && d.state==200){
                for(var i in d.data){
                    $addressBox.append(addressView.add(d.data[i]));
                }
            }
        });
    }
};
$(document).ready(function(){
    var $address=$("#address");
    var $addressBox=$address.find("#currentAddress");
    var $modifyTpl=$address.find("div.modify").remove();
    var $addressTpl=$address.find("div.normal").remove();
    var modify = $('#container').attr('data-modify');
    addressMode.userAddress(function(d){
        if(d && d.state==200){
            console.log(d);
            addressView.init($addressBox, $addressTpl, $modifyTpl);
            for(var i in d.data){
                $addressBox.append(addressView.add(d.data[i]));
            }
        }
        $('#currentAddress .default').find('.choose').prop('checked', true);
        var addressId = $('#address').attr('data-address');
        $('#currentAddress .normal').each(function(index, el) {
            if($(this).find('.id').val()==addressId){
                $(this).find('.choose').prop('checked', true);
                if(modify==0){
                    $(this).find('.modify').trigger('click');
                    $('#address .add').remove();
                    $('.sub_order').remove();
                    $('.button').remove();
                    $('input,select,textarea').attr('disabled','disabled');
                    $('a.modify,a.delete').remove();
                }
            }else{
                if(modify==0){
                    $(this).remove();
                }
            }
        });

        
    });
});

