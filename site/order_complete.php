<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <title>订单提交成功 -品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/backet.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/cart.js"></script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <div class="wrapper">
        <div class="progress_box">
            <div class="progress progress_3">
                <div class="step-1">
                    <span>1.我的购物车</span>
                </div>
                <div class="step-2">
                    <span>2.填写核对订单信息</span>
                </div>
                <div class="step-3 complete">
                    <span>3.成功提交订单</span>
                </div>
                <div class="step-4">
                    <span>4评价订单</span>
                </div>
            </div>
        </div>
    </div>
    <div id="compeletFrame">
        <span class="submit">订单提交成功</span>
        <span>您可以<a href="<?php e_page('home','index');?>">返回首页</a>继续购物，或前往<a href="#">个人中心</a>查看已买到商品</span>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>	
</body>
</html>