
<?php //预定义
    $page  = $result['page']['currentPage'];
    $total = $result['page']['totalPage'];
    $page_start = floor($page/10)*10+1;
    $page_end = (ceil($page/10))*10;

    $order_method = $result['order']['method'];
    $order_value  = $result['order']['value']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <title><?php echo $result['title'];?></title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/list.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugin/jquery.cookie.js"></script>
    <script type="text/javascript">
        $.cookie('backUrl',window.location.href,{ path: "/"});
    </script>
    <script type="text/javascript" src="script/goods_list.js"></script>
</head>
<body>
<?php import_part("custom.module","header"); ?>
<div id="container">
   	<div id="contain">
   		<div class="wrapper">
            <div class="classify_box">
                <div class="sideTitle">
                    <span>收藏商城</span>
                </div>
                <img src="style/image/bookmark.png" alt="#">
                <ul class="classify">
                    <div class="sideTitle">
                    <span>宝贝分类</span>
                    </div>
                <?php foreach ($result['subclasses'] as $key => $value) { ?>
                    <li>
                        <a href="<?php e_page('goods','list',"category_id={$value['id']}");?>"><?php echo $value['name'];?></a>
                    </li>
                <?php } ?>
                </ul>
            </div>
            <div class="product_list">
                <div class="top_row">
                    <div class="list_map">
                        <?php //var_dump($result); ?>
                        <?php foreach ($result['class'] as $key => $value) { ?>
                        <a class="map_<?php echo $key;?>" href="<?php e_page('goods','list',"category_id={$value['id']}");?>">
                            <?php echo $value['name'];?></a>
                        <?php } ?>
                    </div>
                    <div class="sorting">
                        <h3>排序方式：</h3>
                        <div class="default_li">
                            <a class="default" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page));?>">默认</a>
                        </div>
                        <div class="hot_li">
                            <a class="hot" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page,"volume"=>'1'));?>">人气</a>
                            <img class="up_image" src="style/image/down.png" alt="上升图标">
                        </div>
                        <div class="sort_way_li">
                            <a class="new" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page,"create_time"=>'1'));?>">
                            <span>新品</span>
                            <img class="up_image" src="style/image/down_black.png" alt="上升图标">
                            </a>
                        </div>
                        <div class="price_sort_li">
                            <a class="price_button" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page,"selling_price"=>'1'));?>">价格</a>
                            <img class="up_image" src="style/image/down_black.png" alt="向下的按钮">
                        </div>
                    </div>
                </div>

                <div class="goods_show">
                    <?php if($result['goodslist']){?>
                        <?php foreach ($result['goodslist'] as $i => $value) {?>
                        <div class="goods">
                            <a href="<?php e_page('goods','detail',"gopen_id={$value['gopen_id']}");?>">
                                <img class="show_image" src="<?php echo $result['goodslist'][$i]['picture'];?>" alt="<?php echo $result['goodslist'][$i]['name'];?>">
                            </a>
                            <div class="ProductInfo">
                            <a class="show_name" href="<?php e_page('goods','detail',"gopen_id={$value['gopen_id']}");?>">
                            <?php echo $result['goodslist'][$i]['name'];?></a>
                            <?php if(isset($value['original_price'])&&!empty($value['original_price'])){ ?>
                            <span class="origin_price">￥<?php echo $result['goodslist'][$i]['original_price'];?></span>
                            <?php } ?>
                            <p><?php echo $result['goodslist'][$i]['selling_price'];?></p>
                            </div>
                        </div>
                        <?php } ?>
                <?php }?>
                </div>

                <div class="page_change">
                    <?php if($page!=1){ ?>
                    <a class="pre_page" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page-1,"$order_method"=>$order_value));?>"><上一页</a>
                    <?php }else{ ?>
                    <span class="pre_page">上一页</span>

                    <?php } if($page_start>10){ ?>
                    <a class="page_part_pre" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page_start-10,"$order_method"=>$order_value));?>">...</a>
                    <?php } ?>
            
                    <?php for($i=$page_start;($i<=$total)&&($i<=$page_end);$i++){//页面数字显示 ?>
                    <?php if($i==$page){ ?>
                        <a class="page_num current" href="javascript:"><?php echo $i;?></a>
                    <?php }else{ ?>
                        <a class="page_num" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$i,"$order_method"=>$order_value));?>"><?php echo $i;?></a>
                    <?php } ?>
                    <?php } ?>
            
                    <?php if($page_end<$total){ ?>
                        <a class="page_part_next" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page_start+10,"$order_method"=>$order_value));?>">...</a>
                    <?php } ?>
                    
                    <?php if($page<$total){ ?>
                    <a class="next_page" href="<?php e_page("goods","list",array("category_id"=>$result['category_id'],"page"=>$page+1,"$order_method"=>$order_value));?>">下一页></a>
                    <?php }else{echo "<span class='next_page'>下一页></span>";} ?>
                </div>
            </div>
   		</div>
   	</div>
   	<?php import_part("custom.goods","hotShow");?>
    <?php import_part("custom.module","footer");?>
</div>
</body>
</html>














            