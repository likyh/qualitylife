<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>品质家生活</title>
    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/index.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/mode.js"></script>
    <script type="text/javascript" src="script/index.js"></script>
</head>
<body>
	<div id="ad">
		<img class="headerImg" src="<?php echo $result['copywriting']['picurl'];?>" />
	</div>
	<?php import_part("custom.module","header"); ?>
	<div id="container">
		<div class="couponInfo">
			<?php for ($i=1; $i <=3 ; $i++) { ?>
			<div class="coupon coupon_<?php echo $i;?>">
				<div class="couponNumber">
					<div class="couponNumberBig">
						NO.<?php echo $i;?>
					</div>
					<div class="couponNumberInfo">
						ANNOUNCEMENT
					</div>
				</div>
				<div class="couponContent">
					<div class="couponContentBox">
						<h3><?php echo $result["copywriting"]["title".$i];?></h3>
						<p><?php echo $result["copywriting"]["text".$i];?></p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="secKill">
			<?php foreach ($result["scrolling"] as $key => $value) { ?>
			<div class="secKillProduct secKillProduct_<?php echo $key+1;?>">
				<a href="<?php echo $value['url'];?>"><img src="<?php echo $value['picurl'];?>" /></a>
			</div>
			<?php } ?>
		</div>
		<div class="personalPop">
			<div class="headLine">
				<h2><img src="style/image/title4.png"></h2>
				<a class="moreProducts" href="<?php e_page('goods','label',array('id'=>$result['label_1']['id']));?>">更多</a>
			</div>
			<div class="productPics">
				<div class="part_1 part">
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][0]['gopen_id']));?>">
						<div class="productPicture productPicture_1" style="background-image: url(<?php echo $result['label_1']['goods'][0]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][0]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="part_2 part">
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][1]['gopen_id']));?>">
						<div class="productPicture productPicture_2" style="background-image: url(<?php echo $result['label_1']['goods'][1]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][1]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][2]['gopen_id']));?>">
						<div class="productPicture productPicture_3" style="background-image: url(<?php echo $result['label_1']['goods'][2]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][2]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][3]['gopen_id']));?>">
						<div class="productPicture productPicture_4" style="background-image: url(<?php echo $result['label_1']['goods'][3]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][3]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="part_3 part">
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][4]['gopen_id']));?>">
						<div class="productPicture productPicture_5" style="background-image: url(<?php echo $result['label_1']['goods'][4]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][4]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][5]['gopen_id']));?>">
						<div class="productPicture productPicture_6" style="background-image: url(<?php echo $result['label_1']['goods'][5]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][5]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][6]['gopen_id']));?>">
						<div class="productPicture productPicture_7" style="background-image: url(<?php echo $result['label_1']['goods'][6]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][6]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="part_4 part">
					<a href="<?php e_page('goods','detail',array('gopen_id'=>$result['label_1']['goods'][7]['gopen_id']));?>">
						<div class="productPicture productPicture_8" style="background-image: url(<?php echo $result['label_1']['goods'][7]['picture'];?>);">
							<div class="opacityBox">
								<div class="textBox">
									<p><?php echo $result['label_1']['goods'][7]['name'];?></p>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<?php import_part("custom.goods","hotShow");?>
		<div class="recommend">
			<div class="options">
				<div class="textBox" data-label="1"><h3>本月最新上架新品促销中</h3></div>
				<div class="textBox active" data-label="2"><h3>年度人气宝贝，向量冠军！</h3></div>
				<div class="textBox" data-label="3"><h3>镇店之宝，掌柜强力推荐</h3></div>
			</div>
			<div class="recommendBox recommend_1">
				<?php foreach ($result['label_1']['goods'] as $key => $value) { ?>
					<div class="recProduct" data-id="<?php echo $value['gopen_id'];?>">
						<a href="<?php e_page('goods','detail',array('gopen_id'=>$value['gopen_id']));?>">
							<img src="<?php echo $value['picture'];?>" />
						</a>
						<h4><?php echo $value['name'];?></h4>
						<div class="pricePart">
							<div class="priceBox">￥<span class="priceNum"><?php echo $value['selling_price'];?></span>元</div>
							<a class="buyNow" href="<?php e_page('goods','detail',array('gopen_id'=>$value['gopen_id']));?>">立即购买</a>
						</div>
						<div class="soldPart">
							<div class="soldBox">已售<span class="soldNum">77</span>笔</div>
							<a class="share">收藏</a>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="recommendBox recommend_2">
				<?php foreach ($result['label_2']['goods'] as $key => $value) { ?>
					<div class="recProduct" data-id="<?php echo $value['gopen_id'];?>">
						<a href="<?php e_page('goods','detail',array('gopen_id'=>$value['gopen_id']));?>">
							<img src="<?php echo $value['picture'];?>" />
						</a>
						<h4><?php echo $value['name'];?></h4>
						<div class="pricePart">
							<div class="priceBox">￥<span class="priceNum"><?php echo $value['selling_price'];?></span>元</div>
							<a class="buyNow" href="<?php e_page('goods','detail',array('gopen_id'=>$value['gopen_id']));?>">立即购买</a>
						</div>
						<div class="soldPart">
							<div class="soldBox">已售<span class="soldNum">77</span>笔</div>
							<a class="share">收藏</a>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="recommendBox recommend_3">
				<?php foreach ($result['label_3']['goods'] as $key => $value) { ?>
					<div class="recProduct" data-id="<?php echo $value['gopen_id'];?>">
						<a href="<?php e_page('goods','detail',array('gopen_id'=>$value['gopen_id']));?>">
							<img src="<?php echo $value['picture'];?>" />
						</a>
						<h4><?php echo $value['name'];?></h4>
						<div class="pricePart">
							<div class="priceBox">￥<span class="priceNum"><?php echo $value['selling_price'];?></span>元</div>
							<a class="buyNow" href="<?php e_page('goods','detail',array('gopen_id'=>$value['gopen_id']));?>">立即购买</a>
						</div>
						<div class="soldPart">
							<div class="soldBox">已售<span class="soldNum">77</span>笔</div>
							<a class="share">收藏</a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php import_part("custom.module","footer"); ?>
	</div>
</BODY>
</HTML>