<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <title>品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/login.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
</head>
<body>
<div id="container" class="login">
    <div id="userbar">
        <div class="wrapper">
            <?php import_part("Custom.module","userBar");?>
        </div>
    </div>
    <div id="contain">
        <div class="wrapper">
            <div class="main_box">
                <div id="login_showText">
                    <p>美好·优质·生活</p>
                    <p>品质家生活</p>
                </div>
                <div class="log_box">
                    <div class="log_or_reg">
                        <a class="selected" href="<?php e_page("user","login");?>">登陆</a>
                        <a class="" href="<?php e_page("user","register");?>">注册</a>
                    </div>
                    <div class="input">
                        <form action="<?php e_page("user","loginSubmit");?>" class="log_form" method="POST">
                            <label for="user_username">手机号码/邮箱</label>
                            <input type="text" id="user_username" name="loginName" placeholder="请输入你的手机号码或者邮箱">
                            <label for="user_passwprd">密码</label>
                            <input type="password" id="user_passwprd" name="password" placeholder="请输入你的密码">
                            <input type="submit" class="confirm" value="登陆">
                        </form>
                        <a class="code_forget" href="#">忘记密码？</a>
                    </div>
                    <div class="other_way" style="display: none;">
                        <h3>其他方式登陆</h3>
                        <div class="other_box">
                            <a class="qq" href="#"><img src="style/image/qq.png"></a>
                            <a class="weibo" href="#"><img src="style/image/weibo.png"></a>
                            <a class="taobao" href="#"><img src="style/image/taobao.png"></a>
                            <a class="zhifubao" href="#"><img src="style/image/zfb.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>