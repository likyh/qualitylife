<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <title>品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/index.css"/>
    <link rel="stylesheet" type="text/css" href="banner/banner.css"/>
    <script src="plugin/jquery-1.10.2.min.js"></script>
    <script src="banner/banner.js"></script>
</head>
<body>
<div id="container">
    <div id="userbar">
        <div class="wrapper">
            <?php import_part("Custom.module","userBar");?>
        </div>
    </div>
    <div id="logo">
        <div class="wrapper">
            <img src="style/image/title.png" alt="品质生活">
        </div>
    </div>
    <div id="header">
        <div class="wrapper">
            <div id="nav">
                <div id="smallLogo"><img src="style/image/menu_logo.png" alt="品质生活logo"/></div>
                <h2>分类导航</h2>
                <div class="navBox">
                    <ul class="nav">
                        <li class="car"><a href="<?php e_page('goods','list',"category_id=1");?>">有车族</a></li>
                        <li class="food"><a href="<?php e_page('goods','list',"category_id=2");?>">美食汇</a></li>
                        <li class="baby"><a href="<?php e_page('goods','list',"category_id=3");?>">婴童房</a></li>
                        <li class="home"><a href="<?php e_page('goods','list',"category_id=4");?>">生活家</a></li>
                        <li class="space"><a href="<?php e_page('article','news');?>">微空间</a></li>
                    </ul>
                </div>
            </div>
            <div class="recommend">
                <?php import_part("Custom.module","search");?>
                <div class="picBox likyh-banner">
                    <ul class="banner-slides">
                        <li title="标题1">
                            <a href="链接">
                                <img src="p1.jpg"/>
                            </a>
                        </li>
                        <li title="标题1">
                            <a href="链接">
                                <img src="p2.jpg"/>
                            </a>
                        </li>
                        <li title="标题1">
                            <a href="链接">
                                <img src="p1.jpg"/>
                            </a>
                        </li>
                        <li title="标题1">
                            <a href="链接">
                                <img src="p2.jpg"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user">
                <div class="icon"><img src="style/image/user.png" alt="用户头像"/></div>
                <div class="operate">
                    <div class="intro">
                        <?php if($r['loged']){ ?>
                        <p>
                            欢迎您，<br />
                            <a href="<?php e_page("userInfo","index");?>"><?php echo $_SESSION['user']['info']['username'];?></a>
                            <br>
                        </p>
                        <?php }else{ ?>
                        <p>
                            亲，请<a href="<?php e_page("user","login");?>">登录</a><br>
                            享受更多服务
                        </p>
                        <?php } ?>
                    </div>
                    <div class="button">
                        <ul class="button_list">
                            <?php if($r['loged']){ ?>
                            <li class="log"><a href="<?php e_page("user","logout");?>">退出登录</a></li>
                            <?php }else{ ?>
                            <li class="log"><a href="<?php e_page("user","login");?>">登陆</a></li>
                            <li class="reg"><a href="<?php e_page("user","register");?>">注册</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="discount">
                        <div class="present">
                            <div class="option1">
                                <a href="#">礼品券</a>
                            </div>
                            <div class="option2">
                                <a href="#">优惠券</a>
                            </div>
                            <div class="option3">
                                <a href="#">新品热卖</a>
                            </div>
                            <div class="option4">
                                <a href="#">优惠促销</a>
                            </div>
                        </div>
                        <div class="free">
                            <ul class="free_info">
                                <li class="free free_1">
                                    <span>十五天无理由退换货</span>
                                </li>
                                <li class="free free_2">
                                    <span>购物满169免运费</span>
                                </li>
                                <li class="free free_3">
                                    <span>满500元成为会员</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php import_part("Custom.home","goodsShow");?>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="service">
        <div class="wrapper">
            <?php import_part("Custom.article","service");?>
        </div>
    </div>
    <div id="link_icon">
        <div class="wrapper">
            <?php import_part("Custom.module","linkIcon");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













