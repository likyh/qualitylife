<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <base href="<?php echo $system['siteRoot'];?>" />
    <title>个人信息 - 品质生活</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/user.css"/>
    <script type="text/javascript" src="plugin/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugin/jquery.cookie.js"></script>
    <script type="text/javascript">
        $.cookie('backUrl',window.location.href,{ path: "/"});
    </script>
</head>
<body>
<?php import_part("custom.module","header");?>
<div id="container">
    <?php import_tpl("tpl/user/header.php");?>
    <div id="content">
        <div class="user_info_nav_box">
            <h2><?php echo $result['detail']['type'];?></h2>
            <ul class="ser_info_nav">
                <?php
                foreach ($result['list'] as $key => $value) {
                ?>
                <li><a href="<?php e_page('article','index',array('id'=>$value['id']));?>">
                    <?php echo $value['title'];?>
                </a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="content">
            <h3><?php echo $result['detail']['title'];?></h3>
            <p class="content_info">
                <?php echo $result['detail']['date'];?>
            </p>
            <p class="content_main">
                <?php echo $result['detail']['content'];?>
            </p>
        </div>
    </div>
    <div id="product_hot" class="product_show">
        <div class="wrapper">
            <?php import_part("Custom.goods","hotShow");?>
        </div>
    </div>
    <div id="footer">
        <div class="wrapper">
            <?php import_part("Custom.module","footer");?>
        </div>
    </div>
</div>
</body>
</html>













