<footer>
    <div class="bottomList">
        <ul>
            <li>
                <a href="<?php e_page('home','index');?>">店铺首页</a>
                <a href="<?php e_page('userInfo','complaint');?>">信誉评价</a>
                <a href="<?php e_page('article','index',array('id'=>1));?>">关于我们</a>
            </li>
        </ul>
        <a class="backToTop" href="#">返回顶部</a>
    </div>
    <div class="bottomTips">
        <?php for ($i=4; $i <= 6; $i++) {  ?>
        <div class="tipList">
            <h4><?php echo $result['title'.$i];?></h4>
            <p><?php echo $result['text'.$i];?></p>
        </div>
        <?php } ?>
        <div class="friendLink">
            <h4>友情链接</h4>
            <p><a href="http://m.kuaidi100.com" target="_blank">快递查询</a></p>
        </div>
        <div class="powerBy">
            <h4>技术支持</h4>
            <p>Powered By LikyhStudio</p>
        </div>
    </div>
    <script type="text/javascript">
        $('a.backToTop').click(function(){
            $('html body').animate({scrollTop: 0}, 500);
            return false;
        });
    </script>
</footer>