<div class="hotSelling">
    <div class="headLine">
        <h2><img src="style/image/title2.png"></h2>
        <a class="moreProducts" href="<?php e_page('goods','label',array('id'=>$result['id']));?>">更多</a>
    </div>
    <?php foreach ($result['label_hot'] as $key => $value) { ?>
        <div class="hotProduct hotProduct_<?php echo $key+1;?>">
            <a class="hotShow" href="<?php e_page("goods","detail",array('gopen_id'=>$value['gopen_id']));?>">
                <img class="hotProductPic" src="<?php echo $value['picture'];?>" />
            </a>
            <div class="hotProductInfo">
                <p><?php echo $value['name'];?></p>
                <div class="price">
                    RMB:￥
                    <span class="priceNumber"><?php echo $value['selling_price'];?></span>
                </div>
            </div>
        </div>
    <?php } ?>
</div>