    <div id="product_groupBuy" class="product_show">
        <div class="wrapper">
            <div class="head">
                <h3>团购</h3>
                <a class="more" href="<?php e_page("goods","label","label={$result['label_1']['id']}");?>">更多&gt;</a>
            </div>
            <div class="main">
                <div class="goods_show">
                    <?php foreach ($result['label_1']['goods'] as $i=>$value) {?>
                    <div class="goods order-<?php echo $i;?>">
                        <a href="<?php e_page("goods","detail","gopen_id={$value['gopen_id']}");?>">
                            <img class="show_image" src="resource/<?php echo $value['picture'];?>" alt="<?php echo $value['name'];?>">
                        </a>
                        <a class="show_name" href="<?php e_page("goods","detail","gopen_id={$value['gopen_id']}");?>"><?php echo $value['name'];?></a>
                        <div class="price"><?php echo $value['selling_price'];?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>    
    <div id="product_secKill" class="product_show">
        <div class="wrapper">
            <div class="head">
                <h3>秒杀</h3>
                <a class="more" href="<?php e_page("goods","label","label={$result['label_2']['id']}");?>">更多</a>
            </div>
            <div class="main">
                <div class="goods_show">
                    <?php foreach ($result['label_2']['goods'] as $i => $value) {?>
                    <div class="goods order-<?php echo $i;?>">
                        <a href="<?php e_page("goods","detail","gopen_id={$value['gopen_id']}");?>">
                            <img class="show_image" src="resource/<?php echo $value['picture'];?>" alt="<?php echo $value['name'];?>">
                        </a>
                        <a class="show_name" href="<?php e_page("goods","detail","gopen_id={$value['gopen_id']}");?>"><?php echo $value['name'];?></a>
                        <div class="price"><?php echo $value['selling_price'];?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div id="product_timeBuy" class="product_show">
        <div class="wrapper">
            <div class="head">
                <h3>限时购</h3>
                <a class="more" href="<?php e_page("goods","label","label={$result['label_3']['id']}");?>">更多</a>
            </div>
            <div class="main">
                <div class="goods_show">
                    <?php foreach ($result['label_3']['goods'] as $i => $value) {?>
                    <div class="goods order-<?php echo $i;?>">
                        <a href="<?php e_page("goods","detail","gopen_id={$value['gopen_id']}");?>">
                            <img class="show_image" src="resource/<?php echo $value['picture'];?>" alt="<?php echo $value['name'];?>">
                        </a>
                        <a class="show_name" href="<?php e_page("goods","detail","gopen_id={$value['gopen_id']}");?>"><?php echo $value['name'];?></a>
                        <div class="price"><?php echo $value['selling_price'];?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>