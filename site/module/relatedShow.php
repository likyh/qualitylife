<div class="sideTitle">
    <span>相关商品</span>
</div>
<div class="relatedGoods">
    <ul>
    <?php foreach ($result['label_hot'] as $key => $value) { ?>
        <li>
            <a href="<?php e_page("goods","detail",array('gopen_id'=>$value['gopen_id']));?>">
                <img src="<?php echo $value['picture'];?>">
                <?php echo $value['name'];?>
                <span class="price"><?php echo $value['selling_price'];?>元</span>
            </a>
        </li>
    <?php } ?>
    </ul>
</div>