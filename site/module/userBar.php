<div id="userBar">
    <div class="user_operate">
        <?php if($r['loged']){ ?>
        <span>欢迎您，</span>
        <a href="<?php e_page("userInfo","index");?>"><?php echo $_SESSION['user']['info']['username'];?></a>
        <a href="<?php e_page("user","logout");?>">退出登录</a>
        <?php }else{ ?>
        <a class="login" href="<?php e_page("user","login");?>">登陆</a>
        <a href="<?php e_page("user","register");?>">注册</a>
        <?php } ?>
    </div>
    <div class="user_info">
        <a href="<?php e_page("home","index");?>">
            <span>主页</span>
        </a>
        <a href="<?php e_page("cart","index");?>">
            <img class="buy_car" src="style/image/shoppingCart.png">
            <span>购物车</span>
        </a>
        <a href="<?php e_page("wish","index");?>">
            <span>收藏夹</span>
        </a>
        <a target="_blank" href="<?php e_page('userInfo','complaint');?>">
            <span>投诉建议</span>
        </a>
        <a href="<?php e_page('article','index',array('id'=>5));?>">
            <span>消息订阅</span>
        </a>
    </div>
</div>