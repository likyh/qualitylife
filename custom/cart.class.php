<?php
import("custom.data.userActionMode");
import("custom.data.cartMode");
class cart extends Activity{
    /** @var  userActionMode */
    protected $userAction;
    /** @var  cartMode */
    protected $cart;
	protected function __construct() {
		$this->userAction = userActionMode::init();
        $this->cart=cartMode::init();
	}

    function indexTask(){
        View::displayAsHtml(array(),"basket.php");
    }

    function listTask(){
        $userInfo=$this->userAction->getUserInfo();
        if(empty($userInfo)){
            $result=array('message'=>"未登录",'state'=>403);
        }else{
            $userId=$userInfo['id'];
            $data=$this->cart->userCart($userId);
            $result=array('message'=>"成功",'state'=>200,'data'=>$data);
        }
        View::displayAsJson($result);
    }

    function addToCart(){
        if(!(isset($_GET['gopen_id'])&&isset($_GET['attribute_id'])&&isset($_GET['number']))){
            return array('message'=>"信息不完整",'state'=>400);
        }
        $userInfo=$this->userAction->getUserInfo();
        if(empty($userInfo)){
            return array('message'=>"未登录",'state'=>403);
        }
        $userId=$userInfo['id'];
        $gopen_id=$_GET['gopen_id'];
        $attribute_id=$_GET['attribute_id'];
        $number=$_GET['number'];
        if($this->cart->addToCart($userId,$gopen_id,$attribute_id,$number)){
            return array('message'=>"添加成功",'state'=>200);
        }else{
            return array('message'=>"库存不足",'state'=>500);
        }
    }
    function addTask(){
        $result=$this->addToCart();
        View::displayAsJson($result);
    }

    function numChangeTask(){
        if($this->userAction->checkLogin()){
            $cartId=isset($_GET['cart_id'])?$_GET['cart_id']:0;
            $num=isset($_GET['num'])?$_GET['num']:0;
            $message=$this->cart->numberModify($cartId,$num);
            if($message=='success'){
                $result=array('message'=>$message,'state'=>200);
            }else{
                $result=array('message'=>$message,'state'=>500);
            }
        }else{
            $result=array('message'=>"未登录",'state'=>403);
        }
        View::displayAsJson($result);
    }

    function removeFromCart(){
        if(!isset($_GET['cart_id'])){
            return array('message'=>"信息不完整",'state'=>400);
        }
        if(!$this->userAction->checkLogin()){
            return array('message'=>"未登录",'state'=>403);
        }
        if($this->cart->removeFromCart($_GET['cart_id'])){
            return array('message'=>"删除成功",'state'=>200);
        }else{
            return array('message'=>"删除失败",'state'=>500);
        }
    }
    function removeTask(){
        $result=$this->removeFromCart();
        View::displayAsJson($result);
    }
}
