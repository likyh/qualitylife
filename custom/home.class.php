<?php
import("custom.data.goodsInfoMode");
import("Custom.Data.categoryMode");
import("custom.data.userActionMode");
import("custom.data.copywritingMode");
import("custom.data.pictureMode");
class home extends Activity{
    /** @var  goodsInfoMode */
    protected $goodsInfo;
    /** @var  pictureMode */
    protected $picture;
    /** @var  userActionMode */
    protected $userAction;
    /** @var  copywritingMode */
    protected $copywriting;

	protected function __construct() {
		$this->goodsInfo = goodsInfoMode::init();
		$this->picture   = pictureMode::init();
		$this->userAction= userActionMode::init();
		$this->copywriting = copywritingMode::init();
	}
	function indexTask(){
		//登陆
		$result['loged']=userActionMode::init()->checkLogin();
		//商品
		$category=categoryMode::init();
        $goodsInfo=$this->goodsInfo;
		$label_1 = $result['label_1']['id'] = 1;//首页显示标签的种类ID
		$label_2 = $result['label_2']['id'] = 2;
		$label_3 = $result['label_3']['id'] = 3;
		$sort = 1;//desc
		$orderBy = 'volume';//销量排序
		$result['label_1']['goods'] = $goodsInfo->brief($goodsInfo->getList(8,$offset=0,$category_id=null,$label_1,$orderBy,$sort));
		$result['label_2']['goods'] = $goodsInfo->brief($goodsInfo->getList(4,$offset=0,$category_id=null,$label_2,$orderBy,$sort));
		$result['label_3']['goods'] = $goodsInfo->brief($goodsInfo->getList(9,$offset=0,$category_id=null,$label_3,$orderBy,$sort));
		//广告
		$result['copywriting'] = $this->copywriting->get();
		$result['scrolling'] = $this->picture->scrollingList();
		//var_dump($result);

		View::displayAsHtml($result,"index.php");
	}

	//module~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	function goodsShowTask(){//三段产品展示
		$category=categoryMode::init();
        $goodsInfo=$this->goodsInfo;
		$label_1 = $result['label_1']['id'] = 1;//首页显示标签的种类ID
		$label_2 = $result['label_2']['id'] = 2;
		$label_3 = $result['label_3']['id'] = 3;
		
		$rows = 7;//单块显示个数
		
		$sort = 1;//desc
		$orderBy = 'volume';//销量排序
		$result['label_1']['goods'] = $goodsInfo->brief($goodsInfo->getList($rows,$offset=0,$category_id=null,$label_1,$orderBy,$sort));
		$result['label_2']['goods'] = $goodsInfo->brief($goodsInfo->getList($rows,$offset=0,$category_id=null,$label_2,$orderBy,$sort));
		$result['label_3']['goods'] = $goodsInfo->brief($goodsInfo->getList($rows,$offset=0,$category_id=null,$label_3,$orderBy,$sort));

		View::displayAsHtml($result,"module/index_goodsShow.php");
	}
}




