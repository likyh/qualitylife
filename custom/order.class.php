<?php 
import("custom.data.orderMode");
import("custom.data.cartMode");
import("custom.data.userActionMode");
import("custom.data.addressMode");
import("custom.data.couponMode");
class order extends Activity {
	/** @var  orderMode*/
	protected $order;
	/** @var  cartMode*/
	protected $cart;
	/** @var  userActionMode*/
	protected $user;
    /** @var  addressMode*/
    protected $address;

    protected $coupon;

	protected function __construct(){
		$this->order=orderMode::init();
		$this->cart=cartMode::init();
		$this->user=userActionMode::init();
        $this->address=addressMode::init();
        $this->coupon=couponMode::init();
	}
    //1未确认、3未发货(确定订单)、4已发货、5交易完成
	//订单提交
	function detailTask() {
		$web=WebRouter::init();
        $user=$this->user->getUserInfo();
        if(empty($user)){
            $this->tipState("请登录",$web->getPage("user","login"));
            return;
        }
        $id=$_GET['id'];
        $result=$this->order->detail($id);
        if($result['user_id']!=$user['id']){
            echo "订单错误";
            return;
        }
        //优惠券
        //$result['coupon'] = $this->coupon->enabelCoupon($user['id'],$result['payment_amount']);
        //$result['discount'] = 0.00;
        //foreach ($result['coupon'] as $key => $value) {
        //    $result['discount'] += $value['value'];
        //}
        if($result['state']>2&&$result['state']<6){
            $result['modify'] = 0;
        }else{
            $result['modify'] = 1;
        }
        //var_dump($result);
		View::displayAsHtml($result,"order_submit.php");
	}

    function completeTask() {
        $web=WebRouter::init();
        $user=$this->user->getUserInfo();
        if(empty($user)){
            $this->tipState("请登录",$web->getPage("user","login"));
            return;
        }

        $order_id   = isset($_POST['order_id'])&&!empty($_POST['order_id'])?(int)$_POST['order_id']:-1;
        $original_state      = $this->order->getState($order_id);
        if($original_state>3&&$original_state<6){
            $this->tipState('该订单已确认，不可修改', $web->getPage("userInfo","order"));
            return;
        }
        $data['address_id'] = isset($_POST['address'])&&!empty($_POST['address'])?(int)$_POST['address']:0;
        if(!$this->address->checkComplete($data['address_id'])){
            $this->tipState('地址信息不完整');
            return;
        }
        $data['state']      = 3;
        foreach ($data as $key => $value) {
            if(empty($value)){
                //var_dump($key);
                $this->tipState('信息不完整');
                return;
            }
        }
        $data['remark']     = isset($_POST['remark'])&&!empty($_POST['remark'])?$_POST['remark']:null;
        if(!$this->order->updateOrder($order_id,$data)){
            $this->tipState('修改失败');
        }
        View::displayAsHtml(array(),"order_complete.php");
    }
    //function debugTask(){
    //    var_dump($this->address->checkComplete($_GET['address_id']));
    //}
    function create() {
        $web=WebRouter::init();
        if(!isset($_POST['choose'])||empty($_POST['choose'])){
            return array("订单为空",$web->getPage("cart","index"));
        }
        $userInfo=$this->user->getUserInfo();
        if(empty($userInfo)){
            return array("请登录",$web->getPage("user","login"));
        }
        //用户id
        $userId=$userInfo['id'];
        //获取默认地址
        $defaultAddress = $this->address->getDefault($userId);
        if(!empty($defaultAddress)){
            $address = $defaultAddress['id'];
        }else{
            $address = 0;
        }
        $r=$this->order->create($userId,$_POST['choose'],$address,$freight=0);
        list($state,$message)=$r;
        if($state){
            return array("创建订单成功，在确认价格以后就可以支付了",$web->getPage("order","detail","id=$message"));
        }else{
            return array($message,$web->getPage("cart","index"));
        }
    }
	//订单生成
	function createTask() {
        list($result['message'],$result['url'])=$this->create();
		View::displayAsHtml($result,"state/state.php");
	}
    //直接购买
    function directBuy() {
        $web=WebRouter::init();
        $userInfo=$this->user->getUserInfo();
        if(empty($userInfo)){
            return array("请登录",$web->getPage("user","login"));
        }
        //if(!isset($_POST['choose'])||empty($_POST['choose'])){
        //    return array("订单为空",$web->getPage("cart","index"));
        //}
        $data['user_id']      = $userInfo['id'];
        $data['gopen_id']     = isset($_POST['gopen_id'])&&!empty($_POST['gopen_id'])?$_POST['gopen_id']:'no';
        $data['province']     = isset($_POST['province'])&&!empty($_POST['province'])?$_POST['province']:'no';
        $data['city']         = isset($_POST['city'])&&!empty($_POST['city'])?$_POST['city']:'no';
        $data['district']     = isset($_POST['district'])&&!empty($_POST['district'])?$_POST['district']:'no';
        $data['number']       = isset($_POST['amount'])&&!empty($_POST['amount'])?$_POST['amount']:'no';
        $data['attribute_id'] = isset($_POST['attribute_id'])&&!empty($_POST['attribute_id'])?$_POST['attribute_id']:'no';
        $data['freight']      = '0'; //待定
        $data['receiver']     = null;
        $data['street']       = null;
        $ifComplete = 1;
        //var_dump($data);
        foreach ($data as $key => $value) {
            if($value=='no'){
                $ifComplete = 0;
            }
        }
        if($ifComplete==0){
            return array("信息不完整");
        }
        $addressInfo = $this->address->userSameAddress($data);
        if(!$addressInfo){
            $addressData = $data;
            unset($addressData['gopen_id']);
            unset($addressData['number']);
            unset($addressData['attribute_id']);
            unset($addressData['freight']);
            $this->address->create($addressData);//这个不需要完整，成功率不确定
            $addressInfo = $this->address->userSameAddress($addressData);
            $this->address->setDefault($userInfo['id'],$addressInfo['id']);
            $data['address_id'] = $addressInfo['id'];
        }else{
            $data['address_id'] = $addressInfo['id'];
            $this->address->setDefault($userInfo['id'],$addressInfo['id']);
        }

        $r=$this->order->directCreate($data);
        list($state,$message)=$r;
        if($state){
            return array("success",$web->getPage("order","detail","id=$message"));
        }else{
            return array($message,'');
        }
    }
    function directBuyTask() {
        list($result['message'],$result['url'])=$this->directBuy();
        View::displayAsHtml($result,"state/state.php");
    }
    function tipState($message, $url=null){
        $result['message']=$message;
        $result['url']=$url;
        View::displayAsHtml($result,"state/state.php");
        exit();
    }
}




