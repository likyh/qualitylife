<?php 
import("custom.data.attributeMode");
import('custom.admin.adminBase');
	class attribute extends adminBase
	{
		protected $attribute;
		protected $cms;
		protected function __construct()
		{
			parent::__construct();
			$this->attribute=attributeMode::init();
			$this->cms=CmsView::init();
			//$this->cms->setControlFile("tpl/admin/attribute/control.json");
			$this->cms->setPageTitle("属性管理");
		}
		


		//商品查看属性
		public function getGoodsAttrTask(){
			$gopen_id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			$result['attrs']=$this->attribute->goodsAttribute($gopen_id);
			$result['id']=$gopen_id;
			$this->cms->tableScene($result,"tpl/admin/attribute/list.php");
			
		}

		//加载属性修改界面
		public function updateTask(){
			$id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			$result['attr']=$this->attribute->getOneAttr($id);
			$this->cms->formScene($result,"tpl/admin/attribute/modify.php");
		}

		public function updateAfterTask(){
			if(isset($_POST['submit']))
			{
				$id=$_POST['id'];
				$gopen_id=(int)$_POST['gopen_id'];
				$name=$_POST['name'];
				$stock=(int)$_POST['stock'];
				$bool=$this->attribute->modify($id,$gopen_id,$name,$stock);
				if ($bool) {
					$result[0]='succ';
					$result[1]="修改成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}
		}


		


		//加载在商品列表新增属性页面
		public function addGoodsAttrTask(){
			$result['id']=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			$this->cms->formScene($result,"tpl/admin/attribute/add.php");
		}


		//新增属性
		public function addTask(){
			$gopen_id=(int)$_POST['gopen_id'];
			$name=$_POST['name'];
			$stock=(int)$_POST['stock'];
			$bool=$this->attribute->add($gopen_id,$name,$stock);
			if ($bool) {
				$result[0]='succ';
				$result[1]="增加成功";
				$this->cms->formScene($result,"tpl//admin/result.php");
			}else{
				$result[0]='';
				$this->cms->formScene($result,"tpl//admin/result.php");
			}
		}


		//删除属性
		public function deleteTask(){
			if(isset($_GET['id'])){
				$id=(int)$_GET['id'];
				$bool=$this->attribute->delete($id);
				if ($bool) {
					$result[0]='succ';
					$result[1]="删除成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}
		}




	}
 ?>