<?php
import('Custom.Data.adminActionMode');
class adminBase extends Activity {
    /** @var CmsView */
    protected $cms;
    /** @var  adminActionMode */
    protected $admin;
    protected function __construct() {
        parent::onStart();
        $this->admin=adminActionMode::init();
        $this->cms=CmsView::init();
        $this->checkLogin();
    }

    function checkLogin(){
       if(!$this->admin->checkLogin()){
           $web=WebRouter::init();
           header("Location:".$web->getPage("admin","login"));
           exit();
       }

       //$this->cms->setUserName($this->admin->getUserInfo()['username']);
    }
}