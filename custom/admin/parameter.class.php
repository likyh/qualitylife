<?php
	import("custom.data.parameterMode");
	import('custom.admin.adminBase');
	/**
	  * 商品参数
	  */
	class parameter extends adminBase
	{
		protected $cms;
		protected $parameter;
		protected function __construct()
		{
			parent::__construct();
			$this->cms=CmsView::init();
			$this->parameter=parameterMode::init();
			$this->cms->setPageTitle("商品参数");
			$this->cms->setControlFile("tpl/admin/parameter/control.json");
		}

		
		
		//删除
		function deleteTask()
		{
			$message="";
			if(isset($_GET['submitted']))
			{
				if(!empty($_GET['id']))
				{
					$id=$_GET['id'];
					$bool=$this->parameter->delete($id);
					if($bool)
						$message="删除成功";
					else
						$mwssage="删除失败";
				}
				else
					$message="参数id不能为空";
			}
			$result['message']=$message;
			$this->cms->formScene($result,"tpl/admin/parameter/delete.php");
		}



		//加载参数管理界面
		public function updateTask(){
			$id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			$result['parameter']=$this->parameter->goodsParameter($id);
			$this->cms->formScene($result,"tpl/admin/parameter/modify.php");
			
		}


		//参数修改
		public function updateAfterTask(){
			if(isset($_POST['submit']))
			{	
				$id=$_POST['id'];
				if(!empty($_POST['brand']))
					$data['brand']=$_POST['brand'];
				if(!empty($_POST['no']))
					$data['no']=$_POST['no'];
				if(!empty($_POST['listing_date']))
					$data['listing_date']=$_POST['listing_date'];
				if(!empty($_POST['material']))
					$data['material']=$_POST['material'];
				if(!empty($_POST['season']))
					$data['season']=$_POST['season'];
				if(!empty($_POST['color']))
					$data['color']=$_POST['color'];
				if(!empty($_POST['storage']))
					$data['storage']=$_POST['storage'];
				if(!empty($_POST['guarantee']))
					$data['guarantee']=$_POST['guarantee'];
				if(!empty($_POST['origin']))
					$data['origin']=$_POST['origin'];
				if(!empty($_POST['packaging']))
					$data['packaging']=$_POST['packaging'];
				if(!empty($_POST['volume']))
					$data['volume']=$_POST['volume'];
				if(!empty($_POST['usage']))
					$data['usage']=$_POST['usage'];
				$bool=$this->parameter->modify($id,$data);
				if ($bool) {
					$result[0]='succ';
					$result[1]="修改成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}


		}


		//加载商品参数增加页面
		public function addTask(){
			$id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			if($this->parameter->checkExist($id)){
				exit('此商品已经存在参数，不必再添加，直接点击 参数管理 即可');
			}else{
				$result['gopen_id']=$id;
				$this->cms->formScene($result,"tpl/admin/parameter/add.php");
			}
		}


		//商品增加
		public function addAfterTask(){
			if (isset($_POST['submit'])) {
				$data['gopen_id']=$_POST['gopen_id'];
				$data['brand']=$_POST['brand'];
				$data['no']=$_POST['no'];
				$data['listing_date']=$_POST['listing_date'];
				$data['material']=$_POST['material'];
				$data['season']=$_POST['season'];
				$data['color']=$_POST['color'];
				$data['storage']=$_POST['storage'];
				$data['guarantee']=$_POST['guarantee'];
				$data['origin']=$_POST['origin'];
				$data['packaging']=$_POST['packaging'];
				$data['volume']=$_POST['volume'];
				$data['usage']=$_POST['usage'];
				$bool=$this->parameter->add($data);
				
				if ($bool) {
					$result[0]='succ';
					$result[1]="增加成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}

			
		}


	} 
 ?>