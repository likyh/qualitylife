<?php
import('custom.data.adminActionMode');
import('custom.data.adminInfoMode');
import('custom.admin.adminBase');
// 管理员管理
class adminUser extends adminBase {
	/** @var CmsView*/
	protected $cms;
	/** @var  adminActionMode */
	protected $admin;
	/**@var adminInfoMode*/
	protected $adminInfo;
	protected function __construct(){
		parent::__construct();
		$this->cms=CmsView::init();
		$this->admin=adminActionMode::init();
		$this->adminInfo=adminInfoMode::init();
		$this->cms->setControlFile("tpl/admin/adminAction/control.json");
        $this->cms->setPageTitle("管理员");
	}

	// 显示管理员信息
	function listTask(){
		$admin=$this->adminInfo->Alladmin();
		$result['admin']=$admin;
		$this->cms->tableScene($result,"tpl/admin/adminAction/showadmin.php");
	}
	// 修改密码
	function alterPassTask(){
		$message="";
		if(isset($_POST['submitted']))
		{
			$adminId=$_POST['adminId'];
			$username=$_POST['username'];
			$oldPass=$_POST['oldPass'];
			$newPass=$_POST['newPass'];
			$bool=$this->admin->alterPass($adminId,$username,$oldPass,$newPass);
			if($bool)
				$message="修改成功";
			else
				$message="用户名或密码错误";
		}
		$result['message']=$message;
		$this->cms->formScene($result,"tpl/admin/adminAction/alterPass.php");
	}
	// 添加管理员
	function addTask(){
		$message="";
		if(isset($_POST['submit'])){
			$username=$_POST['username'];
			$password=$_POST['pass'];
			$bool=$this->admin->addadmin($username,$password,$type);
			if($bool) {
				$message = "添加成功";
			}else {
				$message = "添加失败";
			}
		}
		$result['message']=$message;
		$this->cms->formScene($result,"tpl/admin/adminAction/add.php");
	}
	// 修改密码以外的信息
	function modifyTask(){
		$message="";
		if(isset($_POST['submitted'])){
			$id=$_POST['adminId'];
			if(!empty($_POST['username']))
				$data['username']=$_POST['username'];
			if(!empty($_POST['tel']))
				$data['tel']=$_POST['tel'];
			if(!empty($_POST['email']))
				$data['email']=$_POST['email'];
			$bool=$this->admin->modify($id,$data);
			if($bool)
				$message="修改成功";
			else
				$message="修改失败";
		}
		$result['message']=$message;
		$this->cms->formScene($result,"tpl/admin/adminAction/modify.php");
	}
	// 修改权限
	function typeModifyTask(){
		$message="";
		if(isset($_POST['submitted'])){
			$id=$_POST['adminId'];
			$type=$_POST['type'];
			$bool=$this->admin->typeModify($id,$type);
			if($bool)
				$message="修改成功";
			else
				$message="修改失败";
		}
		$result['message']=$message;
		$this->cms->formScene($result,"tpl/admin/adminAction/typemodify.php");
	}
	// 删除管理员
	function deleteTask(){
		$message="";
		if(isset($_POST['submitted']))
		{
			$id=$_POST['adminId'];
			$bool=$this->admin->delete($id);
			if($bool)
				$message="修改成功";
			else
				$message="修改失败";
		}
		$result['message']=$message;
		$this->cms->formScene($result,"tpl/admin/adminAction/delete.php");
	}
}
