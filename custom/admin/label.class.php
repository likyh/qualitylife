<?php 
	import("custom.data.labelMode");
	import('custom.admin.adminBase');
	/**
	 * 商品标签
	 */
	class label extends adminBase
	{
		protected $cms;
		protected $label;
		protected $goodsInfo;
		protected function __construct()
		{
			parent::__construct();
			$this->cms=CmsView::init();
			$this->label=labelMode::init();
			$this->cms->setPageTitle("商品标签");
			$this->cms->setControlFile("tpl/admin/label/control.json");
		}

		//显示标签
		function showTask()
		{
			$message="";


			$total=$this->label->getLabeslToatl();
			$url=WebRouter::init()->getQuestion("page=");
			$page=isset($_GET['page'])?(int)$_GET['page']:1;
			 $result['page']=new Page($page,$total,10,$result['goods']);
			 $result['page']->setPageCallback(function($page)use($url){
			        return $url.$page;
			 });
			 $rows=($page-1)*10;
			if(isset($_GET['submitted']))
			{
				$id=$_GET['submitted'];
				$bool=$this->label->delete($id);
				if($bool)
					$message="修改成功";
				else
					$message="修改失败";	
			}
			$label=$this->label->allLabel($rows,10);
			foreach ($label as $key => $value) {
				$label[$key]['edit']='<input type="submit" name="edit" id="edit" value="编辑" />';
				$label[$key]['edit'].='<input type="hidden" name="submitted" value="'.$value['id'].'" />';
				$label[$key]['delete']='<input type="submit" name="delete" id="delete" value="删除"/>';
				$label[$key]['delete'].='<input type="hidden" name="submitted" value="'.$value['id'].'" />';
			}
			$result['label']=$label;
			$result['message']=$message;
			$this->cms->tableScene($result,"tpl/admin/label/show.php");
		}

		//添加标签
		function addTask()
		{
			$message="";
			if(isset($_GET['submitted']))
			{
				$name=$_GET['name'];
				$message=$this->label->add($name);
			}
			$result['message']=$message;
			$this->cms->formScene($result,"tpl/admin/label/add.php");
		}
		//修改类别
		function modifyTask()
		{
			$message="";
			if(isset($_GET['submitted']))
			{
				$id=$_GET['id'];
				$name=$_GET['name'];
				$bool=$this->label->modify($id,$name);
				if($bool)
					$message="修改成功";
				else
					$message="修改失败";
			}
			$result['message']=$message;
			$this->cms->formScene($result,"tpl/admin/label/modify.php");
		}
		//删除
		function deleteTask()
		{
			$message="";
			if(isset($_GET['submitted']))
			{
				$id=$_GET['id'];
				$bool=$this->label->delete($id);
				if($bool)
					$message="修改成功";
				else
					$message="修改失败";
			}
			$result['message']=$message;
			$this->cms->formScene($result,"tpl/admin/label/delete.php");
		}

		//编辑
		function editTask()
		{
			$id=$_GET['submitted'];
			//echo $id;
			$label=$this->label->detail($id);
			//var_dump($label);
			$result['label']=$label;
			$this->cms->formScene($result,"tpl/admin/label/edit.php");
		}

		//
		function modifyoneTask()
		{
			$message="";
			if(isset($_GET['submitted']))
			{
				$id=$_GET['submitted'];
				$name=$_GET['name'];
				$bool=$this->label->modify($id,$name);
				if($bool)
					$message="修改成功";
				else
					$message="修改失败";
			}
			$result['message']=$message;
			header("location:".getAppInfo()->webRouter->getPage("label","show"));
		}
	}
 ?>