<?php
import('custom.data.goodsActionMode');
import('custom.data.goodsInfoMode');
import('custom.data.categoryMode');
import('custom.data.labelMode');
import('custom.data.pictureMode');
import('custom.data.serviceMode');
import('custom.data.buyreadMode');
import('custom.data.deliveryMode');
import('custom.admin.adminBase');
class goods extends adminBase{
	/** @var CmsView */
    protected $cms;
    /** @var  goodsActionMode */
    protected $goods;
    /** @var  goodsInfoMode */
    protected $goodsInfo;
    /** @var labelMode*/
    protected $label;
    /** @var pictureMode*/
    protected $picture;

    private $service;

    private $buyread;

    private $delivery;
    //protected $category;
    protected function __construct(){
    	parent::__construct();
    	$this->cms=CmsView::init();
    	$this->goods=goodsActionMode::init();
           $this->goodsInfo=goodsInfoMode::init();
           $this->category=categoryMode::init();
           $this->label=labelMode::init();
           $this->picture=pictureMode::init();
           $this->service=serviceMode::init();
           $this->delivery=deliveryMode::init();
           $this->buyread=buyreadMode::init();
    	$this->cms->setControlFile("tpl/admin/goods/control.json");
    	$this->cms->setPageTitle("商品管理");
    }

    
     
    /**
     *删除商品
     */
    function deleteTask(){
        $message=array();
        $data=(int)$_GET['id'];
        if($this->goods->delete($data)){
            $message['result']="删除成功";
        }else{
            $message['result']="删除失败";
        }
        header("Location:".WebRouter::init()->getPage("goods","list"));
    } 




    //加载增加商品页面
    public function addTask(){
        $result['category']=$this->category->getGoodsCategory();
        $result['label']=$this->label->getGoodsLabel();
        $result['service']=$this->service->show();
        $result['delivery']=$this->delivery->show();
        $result['buyread']=$this->buyread->show();
        $result['picture'] = $this->picture->allPicture();
        $this->cms->normalScene($result,"tpl/admin/goods/add.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    //增加商品
    public function addAfterTask(){
        if (isset($_POST['submit'])) {
            $data=array();
            $data['name']=$_POST['name'];
            $data['selling_price']=$_POST['selling_price'];
            $data['original_price']=$_POST['original_price'];
            $data['category_id']=$_POST['category'];
            //$data['picture_ids']=$_POST['picture_ids'];
            $data['detail']=$_POST['detail'];
            $data['stock']=$_POST['stock'];
            $data['volume']=$_POST['volume'];
            if ($_POST['label']) {
                $data['label_ids']=implode(",",$_POST['label']);
            }
            if ($_POST['picture']) {
                $data['picture_ids']=implode(",",$_POST['picture']);
            }
            $data['delivery_id']=$_POST['delivery'];
            $data['service_id']=$_POST['service'];
            $data['buyread_id']=$_POST['buyread'];
            $bool=$this->goods->create($data);
            if ($bool) {
                $result['message']='增加成功';
                $result['url']=WebRouter::init()->getAction('list');
                View::displayAsHtml($result,"plugin/state/tips.php");
            }else{
                $result['message']='增加失败';
                View::displayAsHtml($result,"plugin/state/tips.php");
            }
        }
    }



    //加载修改页面
    public function updateTask(){
        $id=(int)$_GET['id'];
        $result['category']=$this->category->getGoodsCategory();
        $result['label']=$this->label->getGoodsLabel();
        $result['service']=$this->service->show();
        $result['delivery']=$this->delivery->show();
        $result['buyread']=$this->buyread->show();
        $result['picture'] = $this->picture->allPicture();
        $result['good']=$this->goods->getOneGood($id);
        $result['good']['label_ids']=explode(',', $result['good']['label_ids']);
        $result['good']['picture_ids']=explode(',', $result['good']['picture_ids']);
        $this->cms->normalScene($result,"tpl/admin/goods/update.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    
    //商品修改
    public function updateAfterTask(){
        if (isset($_POST['submit'])) {
            $data=array();
            $gopen_id=$_POST['gopen_id'];
            $data['gopen_id']=$_POST['gopen_id'];
            $data['name']=$_POST['name'];
            $data['selling_price']=$_POST['selling_price'];
            $data['original_price']=$_POST['original_price'];
            $data['category_id']=$_POST['category'];
            //$data['picture_ids']=$_POST['picture_ids'];
            $data['detail']=$_POST['detail'];
            $data['stock']=$_POST['stock'];
            $data['volume']=$_POST['volume'];
            if ($_POST['label']) {
                $data['label_ids']=implode(",",$_POST['label']);
            }
            if ($_POST['picture']) {
                $data['picture_ids']=implode(",",$_POST['picture']);
            }
            $data['delivery_id']=$_POST['delivery'];
            $data['service_id']=$_POST['service'];
            $data['buyread_id']=$_POST['buyread'];
            $bool=$this->goods->modify($gopen_id,$data);
            if ($bool) {
                $result['message']='修改成功';
                $result['url']=WebRouter::init()->getAction('list');
                View::displayAsHtml($result,"plugin/state/tips.php");
            }else{
                $result['message']='修改失败';
                View::displayAsHtml($result,"plugin/state/tips.php");
            }
        }
    }


  // //商品列表
    public function listTask(){
        $total=$this->goodsInfo->getGoodsToatl();
        $url=WebRouter::init()->getQuestion("page=");
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $result['page']=new Page($page,$total,10,$result['goods']);
        $result['page']->setPageCallback(function($page)use($url){
            return $url.$page;
        });
        $rows=($page-1)*10;
        $result['goods']=$this->goodsInfo->getListGoods($rows,10);
        $this->cms->tableScene($result,"tpl/admin/show_information/showGoods.php");
    }

    //上下架
    public function is_upTask(){
        if ($_GET['action']=='up') {
            if(isset($_GET['id'])){
                $id=(int)$_GET['id'];
                $this->goods->up($id);
                $result['message']='上架成功';
                View::displayAsHtml($result,"plugin/state/tips.php");
            }
        }elseif ($_GET['action']=='down') {
            if(isset($_GET['id'])){
                $id=(int)$_GET['id'];
                $this->goods->down($id);
                $result['message']='下架成功';
                View::displayAsHtml($result,"plugin/state/tips.php");
            }
        }


    }











}
