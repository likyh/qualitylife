<?php 
	import("custom.data.categoryMode");
	import("custom.data.goodsInfoMode");
	import('custom.admin.adminBase');
	/**
	 * 商品种类
	 */
	class category extends adminBase
	{
		protected $cms;
		protected $category;
		//protected $goodsInfo;
		protected function __construct()
		{
			parent::__construct();
			$this->cms=CmsView::init();
			$this->category=categoryMode::init();
			//$this->goodsInfo=goodsInfoMode::init();
			$this->cms->setPageTitle("商品分类");
			$this->cms->setControlFile("tpl/admin/category/control.json");
		}

		//显示类别
		function listTask()
		{
			$category=$this->category->getMainCategory();
			$result['category']=$category;
			$this->cms->tableScene($result,"tpl//admin/category/list.php");
		}

		
		
		//删除
		function deleteTask(){
			if(isset($_GET['id'])){
				$id=(int)$_GET['id'];
				$bool=$this->category->delete($id);
				if ($bool) {
					$result[0]='succ';
					$result[1]="删除成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}
		}


		//查看子类
		public function showChlldTask(){
			$id=is_numeric($_GET['id']) ? $_GET['id'] : 1;
			$childCategory=$this->category->showChlld($id);
			$result['childCategory']=$childCategory;
			$this->cms->tableScene($result,"tpl//admin/category/showChild.php");
		}

		//加载增加子类界面
		public function addChildTask(){
			$id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			$result['id']=$id;
			$this->cms->formScene($result,"tpl//admin/category/addChild.php");
		}

		//增加子类
		public function addChildAfterTask(){
			if (isset($_POST['submit'])) {
				$pid=$_POST['id'];
				$name=$_POST['name'];
				$bool=$this->category->addChild($pid,$name);
				if ($bool) {
					$result[0]='succ';
					$result[1]="增加子类成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
				
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}
		}

		//加载增加主类页面
		public function addTask(){
			$this->cms->formScene($result,"tpl//admin/category/add.php");
		}

		//增加主类
		public function addAfterTask(){
			if (isset($_POST['submit'])) {
				$name=$_POST['name'];
				$bool=$this->category->add($name);
				if ($bool) {
					$result[0]='succ';
					$result[1]="增加主类成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}
		}


		//加载修改页面
		public function updateTask(){
			$id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
			$category=$this->category->getCateInfo($id);
			$result['category']=$category;
			$this->cms->tableScene($result,"tpl//admin/category/modify.php");
		}


		//修改类别
		public function updateAfterTask(){
			if(isset($_POST['submit']))
			{
				$id=$_POST['id'];
				$name=$_POST['name'];
				$bool=$this->category->modify($id,$name);
				if ($bool) {
					$result[0]='succ';
					$result[1]="修改成功";
					$this->cms->formScene($result,"tpl//admin/result.php");
				}else{
					$result[0]='';
					$this->cms->formScene($result,"tpl//admin/result.php");
				}
			}
		}


		





	}
 ?>