<?php
import('custom.data.adminActionMode');
import('custom.data.adminInfoMode');
// 后台登陆
class admin extends Activity {
	/** @var CmsView*/
	protected $cms;
	/** @var  adminActionMode */
	protected $admin;
	protected function __construct(){
		parent::__construct();
		$this->cms=CmsView::init();
		$this->admin=adminActionMode::init();
		$this->cms->setControlFile("tpl/admin/adminAction/control.json");
        $this->cms->setPageTitle("管理员");
	}

	function loginTask(){
		$result['actionUrl']=getAppInfo()->webRouter->getPage("admin","loginSubmit");
		$this->cms->loginScene($result['actionUrl'],'user','pass');
	}
	function loginSubmitTask(){
		if(isset($_POST['user'])&&isset($_POST['pass'])){
			$user=$_POST['user'];
			$pass=$_POST['pass'];
			if($this->admin->login($user,$pass)){
				header("location:".getAppInfo()->webRouter->getPage("adminUser","list"));
			}else{
				$result['message']="账号或密码错误";
				View::displayAsHtml($result,'state/state.php');
			}
		}else{
			$result['message']="请输入用户名密码";
			View::displayAsHtml($result,'state/state.php');
		}
	}
	//登出
	function logoutTask(){
		$this->admin->logout();
	}
}
