<?php
    import("custom.data.serviceMode");
    import('custom.admin.adminBase');
    /**
      * 商品参数
      */
    class service extends adminBase{

        protected $service;
        protected $cms;

        public function __construct(){
            parent::__construct();
            $this->service=serviceMode::init();
            $this->cms=CmsView::init();
            $this->cms->setControlFile("tpl/admin/service/control.json");
            $this->cms->setPageTitle("售后服务管理");
        }
        
        public function listTask(){
            $result['service']=$this->service->show();
            $this->cms->tableScene($result,"tpl/admin/service/list.php");
        }

        public function addTask(){
            $this->cms->normalScene(array(),"tpl/admin/service/add.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
        }

        public function addAfterTask(){
            if (isset($_POST['submit'])) {
                $name=$_POST['name'];
                $content=$_POST['content'];
                if ($this->service->add($name,$content)) {
                    $result['message']='增加成功';
                    $result['url']=WebRouter::init()->getAction('list');
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='增加失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }

        //加载修改页面
        public function updateTask(){
            $id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
            $result['service']=$this->service->getOneService($id);
            $this->cms->normalScene($result,"tpl/admin/service/update.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
        }

        //修改
        public function updateAfterTask(){
            if(isset($_POST['submit']))
            {
                $id=$_POST['id'];
                $name=$_POST['name'];
                $content=$_POST['content'];
                $bool=$this->service->update($id,$name,$content);

                if ($bool) {
                    $result['message']='修改成功';
                    $result['url']=WebRouter::init()->getAction('list');
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='修改失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }

        public function deleteTask(){
            if(isset($_GET['id'])){
                $id=(int)$_GET['id'];
                $bool=$this->service->delete($id);
                if ($bool) {
                    $result['message']='删除成功';
                    $result['url']=WebRouter::init()->getAction('list');
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='删除失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }










    } 
 