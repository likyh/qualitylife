<?php
import('custom.admin.adminBase');
class upload extends adminBase{
	protected $cms;
	protected function __construct()
	{
		parent::__construct();
		$this->cms=CmsView::init();
		$this->cms->setPageTitle("文件上传");
		$this->cms->setControlFile("tpl/admin/upload/control.json");
	}
	function pictureTask(){
		$result=array();
		$this->cms->normalScene($result,"admin/picUpload.php",CmsView::TYPE_JQUERY);
	}
	function ajaxUploadTask(){
		import("custom.data.pictureMode");
		$pic=pictureMode::init();
		$up=Uploader::init();
		list($ret,$picInfo)=$up->upFile("file");
		$result['state']=$pic->createPic($picInfo['url'],null,$picInfo['savePath'],$picInfo['filename']);
		View::displayAsJson($result);
	}
}