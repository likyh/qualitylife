<?php
    import("custom.data.orderActionMode");
    import('custom.admin.adminBase');
    import('custom.data.userInfoMode');
    import('custom.data.addressMode');
    import('custom.data.goodsOrderMode');
    import('custom.data.goodsActionMode');
    /**
      * 商品参数
      */
    class order extends adminBase{

        private $order;
        protected $cms;
        private $user;
        private $address;
        private $goodsOrderMode;
        private $goods;

        public function __construct(){
            parent::__construct();
            $this->order=orderActionMode::init();
            $this->user=userInfoMode::init();
            $this->goods=goodsActionMode::init();
            $this->address=addressMode::init();
            $this->goodsOrder=goodsOrderMode::init();
            $this->cms=CmsView::init();
            $this->cms->setControlFile("tpl/admin/order/control.json");
            $this->cms->setPageTitle("售后服务管理");
        }
        
        public function listTask(){
            $userInfo=array();
            $result['order']=$this->order->show();
            $this->cms->tableScene($result,"tpl/admin/order/list.php");
        }



        //加载修改页面
        public function updateTask(){
            $id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
            $user_id=is_numeric($_GET['user_id']) ? $_GET['user_id'] : (int)$_GET['user_id'];
            $address_id=is_numeric($_GET['address_id']) ? $_GET['address_id'] : (int)$_GET['address_id'];
            $result['id']=$id;
            $result['order']=$this->order->orderState($id);
            $result['user']=$this->user->accountInfo($user_id);
            $result['address']=$this->address->oneAddress($address_id);
            $result['goods']=$this->goodsOrder->getGoodsDetails($id);
            $this->cms->formScene($result,"tpl/admin/order/update.php");
        }

        //修改
        public function updateAfterTask(){
            if(isset($_POST['submit']))
            {
                $id=$_POST['id'];
                $state=$_POST['state'];
                $bool=$this->order->update($id,$state);
                if ($bool) {
                    $result['message']='修改成功';
                    $result['url']=WebRouter::init()->getAction('list');
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='修改失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }

        public function deleteTask(){
            if(isset($_GET['id'])){
                $id=(int)$_GET['id'];
                $bool=$this->service->delete($id);
                if ($bool) {
                    $result['message']='删除成功';
                    $result['url']=WebRouter::init()->getAction('list');
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='删除失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }










    } 
 