<?php
    import("custom.data.deliveryMode");
    import('custom.admin.adminBase');
    /**
      * 商品参数
      */
    class delivery extends adminBase{

        protected $delivery;
        protected $cms;

        public function __construct(){
            parent::__construct();
            $this->delivery=deliveryMode::init();
            $this->cms=CmsView::init();
            $this->cms->setControlFile("tpl/admin/delivery/control.json");
            $this->cms->setPageTitle("快递说明管理");
        }
        
        public function listTask(){
            $result['delivery']=$this->delivery->show();
            $this->cms->tableScene($result,"tpl/admin/delivery/list.php");
        }

        public function addTask(){
            $this->cms->normalScene(array(),"tpl/admin/delivery/add.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
        }

        public function addAfterTask(){
            if (isset($_POST['submit'])) {
                $name=$_POST['name'];
                $content=$_POST['content'];
                if ($this->delivery->add($name,$content)) {
                    $result['message']='增加成功';
                    $result['url']=WebRouter::init()->getAction('list');;
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='增加失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }

        //加载修改页面
        public function updateTask(){
            $id=is_numeric($_GET['id']) ? $_GET['id'] : (int)$_GET['id'];
            $result['delivery']=$this->delivery->getOneDelivery($id);
            $this->cms->normalScene($result,"tpl/admin/delivery/update.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
        }

        //修改
        public function updateAfterTask(){
            if(isset($_POST['submit']))
            {
                $id=$_POST['id'];
                $name=$_POST['name'];
                $content=$_POST['content'];
                $bool=$this->delivery->update($id,$name,$content);

                if ($bool) {
                    $result['message']='修改成功';
                    $result['url']=WebRouter::init()->getAction('list');;
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='修改失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }

        public function deleteTask(){
            if(isset($_GET['id'])){
                $id=(int)$_GET['id'];
                $bool=$this->delivery->delete($id);
                if ($bool) {
                    $result['message']='删除成功';
                    $result['url']=WebRouter::init()->getAction('list');;
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }else{
                    $result['message']='删除失败';
                    View::displayAsHtml($result,"plugin/state/tips.php");
                }
            }
        }










    } 
 