<?php
import("custom.data.userActionMode");
import("custom.data.wishMode");
class wish extends Activity{
    /** @var  userActionMode */
    protected $userAction;
    /** @var  wishMode */
    protected $wish;
	protected function __construct() {
		$this->userAction = userActionMode::init();
        $this->wish=wishMode::init();
	}

    function indexTask(){
        View::displayAsHtml(array(),"wish.php");
    }

    function listTask(){
        $userInfo=$this->userAction->getUserInfo();
        if(empty($userInfo)){
            $result=array('message'=>"未登录",'state'=>403);
        }else{
            $userId=$userInfo['id'];
            $data=$this->wish->wishList($userId,100);
            $result=array('message'=>"成功",'state'=>200,'data'=>$data);
        }
        //var_dump($result);
        View::displayAsJson($result);
    }

    function addToWish(){
        if(!(isset($_GET['gopen_id']))){
            return array('message'=>"信息不完整",'state'=>400);
        }
        $userInfo=$this->userAction->getUserInfo();
        if(empty($userInfo)){
            return array('message'=>"未登录",'state'=>403);
        }
        $userId=$userInfo['id'];
        $gopen_id=$_GET['gopen_id'];
        $result = $this->wish->addToWish($userId,$gopen_id);
        if($result=='success'){
            return array('message'=>"添加成功",'state'=>200);
        }else{
            return array('message'=>$result,'state'=>500);
        }
    }
    function addTask(){
        $result=$this->addTowish();
        View::displayAsJson($result);
    }
    function removeFromWish(){
        if(!isset($_GET['gopen_id'])){
            return array('message'=>"信息不完整",'state'=>400);
        }
        $userInfo=$this->userAction->getUserInfo();
        if(empty($userInfo)){
            return array('message'=>"未登录",'state'=>403);
        }
        $result = $this->wish->removeFromwish($userInfo['id'],$_GET['gopen_id']);
        if($result=='success'){
            return array('message'=>"删除成功",'state'=>200);
        }else{
            return array('message'=>$result,'state'=>500);
        }
    }
    function removeTask(){
        $result=$this->removeFromWish();
        View::displayAsJson($result);
    }
    function checkWish(){
        if(!isset($_GET['gopen_id'])){
            return array('message'=>"信息不完整",'state'=>400);
        }
        $userInfo=$this->userAction->getUserInfo();
        if(empty($userInfo)){
            return array('message'=>"未登录",'state'=>403);
        }
        $data = $this->wish->checkWish($userInfo['id'],$_GET['gopen_id']);
        if($data){
            return array('message'=>"已收藏",'state'=>200);
        }else{
            return array('message'=>"未收藏",'state'=>500);
        }
    }
    function checkWishTask(){
        $result = $this->checkWish();
        View::displayAsJson($result);
    }
}
