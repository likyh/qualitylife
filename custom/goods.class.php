<?php 
import("custom.data.goodsInfoMode");
import("Custom.Data.categoryMode");
import("Custom.Data.serviceMode");
import("Custom.Data.deliveryMode");
import("Custom.Data.buyreadMode");
class goods extends Activity{
    /** @var  goodsInfoMode */
    protected $goodsInfo;
	protected function __construct(){
		$this->goodsInfo = goodsInfoMode::init();
	}
    const RELATED_NUM=5;
    const HOT_NUM=5;
    const LIST_NUM=9;

    // 某一个分类的列表
    function listTask() {
        $category=categoryMode::init();
        $category_id=isset($_GET['category_id'])&&!empty($_GET['category_id'])?(int)$_GET['category_id']:0;

        $result['category_id']=$category_id;
        $category_bottom_id = $category->getBottomCategory($category_id);//所有底层id数组
        $result['class']=$category->getParentsCategory($category_id);
        $result['title'] = $result['class'][0]['name'];
        $result['subclasses']=$category->getSonCategory($result['class'][0]['id']);
        if(empty($result['subclasses'])){
            $result['subclasses']=$category->getSonCategory(0);
        }

        //分页显示
        $page=isset($_GET['page'])&&!empty($_GET['page'])?(int)$_GET['page']:1;
        $page_size = self::LIST_NUM;
        $page_begin = ($page-1)*$page_size;
        $page_total = ceil($this->goodsInfo->getTotalNum($category_bottom_id,'category_id')/$page_size);
        $result['page']['currentPage']=$page;
        $result['page']['totalPage']=$page_total;

        //各种排序方式
        $result['order']['method'] = 'create_time';
        $result['order']['value']  = '1';
        //默认

        //0 无要求 1 DESC default ASCd
        if(!empty($_GET['volume'])){//价格排序 
            $result['order']['method'] = 'volume';
            $result['order']['value']  = (int)$_GET['volume'];
        }
        if(!empty($_GET['create_time'])){//时间排序 
            $result['order']['method'] = 'create_time';
            $result['order']['value']  = (int)$_GET['create_time'];
        }
        if(!empty($_GET['selling_price'])){//排序方式 
            $result['order']['method'] = 'selling_price';
            $result['order']['value']  = (int)$_GET['selling_price'];
        }

        //商品列表
        $orderBy = $result['order']['method'];//排序方式
        $sort = $result['order']['value'];//值
        $goodsIds = $this->goodsInfo->getList(self::LIST_NUM,$page_begin,$category_bottom_id,null,$orderBy,$sort);
        $result['goodslist'] = $this->goodsInfo->brief($goodsIds);
        View::displayAsHtml($result,"goodslist.php");
    }

    // 某一个标签的列表
    function labelTask() {
        import("Custom.Data.labelMode");
        $category=categoryMode::init();
        $label=isset($_GET['label'])&&!empty($_GET['label'])? (int)$_GET['label']: 1;

        $result['subclasses']=$category->getSonCategory(0);
        $result['label']=labelMode::init()->detail($label);
        $result['title']=$result['label']['name'];
  
        //分页显示
        $page=isset($_GET['page'])&&!empty($_GET['page'])?(int)$_GET['page']:1;
        $page_size = self::LIST_NUM;
        $page_begin = ($page-1)*$page_size;
        //var_dump($label);
        $page_total = ceil($this->goodsInfo->getTotalNum($label,'label')/$page_size);
        $result['page']['currentPage']=$page;
        $result['page']['totalPage']=$page_total;

        //各种排序方式
        $result['order']['method'] = 'create_time';
        $result['order']['value']  = '1';
        //默认

        //0 无要求 1 DESC default ASCd
        if(!empty($_GET['volume'])){//价格排序 
            $result['order']['method'] = 'volume';
            $result['order']['value']  = (int)$_GET['volume'];
        }
        if(!empty($_GET['create_time'])){//时间排序 
            $result['order']['method'] = 'create_time';
            $result['order']['value']  = (int)$_GET['create_time'];
        }
        if(!empty($_GET['selling_price'])){//排序方式 
            $result['order']['method'] = 'selling_price';
            $result['order']['value']  = (int)$_GET['selling_price'];
        }
            
        //商品列表
        $orderBy = $result['order']['method'];//排序方式
        $sort = $result['order']['value'];//值
        $goodsIds = $this->goodsInfo->getList(self::LIST_NUM,$page_begin,null,$label,$orderBy,$sort);
        $result['goodslist'] = $this->goodsInfo->brief($goodsIds);
        //var_dump($result);
        View::displayAsHtml($result,"goodsLabel.php");
    }

    // 商品详情
	function detailTask() {
                import("Custom.Data.articleMode");
        	   $category=categoryMode::init();
                $article=articleMode::init();
                $service=serviceMode::init();
                $buyread=buyreadMode::init();
                $delivery=deliveryMode::init();
                $gopen_id=isset($_GET['gopen_id'])&&!empty($_GET['gopen_id']) ? (int)$_GET['gopen_id'] : 1;
        		//商品详细信息
                $result = $this->goodsInfo->detail($gopen_id);
                $result['class']=$category->getParentsCategory($result['category_id']);
                $result['subclasses']=$category->getSonCategory($result['class'][0]['id']);
                $result['score']=$this->goodsInfo->goodsScore($gopen_id);
                //$result['comment']=$this->goodsInfo->goodsComments($gopen_id);
                //var_dump($result);
                $result['delivery']=$delivery->getOneDelivery($result['delivery_id']);
                $result['service']=$service->getOneService($result['service_id']);
                $result['buyread']=$buyread->getOneBuyread($result['buyread_id']);
                //var_dump($result);
                //var_dump($result['article']);
             
        	      View::displayAsHtml($result,"goods.php");
	}

    //商品评论
    function commentTask(){
        import("Custom.Data.commentMode");
        $comment=commentMode::init();
        $gopen_id=isset($_GET['gopen_id'])&&!empty($_GET['gopen_id'])?(int)$_GET['gopen_id']:1;
        $comment_page=isset($_GET['comment_page'])&&!empty($_GET['comment_page'])?(int)$_GET['comment_page']:1;
        $row = 10;//单页显示评论数
        list($result['data'],$result['total'])=$comment->commentList($gopen_id,$row,$comment_page-1);
        View::displayAsJson($result);
    }

	//相关热门商品展示
	function relatedShowTask(){
        $gopen_id=isset($_GET['gopen_id'])&&!empty($_GET['gopen_id'])?(int)$_GET['gopen_id']:1;
		//获取此展品种类
		$data['cata'] = $this->goodsInfo->detail($gopen_id);
		$orderBy = 'volume';//销量排序
		$sort = 1;//desc
        $goodsIds = $this->goodsInfo->getList(self::RELATED_NUM,null,$data['cata']['category_id'],null,$orderBy,$sort);
        $result['label_hot'] = $this->goodsInfo->brief($goodsIds);

		$result['title'] = "相关热卖";
        $result['id'] = $data['cata']['id'];
		View::displayAsHtml($result,"module/relatedShow.php");
	}

	//热门产品展示
    function hotShowTask(){
        $category_id=null;
        $label = 4;//热门标签的ID
        $orderBy = 'volume';//销量排序
        $sort = 1;//desc

        $goodsIds = $this->goodsInfo->getList(4,0,$category_id,$label,$orderBy,$sort);
        $result['label_hot'] = $this->goodsInfo->brief($goodsIds);
        $result['title'] = "热门商品";
        $result['id'] = $label;
        View::displayAsHtml($result,"module/hotShow.php");
    }
}