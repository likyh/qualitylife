<?php
import("custom.data.articleMode");
class article extends Activity{
	/** @var  articleMode*/
	protected $article;

	protected function __construct(){
		$this->article=articleMode::init();
	}

	function indexTask() {
		$id = isset($_GET['id'])&&!empty($_GET['id'])?(int)$_GET['id']:0;

		$result['detail'] = $this->article->detail($id);
		$result['list'] = $this->article->brief($result['detail']['type']);
		//var_dump($result);
		View::displayAsHtml($result,"article.php");
	}
	function serviceTask(){
		$result[1] = $this->article->brief('新手入门');
		$result[2] = $this->article->brief('配送服务');
		$result[3] = $this->article->brief('支付方式');
		$result[4] = $this->article->brief('售后服务');
		$result[5] = $this->article->brief('自助服务');
		$result[6] = $this->article->brief('招商合作');
		//var_dump($result);
		View::displayAsHtml($result,"module/service.php");
	}
}