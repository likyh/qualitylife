<?php
import("custom.data.addressMode");
import("custom.data.userActionMode");
class address extends Activity{
	/** @var  addressMode*/
	protected $address;

	protected function __construct(){
		$this->address=addressMode::init();
	}

	// 用户个人地址
	function userAddressTask(){
        $user=userActionMode::init();
        $user=$user->getUserInfo();
        if(empty($user)){
            $result=array("state"=>500,"message"=>"未登录");
        }else{
            $result["state"]=200;
            $result['data'] = $this->address->userAddress($user['id']);
        }
        View::displayAsJson($result);
	}

	// 用户新增/修改地址
    // 有id传入则为修改否则为新增
	function userAddressActionTask() {
		$db=SqlDB::init();
		$user=userActionMode::init();
        $user=$user->getUserInfo();
        if(empty($user)){
            $result=array("state"=>500,"message"=>"未登录");
        }else{
            $data['user_id']      = $user['id'];
        	$data['receiver']     = isset($_GET['receiver'])&&!empty($_GET['receiver'])?$_GET['receiver']:'no';
        	$data['province']     = isset($_GET['province'])&&!empty($_GET['province'])?$_GET['province']:'no';
        	$data['city']         = isset($_GET['city'])&&!empty($_GET['city'])?$_GET['city']:'no';
        	$data['district']     = isset($_GET['district'])&&!empty($_GET['district'])?$_GET['district']:'no';
        	$data['street']       = isset($_GET['street'])&&!empty($_GET['street'])?$_GET['street']:'no';
        	$data['postal_code']  = isset($_GET['postal_code'])&&!empty($_GET['postal_code'])?$_GET['postal_code']:null;
        	$data['mobile_phone'] = isset($_GET['mobile_phone'])&&!empty($_GET['mobile_phone'])?$_GET['mobile_phone']:null;
        	$data['fixed_phone']  = isset($_GET['fixed_phone'])&&!empty($_GET['fixed_phone'])?$_GET['fixed_phone']:null;

            //验证完整性
            $ifComplete = 1;
            foreach ($data as $key => &$value) {
                if($value=='no'){
                    $ifComplete = 0;
                }
            }
            if($data['mobile_phone']==null&&$data['fixed_phone']==null) {
                $ifComplete = 0;
            }

            //数据信息处理
            $result=array("state"=>400,"message"=>"信息不完整或已存在");
            if($ifComplete==1){
                if(isset($_GET['address_id'])&&!empty($_GET['address_id'])){
                    $address_id = (int)$_GET['address_id'];
                    if($this->address->check($user['id'],$address_id))
                        if($this->address->update($address_id,$data))
                            $result=array("state"=>200);
                }else{
                    if($this->address->create($data)) $result=array("state"=>200);
                }
            }
        }
        View::displayAsJson($result);
	}

    // 设置默认地址
    function userSetDefaultTask(){
    	$user=userActionMode::init();
        $user=$user->getUserInfo();
        if(empty($user)){
            $result=array("state"=>500,"message"=>"未登录");
        }else{
            $data['address'] = $this->address->userAddress($user['id']);
            if($data['address']=='') {
            	$result=array("state"=>400,"message"=>"未设置地址");
            } else {
                $address_id = isset($_GET['address_id'])&&!empty($_GET['address_id'])?(int)$_GET['address_id']:$data['address'][0]['id'];
                if($this->address->setDefault($user['id'],$address_id)){
                    $result = array("state"=>200);
                }else{
                    $result=array("state"=>401,"message"=>"请求超时，请重试");
                }
            }
        }
        View::displayAsJson($result);
    }
    //获取默认地址
    function getDefaultTask(){
        $user=userActionMode::init();
        $user=$user->getUserInfo();
        if(empty($user)){
            $result=array("state"=>500,"message"=>"未登录");
        }else{
            $result = $this->address->getDefault($user['id']);
        }
        View::displayAsJson($result);
    }
    //删除
    function userDeleteTask() {
        $user=userActionMode::init();
        $user=$user->getUserInfo();
        if(empty($user)){
            $result=array("state"=>500,"message"=>"未登录");
        }else{
            $result=array("state"=>400,"message"=>"请求超时，请重试");
            $address_id = isset($_GET['address_id'])&&!empty($_GET['address_id'])?(int)$_GET['address_id']:0;
            if($this->address->check($user['id'],$address_id))
                if($this->address->delete($address_id))
                    $result = array("state"=>200);
        }
        View::displayAsJson($result);
    }
    // 获取地址信息
    function cityTask(){
        $db=SqlDB::init();
        $pid=isset($_GET['pid'])?(int)$_GET['pid']:0;
        $result['state']=200;
        $result['data']=$db->getAll("select * from city where pid=$pid");
        View::displayAsJson($result);
    }
    function cityListTask(){
        $db=SqlDB::init();
        $name=isset($_GET['name'])?$db->quote($_GET['name']):'';
        $level=isset($_GET['level'])?$db->quote($_GET['level']):'';
        $result['state']=200;
        $result['data']=$db->getOne("select * from city where name=$name and level=$level");
        View::displayAsJson($result);
    }
}