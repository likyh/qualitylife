<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/24
 * Time: 20:02
 */
import("custom.data.pictureMode");
import("custom.data.labelMode");
class goodsInfoMode extends Data{
    /** @var  pictureMode */
    protected $picture;
    protected function __construct() {
        parent::__construct();
        $this->picture=pictureMode::init();
    }

    /**
     * 商品列表，只搜出了id
     * @param $rows,$offset,$category_id,$label_id,$sort(0为空，1为desc,2为asc),$orderBy(排序依据)
     */
    public function getList($rows,$offset=0,$category_id=null,$label_id=null,$orderBy=null,$sort=0){
        if(!is_array($category_id)){
            $condition['category_id']=$category_id;
        }else{
            $sql_category_id = ' AND (';
            foreach ($category_id as $key => $value) {
                $sql_category_id .= 'category_id='.(int)$value.' OR ';
            }
            $sql_category_id .='1=2 )';
        }

        $rows=(int)$rows;
        $offset=(int)$offset;
        $condition['enable']=1;
        if(!empty($label_id)){
            $condition[]="FIND_IN_SET(".(int)$label_id.",`label_ids`)";
        }
        $attach='';
        if(!empty($orderBy)){
            $attach=" order by ".$orderBy;
            if($sort!=0){
                $attach.= $sort==1?' DESC':' ASC';
            }
        }
        $sql=$this->db->selectSql(
            array('goods'=>array('gopen_id')),
            $condition,$attach,$rows,$offset);
        if(is_array($category_id)){
            $sql = str_replace("order", $sql_category_id." order", $sql);
        }
        //echo $sql;
        return array_map(function($v){return $v['gopen_id'];},$this->db->getAll($sql));

    }
    /**
     * 商品详细信息
     * @param $gopen_id
     * @return array
     */
    public function detail($gopen_id){
        $gopen_id=(int)$gopen_id;
        $sql="select * from `goods` where `gopen_id`=$gopen_id and `enable`=1";
        $data=$this->db->getOne($sql);
        if(!empty($data['picture_ids'])){
            $data['picture']=$this->picture->allIdUrl($data['picture_ids']);
        }

        $data['parameter']=$this->oneParameter($data['gopen_id']);

        $label=labelMode::init();
        if(!empty($data['label_ids'])){
            $data['label']=$label->oneLabel($data['label_ids']);
        }
        
        $data['attribute']=$this->OneAttribute($gopen_id);
        return $data;
    }

    /**
     * 商品简略信息（gopen_id、name、售价，原价，一张图片）
     */
    public function brief($gopen_ids){
        if(empty($gopen_ids)) return null;
        $data=array();
        $sql = "select `gopen_id`, `name`, `selling_price`, `original_price`, `picture_ids`
              from `goods` where ".$this->db->getIdCondition($gopen_ids,"gopen_id")."and `enable`=1";
        $data = $this->db->getAll($sql);
        foreach($data as &$t){
            $t['picture'] = empty($t['picture_ids'])? null: $this->picture->firstIdUrl($t['picture_ids']);
        }
        return is_array($gopen_ids)?$data:$data[0];
    }
    // 获取商品总数(ID 和 依据)
    public function getTotalNum($selection_id,$selectionBasis){
        $sql = "select count(*) from goods where 1=2 ";
        if(is_array($selection_id)){
            //var_dump($selection_id);
            foreach ($selection_id as $key => $value) {
                $id = (int)$value;
                $sql .= " or `$selectionBasis` = $id";
            }
            //echo $sql;
        }else if($selectionBasis=='label'){
            $id = (int)$selection_id;
            $sql .= " or FIND_IN_SET(".$id.",`label_ids`)";
        }else{
            $id = (int)$selection_id; 
            $sql .= " or `$selectionBasis` = $id";  
        }
        $sql .= " and `enable`=1";
        $data = $this->db->getValue($sql);
        //var_dump($data);
        
        return $data;
    }
    /**
     * 搜索指定商品参数信息
     */
    public function oneParameter($parameter_id){
        $parameter_id=(int)$parameter_id;
        $sql="select * from `parameter` where gopen_id=$parameter_id";
        return $this->db->getOne($sql);
    }

    /**
     * 某商品参数信息
     * @param $gopen_id
     * @return array
     */
    public function goodsParameter($gopen_id){
        die("error, goodsParameter Function，on File ".__FILE__." Line ".__LINE__);
        // TODO 检查一下这个sql语句，这个表没有这个字段，其实也不需要这个函数，用上面那个就可以了
        $gopen_id=(int)$gopen_id;
        $sql="select * from `parameter` where `gopen_id`=$gopen_id";
        return $this->db->getOne($sql);
    }

    /**
     * 指定商品所有属性及库存量
     * @param $gopen_id
     */
    public function oneAttribute($gopen_id){
        $gopen_id=(int)$gopen_id;
        $sql="select * from `attribute` where `gopen_id`=$gopen_id";
        return $this->db->getAll($sql);
    }

    /**
     * 获取指定商品的分类
     * @param $gopen_id
     * @return array
     */
    public function oneCategory($gopen_id){
        trigger_error("该方法没有必要，可以自行根据gopen_id获取category_id，使用分类mode获取信息",E_USER_WARNING);
        $gopen_id=(int)$gopen_id;
        $sql="select GC.* from `goods` G,`goods_category` GC
              where G.gopen_id=$gopen_id
              and G.enable=1
              and G.category_id=GC.id";
        return $this->db->getOne($sql);
    }

    /**
     * 检查商品gopen_id存在且有效
     * @param $gopen_id
     */
    public function checkGoods($gopen_id){
        $gopen_id=(int)$gopen_id;
        $sql="select 1 from `goods` where `gopen_id`=$gopen_id and `enable`=1";
        return $this->db->getExist($sql);
    }

    //获取商品评分
    public function goodsScore($gopen_id){
        $gopen_id = (int)$gopen_id;
        $sql = "select `product_quality_score`,`customer_service_score` from comment where `gopen_id`=$gopen_id";
        //$goods_ids = $this->db->getAll($sql);
        //$sql = "select `product_quality_score`,`customer_service_score` from `goods_in_order` where 1=2 ";
        //foreach ($goods_ids as $key => $value) {
        //    $sql .= "or goods_id=".(int)$value['id'].' ';
        //}
        $result = $this->db->getAll($sql);
        if($result){
            $count = 0;
            $product_total = 0;
            $service_total = 0;
            foreach ($result as $key => $value) {
                $product_total += $value['product_quality_score'];
                $service_total += $value['customer_service_score'];
                $count += 1;
            }
            $data['product_ave'] = (int)ceil($product_total/$count);
            $data['service_ave'] = (int)ceil($service_total/$count);
            $data['count'] = (int)$count;
        }else{
            $data['product_ave'] = 0;
            $data['service_ave'] = 0;
            $data['count'] = 0;
        }
        return $data; 
    }



    //后台商品列表
    public function getListGoods($rows,$offset){
        $sql = "select * from goods  limit $rows,$offset";
        $data = $this->db->getAll($sql);
        return $data;
    }

    //商品总数
    public function getGoodsToatl(){
        $sql = "select count(*) as num from goods";
        $data = $this->db->getValue($sql);
        
        return $data;
    }





} 