<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/26
 * Time: 10:09
 */

class goodsActionMode extends Data{
    protected function __construct() {
        parent::__construct();
    }
    /**
     * 新建商品
     * @param array $data 注意格式
     * @return bool
     */
    public function create($data){
        if(!is_array($data)||empty($data)){
            return false;
        }
        $re=$this->db->insert('goods',$data);
        if($re!=1){
            return false;
        }
        $id=$this->db->insertId();
        $data2['gopen_id']=(int)$id;
        return $this->db->modify('goods',$id,$data2);
    }
    //TODO （可能会去掉）检查商品名是否存在
    public function checkName($name){
        $name=$this->db->quote($name);
        $sql="select 1 from `goods` where `name`={$name} and `enable`=1";
        return $this->db->getExist($sql);
    }
    /**
     * @param $gopen_id
     * @param array $data 注意格式，data里面需要gopen_id，不能有id
     * @return bool
     */
    public function modify($gopen_id,$data){
        $gopen_id=(int)$gopen_id;
        if(!is_array($data)||empty($data)){
            return false;
        }
        //清理旧数据
        $sql = "UPDATE `goods` set `enable`=0 where `gopen_id`=$gopen_id and `enable`=1";
        $this->db->sqlExec($sql);
//        旧数据
//        $sql="select * from `goods` where `gopen_id`=$gopen_id and `enable`=1";
//        $oldData=$this->db->getOne($sql);
        return $this->db->insert('goods',$data)==1;

    }
    /**
     *
     * 删除商品
     * @param $id
     * @return bool
     */
    public function delete($id){
        $id=(int)$id;
        $sql="update `goods` set `enable`=0 where `id`=$id";
        return $this->db->sqlExec($sql)==1;
    }
    /**
     * 根据gopen_id查有效id
     * @param $gopen_id
     */
    public function goodsId($gopen_id){
        $gopen_id=(int)$gopen_id;
        $sql="select `id` from `goods` where `gopen_id`=$gopen_id and `enable=1`";
        return $this->db->getValue($sql);
    }


    //上架
    public function up($id){
        $sql="update `goods` set `enable`=1 where `id`=$id";
        return $this->db->sqlExec($sql)==1;
    }

    //下架
    
    public function down($id){
        $sql="update `goods` set `enable`=0 where `id`=$id";
        return $this->db->sqlExec($sql)==1;
    }
    
    //得到一个商品信息
    public function getOneGood($id){
        $id=$this->db->quote($id);
        $sql="SELECT * FROM  `goods` WHERE id=$id";
        return $this->db->getOne($sql);
    }


}