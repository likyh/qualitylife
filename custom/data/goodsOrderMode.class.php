<?php


import("custom.data.goodsInfoMode");

class goodsOrderMode extends Data{
    public static function init() {
        return parent::init();
    }

    //用于显示查看订单页面的商品信息
    public function getGoodsDetails($id){
        $sql="SELECT 
                            a.id,
                            a.goods_id,
                            a.attribute_id,
                            a.order_id,
                            a.number,
                            a.state,
                            b.name,
                            c.name as attribute_name
                            FROM `goods_in_order` a
                            LEFT JOIN  `goods` b ON a.goods_id=b.id
                            LEFT JOIN `attribute`  c ON a.attribute_id=c.id
                            WHERE a.order_id=$id
                            ORDER BY a.id DESC ";
        return $this->db->getAll($sql); 
    }
    
} 