<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/27
 * Time: 14:42
 */

class labelMode extends Data{
    /** @return labelMode */
    public static function init() {
        return parent::init();
    }

    /**
     * 显示所有标签
     */
    public function allLabel($rows,$limit){
        $sql="select * from `label` limit $rows,$limit";
        return $this->db->getAll($sql);
    }
    // 分类信息
    public function detail($id){
        return $this->db->getOne("select * from `label` where `id`=".(int)$id);
    }
    /**
     * 后台添加商品标签
     */
    public function add($name){
        if($this->checkExist($name)){
            return '不要定义重复标签噢，看看这个是不是存在了';
        }
        $data['name']=$name;
        return $this->db->insert('label',$data)==1?'success':'数据库错误';
    }
    /**
     * 检查标签名
     */
    public function checkExist($name){
        $name=$this->db->quote($name);
        $sql="select 1 from label where `name`={$name}";
        return $this->db->getExist($sql);
    }
    /**
     * 修改（name不能相同）
     */
    public function modify($id,$name){
        if($this->checkExist($name)){
            return false;
        }
        $data['name']=$name;
        return $this->db->modify('label',$id,$data)==1;
    }
    /**
     * 删除
     */
    public function delete($id){
        $id=(int)$id;
        return $this->db->delete('label',$id)==1;
    }
    //TODO 批量删除

    /**
     * 搜索指定商品所有标签
     * @param $ids
     */
    public function oneLabel($ids){
        if(empty($ids)){
            return null;
        }
        $idCondition=$this->db->getIdCondition($ids);
        $sql="select * from `label` where {$idCondition}";
        $result=$this->db->getAll($sql);
        return $result;
    }



    //获得商品加载页面的标签
    public function getGoodsLabel(){
        $sql="select id,name from `label`";
        $labels=$this->db->getAll($sql);
        foreach ($labels as $key => $value) {
            $data[$value['id']]=$value['name'];
        }
        return $data;
    }

    //得到标签总数
    public function getLabeslToatl(){
        $sql = "select count(*) as num from label";
        $data = $this->db->getValue($sql);
        return $data;
    }


}