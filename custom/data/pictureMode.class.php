<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 2014/10/21
 * Time: 18:59
 */

class pictureMode extends Data{
    /** @return pictureMode */
    public static function init() {
        return parent::init();
    }

    public function createPic($url,$md5=null,$path=null,$filename=null,$mark=null){
        if(!empty($url)){
            $data['url']=$url;
            $data['md5']=$md5;
            $data['path']=$path;
            $data['filename']=$filename;
            $data['mark']=$mark;
            return $this->db->insert("picture",$data)>0;
        }
        return false;
    }

    //得到第一个id的图片URL
    public function firstIdUrl($ids){
        if(empty($ids)){
            return null;
        }
        $firstIdArr=explode(',',$ids);
        $firstId=(int)$firstIdArr[0];
        $sql="select `url` from `picture` where `id`=$firstId";
        return $this->db->getValue($sql);
    }
    //得到所有id的图片URL
    public function allIdUrl($ids){
        if(empty($ids)){
            return null;
        }
        
        $idCondition=$this->db->getIdCondition($ids);

        $sql="select * from `picture` where {$idCondition}";
        return $this->db->getAll($sql);
    }
    //得到商品一张图片，根据goods_id or gopen_id
    public function onePicOfGoods($id,$isGoodsId=true){
        $id=(int)$id;
        if($isGoodsId){
            $condition=' `id`='.$id;
        }else{
            $condition=' `gopen_id`='.$id;
        }
        $sql="select `picture_ids` from `goods` where ".$condition;
        $ids=$this->db->getValue($sql);
        return $this->firstIdUrl($ids);
    }
    
    /*
     * 滚动图
     */
    public function scrollingList(){
        $sql="SELECT S.`id`,S.`url`,P.`url` as `picurl` FROM `scrolling` S,`picture` P where P.`id`=S.`picture_id`";
        return $this->db->getAll($sql);
    }
    public function allPicture(){
        $sql = "select * from `picture`";
        return $this->db->getAll($sql);
    }
} 