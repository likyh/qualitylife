<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/21
 * Time: 15:49
 */
import("custom.data.orderMode");
import("custom.data.pictureMode");
import("custom.data.addressMode");
class userInfoMode extends Data{
    /** @return userInfoMode */
    public static function init() {
        return parent::init();
    }
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
        $this->order=orderMode::init();
        $this->address=addressMode::init();
    }
    //账户信息
    public function accountInfo($id){
        $id=(int)$id;
        $sql="select `id`,`tel`,`username`,`point` from `user` where id=$id";
        $re=$this->db->getOne($sql);
        return $re;
    }

    //已有的用户投诉建议
    public function userComplaints($userId,$row,$offset=0){
        $userId = (int)$userId;
        $row = (int)$row;
        $offset = (int)$offset;
        $sql = "SELECT * from `complaint` where `user_id`=$userId order by `create_time` desc limit $offset,$row";
        $data[0] =  $this->db->getAll($sql);
        $sql = "SELECT count(1) from `complaint` where `user_id`=$userId";
        $data[1] = (int)$this->db->getValue($sql);
        return $data;
    }
    //添加投诉建议
    public function addComplaints($userId,$content){
        $userId = (int)$userId;
        $data['user_id'] = $userId;
        $data['content'] = $content;
        return $this->db->insert('complaint',$data);
    }

} 