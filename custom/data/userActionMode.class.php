<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/21
 * Time: 14:26
 */
import("custom.data.goodsInfoMode");
class userActionMode extends Data{
    /** @return commentMode */
    public static function init() {
        return parent::init();
    }

    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
        $this->goods=goodsInfoMode::init();
    }
    function checkUsername($username){
        $username=$this->db->quote($username);
        $sql="select id from `user` where `username`=$username";
        return $this->db->getExist($sql);
    }
    function checkTel($tel){
        $tel=$this->db->quote($tel);
        $sql="select id from `user` where `tel`=$tel";
        return $this->db->getExist($sql);
    }
    function login($loginName,$code){
        if(empty($loginName)||empty($code)){
            return "账号或者用户名为空";
        }
        if(preg_match("/^[0-9]*$/", $loginName)){
            $loginName=$this->db->quote($loginName);
            $username=$this->db->getValue("select `username` from `user` where tel=$loginName");
        }else{
            $username=$loginName;
        }
        $pass=getPassWord($username,$code);
        $username=$this->db->quote($username);
        $pass=$this->db->quote($pass);
        $sql="select `id`,`username`,`tel` from `user` where `code`=$pass and `username`=$username";
        $re=$this->db->getOne($sql);
        if(!empty($re)){
            $_SESSION['user']['login']=true;
            $_SESSION['user']['info']=$re;
            return 'success';
        }else{
            return "账号或者用户名错误";
        }
    }
    function logout(){
        SimpleSession::release();
    }
    function checkLogin(){
        return isset($_SESSION['user']['login'])&& $_SESSION['user']['login'];
    }
    function getUserInfo(){
        if(!$this->checkLogin()){
            return null;
        }
        return $_SESSION['user']['info'];
    }
    function getDetailInfo(){
        if(!$this->checkLogin()){
            return null;
        }
        $sql="select `id`,`tel`,`username`,`code`,`point`,`last_login`,`email_check` from `user` where `id`=".(int)$_SESSION['user']['info']['id'];
        return $this->db->getOne($sql);
    }
    //发送短信验证码 TODO
    function sendCode($tel){

        if($this->checkTel($tel)) return array(false,'该手机号已经注册过了噢！');
        $code=substr(getHash($tel.date("Y-m-d")),0,6);
        $message['#code#'] = $code;
        $jm=JuheSMSMode::init();
        $smsRe=$jm->send($tel,'qualityTplId',$message);
        $smsRe=$smsRe[0];
        if($smsRe['error_code']==0){
            return array(true,"success");
        }else{
            return array(false,$smsRe['reason']);
        }
    }
    public function checkCode($tel,$code){
        if($this->checkTel($tel)) return false;
        $code2=substr(getHash($tel.date("Y-m-d")),0,6);
        return $code==$code2;
    }
    public function register($tel,$username,$pass){
        if(empty($tel)||empty($username)||empty($pass)){
            return "手机号码、账号或者密码为空";
        }
        if(!preg_match('/^(([a-zA-Z0-9_\-])+\.)*([a-zA-Z0-9_\-]+)\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/',$username)){
            return '邮箱不合法';
        }
        if($this->checkUsername($username)){
            return "此用户名已被注册";
        }
        if(!preg_match("/^1[34578]{1}\\d{9}$/", $tel)){
            return "请输入正确的手机号";
        }if($this->checkTel($tel)){
            return "此手机号已注册过-.-";
        }
        if(strlen($pass)<6){
            return '密码小于6位';
        }

        $data['username']=$username;
        $data['tel']=$tel;
        $data['code']=getPassWord($username,$pass);
        $re=$this->db->insert('user',$data);
        if($re>0){
            $_SESSION['user']['login']=true;
            $_SESSION['user']['info']=$data;
            $_SESSION['user']['info']['id']=$this->db->insertId();
            var_dump($_SESSION);
            return "success";
        }else{
            return "注册失败，请重试";
        }
    }
    /**
     * 修改账户信息（user表后续可能有添加的字段）
     * $data=array('username'=>'KM');键值与数据表字段名相同
     * @param $userId
     * @param $data
     * @return bool
     */
    public function modifyAccount($userId,$data){
        $userId=(int)$userId;
        if(!is_array($data)||empty($data)){
            return false;
        }
        return $this->db->modify('user',$userId,$data)==1;
    }

    /**
     * 用户修改密码
     * @param $userId,$username
     * @param $oldPass,$newPass
     * @return bool
     */
    public function alterPass($userId,$username,$oldPass,$newPass){
        $userId=(int)$userId;
        $oldPass=getPassWord($username,$oldPass);
        $newPass=getPassWord($username,$newPass);
        $username=$this->db->quote($username);
        $sql="select `code` from `user` where `id`=$userId";
        $hashPass=$this->db->getValue($sql);
        if($oldPass==$hashPass){
            $data['code']=$newPass;
            $sql = "UPDATE `user` SET `code`='$newPass' WHERE `id`=$userId";
            return $this->db->sqlExec($sql);
        }
        return false;
    }
}