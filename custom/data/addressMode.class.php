<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/23
 * Time: 19:16
 */

class addressMode extends Data{
    /** @return addressMode */
    public static function init() {
        return parent::init();
    }
    /**
     * 用户收货地址信息
     * @param $userId
     * @return array
     */
    public function userAddress($userId){
        $userId=(int)$userId;
        $sql="select * from `address` where `user_id`=$userId";
        $result=$this->db->getAll($sql);
        return $result;
    }
    //显示指定地址
    public function oneAddress($addressId){
        $addressId=(int)$addressId;
        $sql="select * from `address` where `id`=$addressId";
        return $this->db->getOne($sql);
    }
    /**
     * $data=array('province'=>'江苏');键值与数据表字段名相同
     * @param $addressId
     * @param $data
     * @return bool
     */
    public function modify($addressId,$data){
        if(!is_array($data)||empty($data)){
            return false;
        }
        $addressId=(int)$addressId;
        return $this->db->modify('address',$addressId,$data)==1;
    }
    /**
     * $data=array('province'=>'江苏');键值与数据表字段名相同
     * 如果数据不变modify会再插一条空的，我只好这样
     * @param $addressId
     * @param $data
     * @return bool
     */
    public function update($addressId,$data){
        if(!is_array($data)||empty($data)){
            return false;
        }
        $addressId=(int)$addressId;
        return $this->db->update('address',$addressId,$data)==1;
    }
    /**
     * $data=array('province'=>'江苏');键值与数据表字段名相同
     * 创建时 `user_id`, `receiver`, `province`, `city`, `district`, `street` 必须
     * `mobile_phone`, `fixed_phone`至少需要其一
     * @param $userId
     * @param $data
     * @return bool
     */
    public function create($data){
        if(!is_array($data)||empty($data)){
            return false;
        }
        return $this->db->insert('address',$data,1)==1;
    }
    /**
     * 删除地址
     * @param $addressId
     * @return bool
     */
    public function delete($addressId){
        $addressId=(int)$addressId;
        return $this->db->delete('address',$addressId)==1;
    }
    /**
     * 检查该地址是否属于该用户
     * @param $userId,$addressId
     * @return bool
     */
    public function check($userId,$addressId){
        $userId=(int)$userId;
        $addressId=(int)$addressId;
        $sql="select 1 from `address` where `user_id`=$userId and `id`=$addressId";
        return $this->db->getExist($sql)==1;
    }
    //TODO 判断重复

    //设置默认地址
    //$user_id,$address_id如不输入则第一个地址设为默认
    public function setDefault($user_id,$address_id){
        $sql_clear = "update `address` SET `default`= 0 where user_id=$user_id";
        if($this->check($user_id,$address_id)){
            $this->db->sqlExec($sql_clear);
            return $this->db->update('address',$address_id,array('default'=>1))==1;
        } else {
            return false;
        }
    }
    //获取默认地址
    //$user_id
    public function getDefault($user_id){
        $user_id = (int)$user_id;
        $sql = "select * from `address` where `user_id` = $user_id and `default`=1";
        return $this->db->getOne($sql);
    }
    //找出地址信息一样的地址
    public function userSameAddress($data){
        $user_id  = (int)$data['user_id'];
        $province = $this->db->quote($data['province']);
        $city     = $this->db->quote($data['city']    );
        $district = $this->db->quote($data['district']);
        //TODO:验证地址是否合法
        $sql = "select * from `address` where user_id=$user_id and province=$province and district=$district";
        //echo $sql;
        $result = $this->db->getOne($sql);
        return $result;
    }
    //验证地址完整性
    public function checkComplete($address_id){
        $address_id = (int)$address_id;
        $sql = "select * from `address` where id=$address_id";
        $data = $this->db->getOne($sql);
        
        if(empty($data['mobile_phone'])&&empty($data['fixed_phone'])){
            return false;
        }
        unset($data["postal_code"]);
        unset($data["mobile_phone"]);
        unset($data["fixed_phone"]);
        unset($data["remark"]);
        foreach ($data as $key => $value) {
            if($value==null){
                return false;
            }
        }
        return true;
    }
}





