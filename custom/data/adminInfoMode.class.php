<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/31
 * Time: 16:40
 */
class adminInfoMode extends Data{
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
    }

    /**
     * 获取某个管理员的信息
     * @param $id
     * @return array
     */
    public function oneAdmin($id){
        $id=(int)$id;
        $sql="select `id`, `username`, `tel`, `email`, `code`, `type`, `create_time`
            from `admin` where id=$id";
        return $this->db->getOne($sql);
    }

    /**
     * 获取管理员的信息
     * @return array
     */
    public function allAdmin($type=null){
        $sql="select `id`, `username`, `tel`, `email`, `code`, `type`, `create_time` from `admin`";
        if(!empty($type)){
            $type=(int)$type;
            $sql.=" where `type`=$type";
        }
        return $this->db->getAll($sql);
    }
} 