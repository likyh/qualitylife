<?php
class couponMode extends Data{
    public static function init() {
        return parent::init();
    }
    
    //获取单个用户拥有的优惠券信息
    public function userCoupon($user_id){
    	$user_id = (int)$user_id;
    	$sql     = "SELECT * from `user_coupon` where `user_id`=$user_id";
    	$data    = $this->db->getAll($sql);
    	foreach ($data as $key => &$value) {
    		$value['coupon'] = $this->oneCoupon($value['coupon_id']);
    	}
    	return $data;
    }

    //为用户添加优惠券
    public function addToUser($coupon_id,$user_id){
    	$coupon_id = (int)$coupon_id;
    	$user_id   = (int)$user_id;
    	$sql       = "INSERT into `user_coupon` (user_id,coupon_id,state) 
    				values ($user_id,$coupon_id,1)";
    	return $this->db->sqlExec($sql);
    }

    //找出可用的优惠券
    public function enabelCoupon($user_id,$total){
        $total = (float)$total;
        $userId = (int)$user_id;
        $sql     = "SELECT * from `user_coupon` where `user_id`=$user_id and `state`=1";
        $data    = $this->db->getAll($sql);
        $result = array();
        foreach ($data as $key => $value) {
            $sql = "SELECT * from `coupon` where `limit`<=$total";
            //还没判日期
            array_push($result, $this->db->getOne($sql));
        }
        return $result;
    }

    //用户使用优惠券 成功则返回优惠券信息
    public function useCoupon($coupon_id,$order_id,$user_id){
    	$coupon_id 			= (int)$coupon_id;
    	$order_id  			= (int)$order_id;
    	$user_id   			= (int)$user_id;
    	$coupon_user_info   = $this->checkBelong($coupon_id,$user_id);
    	if(empty($coupon_user_info)){
    		return 0;
    	}else{
    		$id = $coupon_user_info['id'];
    	}
    	//if($this->checkExpired($coupon_id)==1){
        //    var_dump(1);
    		$sql = "UPDATE `user_coupon` set `order_id`=$order_id,`state`=2
    				where `state`=1 and `id`=$id";
    		if($this->db->sqlExec($sql)){
    			$result = $this->db->getValue("SELECT `value` from `coupon` where `id`=$coupon_id");
    		}else{
    			return 0;
    		}
    	//}else{
    	//	return 0;
    	//}
    	return $result;
    }

    //根据id获取单个优惠券信息
    public function oneCoupon($id){
    	$id  = (int)$id;
    	$sql = "select * from `coupon` where `id` = $id";
    	return $this->db->getOne($sql);
    }


    //获取所有优惠券信息
    public function allCoupon(){
    	$sql = "select * from `coupon` where 1=1";
    	return $this->db->getAll($sql);
    }

    //检查是否过期 
    //如不设置结束时间则永不过期
    public function checkExpired($id){
    	$id       = (int)$id;
    	$sql      = "select `end_time` from `coupon` where id=$id";
    	$end_time = $this->db->getValue($sql);
    	if(!empty($end_time)){
    		$today = date('Y-m-d');
    		if(strtotime($today)>strtotime($end_time)){
    			return 0;
    		}
    	}
    	return 1;
    }

    //检查优惠券是否属于本人，同时用户此优惠券信息以保证每次只修改一个
    public function checkBelong($coupon_id,$user_id){
    	$coupon_id = (int)$coupon_id;
    	$user_id   = (int)$user_id;
    	$sql = "SELECT * from `user_coupon` where `user_id`=$user_id and `coupon_id`=$coupon_id";
    	return $this->db->getOne($sql);
    }
}