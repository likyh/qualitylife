<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/21
 * Time: 20:26
 */
import("custom.data.cartMode");
import("custom.data.goodsInfoMode");
import("custom.data.labelMode");

class orderMode extends Data{
    /** @return orderMode */
    public static function init() {
        return parent::init();
    }
    //得到订单所有id商品信息
    public function allIdContent($ids){
        if(empty($ids)){
            return null;
        }
        $idCondition=$this->db->getIdCondition($ids);
        $sql="select `id`,`name`,`selling_price`,`original_price`,`picture_ids`,`label_ids` from `goods` where {$idCondition}";
        $data=$this->db->getAll($sql);

        $picture=pictureMode::init();
        $label=labelMode::init();

        foreach($data as $k=>$v){
            if(!empty($v['picture_ids'])){
                $data[$k]['picture']=$picture->firstIdUrl($v['picture_ids']);
            }else{
                $data[$k]['picture']=null;
            }
            //标签信息
            if(!empty($v['label_ids'])){
                $labels=$label->oneLabel($v['label_ids']);
                $labelStr='';
                foreach($labels as $key=>$value){
                    $labelStr.=$value['name'];
                    $labelStr.=' ';
                }
                $data[$k]['labels']=$labelStr;
            }else{
                $data[$k]['labels']=null;
            }
        }
        return $data;
    }

    /**
     * 用户订单处理，在userInfo里面被调用
     */
    public function detail($orderId){
        $orderId=(int)$orderId;
        $data=array('order O'=>array('id','user_id','address_id','order_amount','payment_amount',
            'create_time','payment_time','freight','express_name','express_num','state','goods_ids','remark'));
        $condition=array('O.id'=>$orderId);
        $orderSql=$this->db->selectSql($data,$condition);

        $result=$this->db->getOne($orderSql);
        $data2=array(
            'goods_in_order GO'=>array('goods_id', 'attribute_id', 'number', 'state'),
            'attribute A'=>array('name'=>'attribute_name'),
            'goods G'=>array('name','selling_price','original_price')
        );
        $condition2=array('GO.order_id'=>$orderId,'GO.attribute_id=A.id','G.id=GO.goods_id');
        $goodsSql=$this->db->selectSql($data2,$condition2);
        $result['goods_in_order']=$this->db->getAll($goodsSql);
        $picture=pictureMode::init();
        foreach($result['goods_in_order'] as &$v){
            $v['url']=$picture->onePicOfGoods($v['goods_id']);
        }
        return $result;
    }

    /**
     * 用户订单处理，在userInfo里面被调用
     * @return array($list,$total,$count)
     */
    public function userOrderManager($userId,$rows=10,$offset=0,$state=null){
        $userId=(int)$userId;
        $rows=(int)$rows;
        $offset=(int)$offset;
        $state=(int)$state;
        $data=array('order O'=>array('id','user_id','address_id','order_amount','payment_amount',
            'create_time','freight','express_name','express_num','state','goods_ids'));
        $condition=array('O.user_id'=>$userId,'O.state'=>$state);
        $attach='order by `create_time` DESC';
        $selectSql=$this->db->selectSql($data,$condition,$attach,$rows,$offset);
        $list=$this->db->getAll($selectSql);
        //var_dump($list);
        $count=count($list);
        $totalData=array('count(1)','order O'=>array());
        $totalCondition=array('O.user_id'=>$userId,'O.state'=>$state);
        $totalSql=$this->db->selectSql($totalData,$totalCondition,'',null,0);
        $total=$this->db->getValue($totalSql);
        //var_dump($total);
        foreach($list as $k=>&$v){
            if(!empty($v['goods_ids'])){
                $v['goods']=$this->allIdContent($v['goods_ids']);
                foreach ($v['goods'] as $q => &$p) {
                    $sql = "SELECT * from `goods_in_order` where `order_id`={$v['id']} and `goods_id`={$p['id']}";
                    $p['in_order'] = $this->db->getOne($sql);
                }
            }else{
                $v['goods']=array();
            }

            if(!empty($v['state'])){
                $v['stateInfo']=orderMode::getStateInfo((int)$v['state']);
            }else{
                $v['stateInfo']="";
            }
        }
        //var_dump($list);
        return array($list,$total,$count);
    }
    //生成订单
    //TODO freight自动生成
    /**
     * @param $userId
     * @param array $cartIds 包括很多个cartIds的索引数组
     * @return array(state,message)
     */
    public function create($userId,$cartIds,$address_id,$freight){
        if(!is_array($cartIds)||empty($cartIds)){
            return array(false,'数据格式错误');
        }
        $userId=(int)$userId;
        //订单有一个商品或多个商品用相同方法处理
        $cart=cartMode::init();
        $cartData=$cart->userCart($userId,$cartIds);
        if(empty($cartData)){
            return array(false,'没有购物');
        }
        $cartIds=array();
        $goods_ids=array();
        $orderAmount=0;
        if(!$this->checkStock($cartData)){          //检查库存量并减
            return array(false,'购物车商品库存量不够');
        }
        foreach($cartData as $v){
            $goods_ids[]=(int)$v['goods_id'];
            $cartIds[]=(int)$v['id'];
            $orderAmount+=(int)$v['number']*(float)$v['selling_price'];
        }
        $data['goods_ids']=implode(',',$goods_ids);
        $data['user_id']=(int)$userId;
        $data['address_id']=(int)$address_id;
        $data['order_amount']=(float)$orderAmount+(float)$freight;
        $data['payment_amount']=$data['order_amount'];
        $data['freight']=(float)$freight;
        $data['state']=1;
        $re=$this->db->insert('order',$data);       //order TABLE first
        if($re){                                    //goods_in_order then
            $orderId=$this->db->insertId();
            $re2=true;
            foreach($cartData as $v){
                $data2['order_id']=(int)$orderId;
                $data2['goods_id']=(int)$v['goods_id'];
                $data2['number']=(int)$v['number'];
                $data2['attribute_id']=(int)$v['attribute_id'];
                $re2= $re2&&$this->db->insert('goods_in_order',$data2)>0;
            }
            if($re2){                               //cart finally
                $cart->removeFromCart($cartIds);
                return array(true,$orderId);
            }
        }
        return array(false,'系统错误，下单失败');
    }
    //用户不经购物车直接购买
    public function directCreate($orignData) {
        //为了用checkstock函数，我假装购物车。。
        $cartData[0] = $orignData;
        if(!$this->checkStock($cartData)){
            return array(false,'商品库存量不够');
        }
        $gopen_id = $orignData['gopen_id'];
        $sql = "select `id`,`selling_price` from goods where `gopen_id`=$gopen_id and `enable`=1";
        $goodsInfo = $this->db->getOne($sql);
        $price = $goodsInfo['selling_price'];
        $cartData[0]['goods_id'] = $goodsInfo['id'];
        //var_dump($orignData);
        $data['goods_ids']=(int)$goodsInfo['id'];
        $data['user_id']=(int)$orignData['user_id'];
        $data['address_id']=(int)$orignData['address_id'];
        $data['order_amount']=((float)$price*$orignData['number'])+(float)$orignData['freight'];
        $data['payment_amount']=$data['order_amount'];
        $data['freight']=(float)$orignData['freight'];
        $data['state']=1;
        $re = $this->db->insert('order',$data);
        if($re){                                    //goods_in_order then
            $orderId=$this->db->insertId();
            $re2=true;
            foreach($cartData as $v){
                $data2['order_id']=(int)$orderId;
                $data2['goods_id']=(int)$v['goods_id'];
                $data2['number']=(int)$v['number'];
                $data2['attribute_id']=(int)$v['attribute_id'];
                $re2= $re2&&$this->db->insert('goods_in_order',$data2)>0;
            }
            if($re2){                               //cart finally
                return array(true,$orderId);
            }
        }
        return array(false,'系统错误，下单失败');
    }
    //下订单前，检查数量与库存量,库存量自减
    public function checkStock($cartData){
        foreach($cartData as $k=>$v){
            $attribute_id=(int)$v['attribute_id'];
            $number=(int)$v['number'];
            $gopen_id = (int)$v['gopen_id'];
            $sql="select `stock` from `attribute` where `id`=$attribute_id";
            $stock=(int)$this->db->getValue($sql);
            if($stock>=$number){
                $data['stock']=$stock-$number;
                $this->db->update('attribute',$attribute_id,$data,true);
                $sql = "select `stock` from goods where `gopen_id`=$gopen_id";
                $totalStock = $this->db->getValue($sql);
                $leftStock=(int)$totalStock-$number;
                $sql = "update `goods` set `stock`=$leftStock where `gopen_id`=$gopen_id";
                $this->db->sqlExec($sql);
            }else{
                return false;
            }
        }
        return true;
    }
    //用户取消订单
    public function cancel($userId,$orderId){
        if(!$this->checkOrderBelongToUser($userId,$orderId)){
            return false;
        }
        $orderId=(int)$orderId;
        $deleteOrder=$this->db->delete('order',$orderId);       //order first？TODO
        if($deleteOrder){
            $infoS=$this->goodsInOrderInfo($orderId);
            foreach ($infoS as $v) {
                $this->stockInc($v['attribute_id'],$v['number']);//stock then？
                $this->db->delete('goods_in_order',(int)$v['id']);
            }
            return true;
        }
        return false;
    }
    //取消订单，库存量增加
    public function stockInc($attribute_id,$number){
        $attribute_id=(int)$attribute_id;
        $number=(int)$number;
        $sql="select `stock` from `attribute` where `id`=$attribute_id";
        $stock=(int)$this->db->getValue($sql);
        $data['stock']=$stock+$number;
        return $this->db->update('attribute',$attribute_id,$data,true);
    }
    /**
     * 订单商品评论打分
     * @param $goodsInOrderId,$comment,$product_quality_score,$customer_service_score
     * @return bool
     */
    public function addCommentScore($goodsInOrderId,$comment,$product_quality_score,$customer_service_score){
        $goodsInOrderId=(int)$goodsInOrderId;
        $data['comment']=$comment;
        $data['comment_time']=date('Y-m-d h:i:s',time());
        $data['product_quality_score']=(float)$product_quality_score;
        $data['customer_service_score']=(float)$customer_service_score;
        $data['state']=2;
        return $this->db->update('goods_in_order',$goodsInOrderId,$data)==1;
    }

    /**
     * 卖家回复
     * @param $goodsInOrderId,$reply
     * @return bool
     */
    public function addReply($goodsInOrderId,$reply){
        $goodsInOrderId=(int)$goodsInOrderId;
        $data['reply']=$reply;
        $data['reply_time']=date('Y-m-d h:i:s',time());
        $data['state']=3;
        return $this->db->update('goods_in_order',$goodsInOrderId,$data)==1;
    }

    /**
     * 根据orderId查询所有goods_in_order表匹配的id
     */
    public function goodsInOrderInfo($orderId){
        $orderId=(int)$orderId;
        $sql="select `id`,`attribute_id`,`number` from `goods_in_order` where `order_id`=$orderId";
        return $this->db->getAll($sql);
    }

    /**
     * 检查用户id和订单id匹配否
     */
    public function checkOrderBelongToUser($userId,$orderId){
        $userId=(int)$userId;
        $orderId=(int)$orderId;
        $sql="select 1 from `order` where `user_id`=$userId and `id`=$orderId";
        return $this->db->getExist($sql);
    }

    //改变订单状态
    public function changeState($orderId,$state){
        $state = (int)$state;
        $sql = "update `order` set `state`={$state} where `id`={$orderId}";
        return $this->db->sqlExec($sql);
    }

    //获得商品状态信息
    public static function getStateInfo($state){
        switch($state){
            case 1:
                return "未付款";
            case 2:
                return "已付款";
            case 3:
                return "未发货";
            case 4:
                return "已发货";
            case 5:
                return "交易完成";
            case 6:
                return "订单取消";
            default:
                return "";
        }
    }
    //获得当前订单状态
    public function getState($order_id){
        $order_id = (int)$order_id;
        return $this->db->getValue("SELECT `state` from `order` where `id`=$order_id");
    }
    //优惠券相关
    //public function change_payment($order_id,$discount=0,$freight=0){
    //    $order_id = (int)$order_id;
    //    $discount = (float)$discount;
    //    $freight  = (float)$freight;
    //    $order_amount = $this->db->getValue("SELECT `order_amount` from `order` where `id`=$order_id");
    //    $payment_amount = (float)$order_amount-$discount-$freight;
    //    $sql = "UPDATE `order` set `payment_amount`=$payment_amount where `id`=$order_id";
    //    return $this->db->sqlExec($sql);
    //}

    //修改订单
    public function updateOrder($order_id,$data){
        $order_id = (int)$order_id;
        return $this->db->update('`order`',$order_id,$data);
    }
} 