<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2015/1/27
 * Time: 16:52
 */

class copywritingMode extends Data{
    /** @return copywritingMode */
    public static function init() {
        return parent::init();
    }
    public function get(){
        $sql="SELECT C.*,P.`url` as `picurl` FROM `copywriting` C,`picture` P where P.`id`=C.`picture_id` limit 1";
        return $this->db->getOne($sql);
    }
    public function modify($id,$picture_id,$title1,$text1,$title2,$text2,$title3,$text3,$title4,$text4,$title5,
                           $text5,$title6,$text6){
        $id=(int)$id;
        $data['picture_id']=(int)$picture_id;
        $data['title1']=$title1;
        $data['text1']=$text1;
        $data['title2']=$title2;
        $data['text2']=$text2;
        $data['title3']=$title3;
        $data['text3']=$text3;
        $data['title4']=$title4;
        $data['text4']=$text4;
        $data['title5']=$title5;
        $data['text5']=$text5;
        $data['title6']=$title6;
        $data['text6']=$text6;
        return $this->db->update('copywriting',$id,$data)==1;
    }
} 