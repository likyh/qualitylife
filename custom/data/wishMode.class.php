<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 2014/12/26
 * Time: 13:02
 */

class wishMode extends Data{
    public static function init() {
        return parent::init();
    }
    /**
     * 用户愿望清单
     */
    public function wishList($userId,$rows,$offset=0){
        $userId=(int)$userId;
        $rows=(int)$rows;
        $offset=(int)$offset;
        $data=array('wish_list WL'=>array('id','user_id','gopen_id'),
            'goods G'=>array('name'=>'goods_name','selling_price','original_price','picture_ids'));
        $condition=array('user_id'=>$userId,'WL.gopen_id = G.gopen_id');
        $attach='order by WL.`create_time` DESC';
        $sql=$this->db->selectSql($data,$condition,$attach,$rows,$offset);
        $selectData['list']=$this->db->getAll($sql);
        $selectData['count']=count($selectData['list']);
        $totalSql="select count(*)
              from `wish_list` WL,`goods` G
              where WL.gopen_id=G.gopen_id
              and WL.`user_id`=$userId";
        $selectData['total']=$this->db->getValue($totalSql);
        $picture=pictureMode::init();
        foreach($selectData['list'] as &$v){
            if(!empty($v['picture_ids'])){
                $v['picture']=$picture->firstIdUrl($v['picture_ids']);
            }else{
                $v['picture']=null;
            }
        }
        return $selectData;
    }
    /**
     * 检查用户是否已收藏商品
     * @param $userId,$gopen_id
     * @return int|bool
     */
    public function checkWish($userId,$gopen_id){
        $userId=(int)$userId;
        $gopen_id=(int)$gopen_id;
        $sql="select id from `wish_list` where `user_id`=$userId and `gopen_id`=$gopen_id";
        $id=$this->db->getValue($sql);
        if(!empty($id))return $id;
        return false;
    }
    /**
     * 将商品添加到愿望清单
     * @param $userId,$gopen_id
     * @return string
     */
    public function addToWish($userId,$gopen_id){
        if($this->checkWish($userId,$gopen_id)) return '您已经添加过了噢';
        $data['user_id']=(int)$userId;
        $data['gopen_id']=(int)$gopen_id;
        if($this->db->insert('wish_list',$data)==1){
            return 'success';
        }
        return '添加失败，请重试';
    }

    /**
     * 将商品移除愿望清单
     * @param $userId,$gopen_id
     */
    public function removeFromWish($userId,$gopen_id){
        $id=$this->checkWish($userId,$gopen_id);
        if(!$id) return '您的愿望清单里没有该商品噢';
        if($this->db->delete('wish_list',$id)){
            return 'success';
        }
        return '移除失败，请重试';
    }
} 