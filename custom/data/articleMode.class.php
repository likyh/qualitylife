<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/12/13
 * Time: 21:32
 */

class articleMode extends Data{
    public static function init() {
        return parent::init();
    }
    //TODO 分页
    //求别分页。。。
    //文章列表，按日期倒顺排序
    public function getList($type=null){
        if(!empty($type)){
            $condition=" and `type` = {$this->db->quote($type)}";
        }else{
            $condition='';
        }
        $sql="SELECT * from `article` where `enable`=1 ".$condition." order by `type`,`date` desc";
        return $this->db->getAll($sql);
    }
    //文章详情
    public function detail($id){
        $id=(int)$id;
        $sql="SELECT * from `article` where `id`=$id";
        return $this->db->getOne($sql);
    }
    //之搜出id和title
    public function brief($type=null){
        if(!empty($type)){
            $condition=" and `type` = {$this->db->quote($type)}";
        }else{
            $condition='';
        }
        $sql="SELECT `id`,`title` from `article` where `enable`=1 ".$condition." order by `type`,`date` desc";
        //echo $sql;
        return $this->db->getAll($sql);
    }
    public function add($title,$content,$type,$date=null){
        $data['title']=$title;
        $data['content']=$content;
        $data['date']=$date;
        $data['type']=$type;
        return $this->db->insert('article',$data)==1;
    }
    public function modify($id,$title,$content,$type,$date){
        $id=(int)$id;
        $data['title']=$title;
        $data['content']=$content;
        $data['date']=$date;
        $data['type']=$type;
        return $this->db->modify('article',$id,$data)==1;
    }
    public function delete($id){
        $id=(int)$id;
        $sql="UPDATE `article` SET `enable`=0 where `id`=$id";
        return $this->db->sqlExec($sql)==1;
    }
} 