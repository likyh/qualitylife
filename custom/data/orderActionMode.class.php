<?php


import("custom.data.cartMode");
import("custom.data.goodsInfoMode");
import("custom.data.labelMode");

class orderActionMode extends Data{
    public static function init() {
        return parent::init();
    }

    public function show(){
        $sql="SELECT 
                            id,
                            user_id,
                            address_id,
                            order_amount,
                            payment_amount,
                            create_time,
                            payment_time,
                            freight,
                            express_name,
                            express_num,
                            state
                            FROM `order` 
                            ORDER BY create_time DESC ";
        return $this->db->getAll($sql); 
    }

    public function orderState($id){
        $sql="SELECT state FROM `order` WHERE `id`=$id";
        return $this->db->getOne($sql);
    }

    public function update($id,$state){
        $data['state']=(int)$state;
        $id=(int)$id;
        return $this->db->update('`order`',$id,$data)==1;
    }
    
} 