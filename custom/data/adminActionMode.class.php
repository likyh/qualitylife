<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/30
 * Time: 22:24
 */

class adminActionMode extends Data{
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
    }
    /**
     * @param $username
     * @param $pass
     * @return string
     */
    public function login($username,$pass){
        $username=$this->db->quote($username);
        $code=getPassWord($username,$pass);
        $code=$this->db->quote($code);
        $sql="select `id`, `username`, `tel`, `email`, `type` from `admin` where `username`={$username} and `code`={$code}";
        $re=$this->db->getOne($sql);
        //var_dump($re);
        if(!empty($re)){
            $_SESSION['admin']['login']=true;
            $_SESSION['admin']['info']=$re;
            return true;
        }else{
            return false;
        }
    }
    function checkLogin(){
        return isset($_SESSION['admin']['login'])&& $_SESSION['admin']['login'];
    }
    function getUserInfo(){
        if($this->checkLogin()){
            return $_SESSION['admin']['info'];
        }else{
            return null;
        }
    }

    /**
     * 修改密码
     * @param $adminId,$username,$oldPass,$newPass
     * @return bool
     */
    public function alterPass($adminId,$username,$oldPass,$newPass){
        $adminId=(int)$adminId;
        $username=$this->db->quote($username);
        $oldPass=getPassWord($username,$oldPass);
        echo 'oldPass='.$oldPass;
        $sql="select `code` from `admin` where `id`=$adminId";
        $hashPass=$this->db->getValue($sql);
        if($oldPass==$hashPass){
            $newPass=getPassWord($username,$newPass);
            $data['code']=$newPass;
            return $this->db->modify('admin',$adminId,$data)==1;
        }
        return false;
    }

    /**
     * 手动添加管理员，只有用户名，密码，类型
     */
    public function addAdmin($username,$pass,$type=2){
        if($this->checkExist($username)) return false;
        $code=getPassWord($username,$pass);
        $data['username']=$username;
        $data['code']=$code;
        return $this->db->insert('admin',$data)==1;
    }

    /**
     * 检查用户名
     *@param $username
     */
    public function checkExist($username){
        $username=$this->db->quote($username);
        $sql="select 1 from `admin` where `username`={$username}";
        return $this->db->getExist($sql);
    }

    /**
     * 修改除密码外其他信息
     * @param $id
     * @param $data注意格式（不能修改type
     */
    public function modify($id,$data){
        $id=(int)$id;
        if(!is_array($data)||empty($data)){
            return false;
        }
        return $this->db->modify('admin',$id,$data)==1;
    }

    /**
     * 修改权限
     */
    public function typeModify($id,$type){
        $id=(int)$id;
        $data['type']=(int)$type;
        return $this->db->modify('admin',$id,$data)==1;
    }

    /**
     * 删除管理员
     */
    public function delete($id){
        $id=(int)$id;
        return $this->db->delete('admin',$id)==1;
    }
    public function logout(){
        $this->session->release();
    }
}
