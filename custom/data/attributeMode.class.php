<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/28
 * Time: 18:09
 */

class attributeMode extends Data{
    /**
     * 添加
     * @param $gopen_id,$name,$stock
     * @return bool
     */
    public function add($gopen_id,$name,$stock){
        $data['gopen_id']=(int)$gopen_id;
        $data['name']=$name;
        $data['stock']=(int)$stock;
        if($this->checkExist($gopen_id,$name)){
            return false;
        }
        return $this->db->insert('attribute',$data)==1;
    }

    /**
     * 修改
     * @param $id,$gopen_id,$name,$stock
     * @return bool
     */
    public function modify($id,$gopen_id,$name,$stock){
        $gopen_id=(int)$gopen_id;
        $data['name']=$name;
        $data['stock']=(int)$stock;
        if($this->checkExist($gopen_id,$name)){
            return false;
        }
        $id=(int)$id;
        return $this->db->modify('attribute',$id,$data)==1;
    }

    /**
     * 删除一个属性信息
     * @param $id
     */
    public function delete($id){
        $id=(int)$id;
        return $this->db->delete('attribute',$id)==1;
    }

    /**
     * 检查存在/重复
     * @param $gopen_id,$name
     * @return bool
     */
    public function checkExist($gopen_id,$name){
        $gopen_id=(int)$gopen_id;
        $name=$this->db->quote($name);
        $sql="select 1 from `attribute` where `gopen_id`=$gopen_id and `name`={$name}";
        return $this->db->getExist($sql);
    }
    /**
     * 显示某商品已有属性
     * @param $gopen_id
     */
    public function goodsAttribute($gopen_id){
        $gopen_id=$this->db->quote($gopen_id);
        $sql="select * from `attribute` where `gopen_id`=$gopen_id";
        return $this->db->getAll($sql);
    }

    public function getOneAttr($id){
        $id=$this->db->quote($id);
        $sql="select * from `attribute` where `id`=$id ";
        return $this->db->getOne($sql);
    }
} 