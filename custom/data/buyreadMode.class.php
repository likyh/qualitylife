<?php


class buyreadMode extends Data{
    

    public function show(){
        $sql="SELECT id ,name FROM buyread ORDER BY id";
        return $this->db->getAll($sql);
    }

    public function add($name,$content){
        $data['name']=$name;
        $data['content']=$name;
        if($this->checkExist($name)){
            return false;
        }
        return $this->db->insert('buyread',$data)==1;
    }



    public function checkExist($name){
        $name=$this->db->quote($name);
        $sql="SELECT id FROM `buyread` WHERE  name='{$name}'";
        return $this->db->getExist($sql);
    }

    public function getOneBuyread($id){
        $id=$this->db->quote($id);
        $sql="SELECT id,name,content FROM `buyread` WHERE  id=$id";
        return $this->db->getOne($sql);
    }


    public function update($id,$name,$content){
        $data['name']=$name;
        $data['content']=$content;
        $id=(int)$id;
        return $this->db->modify('buyread',$id,$data)==1;
    }

    public function delete($id){
        return $this->db->delete('buyread',$id)==1;
    }


} 


