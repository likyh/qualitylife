<?php


class serviceMode extends Data{
    

    public function show(){
        $sql="SELECT id ,name FROM service ORDER BY id";
        return $this->db->getAll($sql);
    }

    public function add($name,$content){
        $data['name']=$name;
        $data['content']=$name;
        if($this->checkExist($name)){
            return false;
        }
        return $this->db->insert('service',$data)==1;
    }



    public function checkExist($name){
        $sql="SELECT id FROM `service` WHERE  name='{$name}'";
        return $this->db->getExist($sql);
    }

    public function getOneService($id){
        $id=$this->db->quote($id);
        $sql="SELECT id,name,content FROM `service` WHERE  id=$id";
        return $this->db->getOne($sql);
    }


    public function update($id,$name,$content){
        $data['name']=$name;
        $data['content']=$content;
        $id=(int)$id;
        return $this->db->modify('service',$id,$data)==1;
    }

    public function delete($id){
        return $this->db->delete('service',$id)==1;
    }


   

} 


