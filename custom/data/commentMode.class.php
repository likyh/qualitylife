<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/12/27
 * Time: 9:06
 */

class commentMode extends Data{
    /** @return commentMode */
    public static function init() {
        return parent::init();
    }
    /**
     * 商品评论+回复列表
     * @param $gopen_id,$rows,$offset
     */
    public function commentList($gopen_id,$rows,$offset=0){
        $gopen_id=(int)$gopen_id;
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="SELECT SQL_CALC_FOUND_ROWS C.*,U.`username` FROM `comment` C,`user` U WHERE C.`gopen_id`=$gopen_id and C.`user_id`=U.`id` order by C.`comment_time` DESC limit $offset,$rows";
        $list=$this->db->getAll($sql);
        $total=(int)$this->db->getValue("SELECT FOUND_ROWS()");
        return array($list,$total);
    }
    /**
     * 订单商品评论打分
     * @param $goodsInOrderId,$gopen_id,$userId,$comment,$product_quality_score,$customer_service_score
     * @return bool
     */
    public function addCommentScore($orderId,$goodsInOrderId,$gopen_id,$userId,$comment,$product_quality_score,$customer_service_score){
        $goodsInOrderId=(int)$goodsInOrderId;
        $orderId=(int)$orderId;
        //先检查是否评论过
        $sql = "SELECT `state` from `goods_in_order` where `id`=$goodsInOrderId";
        if($this->db->getValue($sql)>1){
            //var_dump(1);
            return false;
        }
        $sql = "SELECT `state` from `order` where `id`=$orderId";
        if($this->db->getValue($sql)!=5){
            //var_dump(2);
            return false;
        }
        //先变state
        $this->stateChange($goodsInOrderId,2);
        $data['goods_in_order_id']=$goodsInOrderId;
        $data['gopen_id']=(int)$gopen_id;
        $data['user_id']=(int)$userId;
        $data['comment']=$comment;
        $data['comment_time']=date('Y-m-d h:i:s',time());
        $data['product_quality_score']=(float)$product_quality_score;
        $data['customer_service_score']=(float)$customer_service_score;
        //var_dump($data);
        return $this->db->insert('comment',$data)==1;
    }
    /**
     * 卖家回复
     * @param $goodsInOrderId,$commentId,$reply
     * @return bool
     */
    public function addReply($goodsInOrderId,$commentId,$reply){
        $commentId=(int)$commentId;
        //先变state
        $this->stateChange($goodsInOrderId,3);
        $data['reply']=$reply;
        $data['reply_time']=date('Y-m-d h:i:s',time());
        return $this->db->update('comment',$commentId,$data)==1;
    }
    /**
     * 修改goods_in_order状态（默认1未评价，2已评价，3卖家已回复）
     * @param $goodsInOrderId,$state
     */
    public function stateChange($goodsInOrderId,$state){
        $goodsInOrderId=(int)$goodsInOrderId;
        $data['state']=(int)$state;
        $this->db->update('goods_in_order',$goodsInOrderId,$data);
    }

    /**
     * 用户自己所有评论列表
     * @param $userId,$rows,$offset
     */
    public function userComment($userId,$rows,$offset=0){
        import("custom.data.goodsInfoMode");
        $goods = goodsInfoMode::init();
        $userId=(int)$userId;
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM `comment` where `user_id`=$userId  order by `comment_time` DESC limit $offset,$rows";
        $result['list']=$this->db->getAll($sql);
        foreach ($result['list'] as $key => &$value) {
            $value['goodsInfo'] = $goods->brief($value['gopen_id']);
        }
        $result['total']=(int)$this->db->getValue("SELECT FOUND_ROWS()");
        return $result;
    }
} 