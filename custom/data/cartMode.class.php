<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/26
 * Time: 19:52
 */
import("custom.data.pictureMode");
class cartMode extends Data{
    /** @var  pictureMode */
    protected $picture;
    /** @return cartMode */
    public static function init() {
        return parent::init();
    }
    protected function __construct() {
        parent::__construct();
        $this->picture=pictureMode::init();
    }
    /**
     * 检查购物车id是否存在
     * @return bool
     */
    public function checkCartIdExist($cartId){
        $cartId=(int)$cartId;
        $sql="select 1 from `cart` where `id`=$cartId";
        return $this->db->getExist($sql);
    }

    /**TODO
     * 购物车列表（购物车应该要有个商品种类限制，不能放太多）
     * @param $userId
     */
    public function userCart($userId, $cartIds=null){
        $userId=(int)$userId;
        $sql="select C.id,C.gopen_id,C.number,G.id as goods_id,G.name,G.selling_price,G.original_price,
              A.name as attribute,A.id as attribute_id,G.picture_ids
              from `cart` C,`goods` G,`attribute` A
              where `user_id`=$userId
              and G.gopen_id=C.gopen_id
              and G.enable=1
              and A.`id`=C.attribute_id";
        if(!empty($cartIds)){
            $sql.=" and C.".$this->db->getIdCondition($cartIds);
        }
        $data=$this->db->getAll($sql);
        foreach ($data as &$v) {
            $v['picture'] = !empty($v['picture_ids'])?$this->picture->firstIdUrl($v['picture_ids']):null;
        }
        return $data;
    }

    /**
     * 用户添加商品到购物车
     * @param $userId,$gopen_id,$attribute_id,$number
     * @return bool
     */
    public function addToCart($userId,$gopen_id,$attribute_id,$number){
        $userId=(int)$userId;
        $number=(int)$number;
        $v=$this->checkGoods($userId,$gopen_id,$attribute_id);
        if(!empty($v)){         //本来存在
            $id=(int)$v['id'];
            $data['number']=(int)$v['number']+$number;
            $check=$this->checkNumber($attribute_id,$data['number']);   //检查数量限度
            if(!$check){
                //return array('s'=>0,'m'=>'不能超过此商品最大购买数量');
              return 0;
            }
            $re=$this->db->update('cart',$id,$data);
        }else{                  //本来不存在
            $data['user_id']=$userId;
            $data['gopen_id']=(int)$gopen_id;
            $data['attribute_id']=(int)$attribute_id;
            $data['number']=$number;
            $check=$this->checkNumber($attribute_id,$number);   //检查数量限度
            if(!$check){
                //return array('s'=>0,'m'=>'不能超过此商品最大购买数量');
                return 0;
            }
            $re=$this->db->insert('cart',$data);
        }
        return $re==1;
    }

    /**
     * 将某物品移出购物车
     * @param array|int $cartId
     * @return bool
     */
    public function removeFromCart($cartIds){
        return $this->db->delete('cart',$cartIds)==count($cartIds);
    }
    /**
     * 判断指定商品是否在购物车中
     * @param $userId,$gopen_id
     */
    public function checkGoods($userId,$gopen_id,$attribute_id=null){
        $userId=(int)$userId;
        $gopen_id=(int)$gopen_id;
        if(!empty($attribute_id)){
            $attribute_id=(int)$attribute_id;
            $attr=' and attribute_id='.$attribute_id;
        }else{
            $attr='';
        }
        $sql="select `id`,`number` from `cart`
              where user_id=$userId
              and gopen_id=$gopen_id ".$attr;
        $v=$this->db->getOne($sql);
        return empty($v)?null:$v;
    }
    //修改购物车数量
    public function numberModify($cartId,$number){
        $cartId=(int)$cartId;
        $attribute_id=(int)$this->db->getValue("select attribute_id from cart WHERE id=".$this->db->quote($cartId));
        $data['number']=(int)$number;
        if($this->checkNumber($attribute_id,$number)){
            $re=$this->db->update('cart',$cartId,$data);
            return $re?'success':'修改失败';
        }else{
            return '您需要的数量已经超过库存，请联系客服确认';
        }

    }
    //购物车数量不能超过库存量
    public function checkNumber($attribute_id,$number){
        $attribute_id=(int)$attribute_id;
        $number=(int)$number;
        $sql="select `stock` from `attribute` where `id`=$attribute_id";
        $stock=(int)$this->db->getValue($sql);
        return $stock>=$number?true:false;
    }
}