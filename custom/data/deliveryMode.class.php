<?php


class deliveryMode extends Data{
    

    public function show(){
        $sql="SELECT id ,name FROM delivery ORDER BY id";
        return $this->db->getAll($sql);
    }

    public function add($name,$content){
        $data['name']=$name;
        $data['content']=$name;
        if($this->checkExist($name)){
            return false;
        }
        return $this->db->insert('delivery',$data)==1;
    }



    public function checkExist($name){
        $sql="SELECT id FROM `delivery` WHERE  name='{$name}'";
        return $this->db->getExist($sql);
    }

    public function getOneDelivery($id){
        $id=$this->db->quote($id);
        $sql="SELECT id,name,content FROM `delivery` WHERE  id=$id";
        return $this->db->getOne($sql);
    }


    public function update($id,$name,$content){
        $data['name']=$name;
        $data['content']=$content;
        $id=(int)$id;
        return $this->db->modify('delivery',$id,$data)==1;
    }

    public function delete($id){
        $id=$this->db->quote($id);
        return $this->db->delete('delivery',$id)==1;
    }


} 


