<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/10/27
 * Time: 15:22
 */

class categoryMode extends Data{
    /** @return categoryMode */
    public static function init() {
        return parent::init();
    }
    /**
     * 添加类别
     */
    public function add($name){
        if($this->checkExist($name)){
            exit("该类别已存在");
        }
        $data['name']=$name;
        return $this->db->insert('goods_category',$data)==1;
    }
    
    /**
     * 修改类别
     */
    public function modify($id,$name){
        $data['name']=$name;
        
        return $this->db->update('goods_category',$id,$data)==1;
    }
    /**
     * 删除类别
     */
    public function delete($id){
        //TODO 批量删除
        $id=(int)$id;
        return $this->db->delete('goods_category',$id)==1;
    }

    //由类别ID获取此类别的信息
    public function getCateInfo($id){
        $id =$this->db->quote($id);
        $sql = "select id,name,pid from goods_category where id=$id";
        $data = $this->db->getOne($sql);
        return $data;
    }
    //由父类别ID获取所有子类别
    public function getSonCategory($id){
        $id=(int)$id;
        $sql = "select * from goods_category where pid=$id";
        $data = $this->db->getAll($sql);
        return $data;
    }
    //由类别ID获取父类别
    public function getFatherCategory($id){
        $data = $this->getCateInfo($id);
        $sql = "select * from goods_category where id={$data['pid']}";
        $data = $this->db->getOne($sql);
        return $data;
    }
    // 获取所有的祖先节点
    public function getParentsCategory($id){
        $parents=array();
        do{
            $data=$this->getCateInfo($id);
            array_unshift($parents,$data);
            $id=$data['pid'];
        }while($id!=0);
        return $parents;
    }

    /**
     * 所有商品分类，并且按照树组合好
     */
    public function allCategory($pid=0){
        $data=$this->getSonCategory($pid);
        foreach(new ArrayIterator($data) as $k=>$v) {
            $data[$k]['children']=$this->allCategory((int)$v['id']);
        }
        return $data;
    }
    /**
     * 获取分类的所有底层分类，用于查询高等级id
     */
    public function getBottomCategory($id=0){
        $origin = $this->allCategory($id);
        if(empty($origin)){
            return $id;
        }
        $new = array();
        $ifBottom = 1; //记录原始数据中是否还有下一层分类，初始为有
        while($ifBottom==1){
            $ifBottom = 0;
            foreach ($origin as $key => $value) {
                if(empty($value["children"])){
                    array_push($new, $value['id']);
                    unset($origin[$key]);
                } else {
                    foreach ($value["children"] as $subKey => $subValue) {
                        array_push($origin, $subValue);
                    }
                    unset($origin[$key]);
                    $ifBottom = 1;
                }
            }
        }
        return $new;
    }



    /**
     * 检查类别名
     */
    public function checkExist($name){
        
        $sql=$this->db->selectSql(array("goods_category"=>array("id")),
            array('name'=>$name));
        return $this->db->getExist($sql);
    }



    //分类列表--一级分类
    public function getMainCategory(){
        $sql = "select id ,name from goods_category where pid=0";
        $data = $this->db->getAll($sql);
        foreach ($data as $key => $value) {
            $showData[$value['id']]=$value['name'];
        }
        return $showData;
    }

    //查看子类
    public function showChlld($id){
        $id=$this->db->quote($id);
        $sql = "select id ,name from goods_category where pid=$id";
        $data = $this->db->getAll($sql);
        foreach ($data as $key => $value) {
            $showData[$value['id']]=$value['name'];
        }

        return $showData;
    }


    //增加子类
    public function addChild($pid,$name){
        if($this->checkExist($name)){
            return false;
        }
        $data['name']=$name;
        $data['pid']=(int)$pid;
        return $this->db->insert('goods_category',$data)==1;
    }


    //获取增加商品页面的所有分类
    public function getGoodsCategory(){
        $mainCategory=$this->getMainCategory();
        foreach ($mainCategory as $key => $value) {
            $sql = "select id ,name from goods_category where pid=$key";
            $data[$value] = $this->db->getAll($sql);
        }
        return $data;
    }
    



}
