<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 2014/10/27
 * Time: 16:36
 */

class parameterMode extends Data{
    /**
     * 显示商品参数
     * @param $gopen_id
     * @return array
     */
    public function goodsParameter($gopen_id){
        $gopen_id=(int)$gopen_id;
        $sql="select * from `parameter` where `gopen_id`=$gopen_id";
        return $this->db->getOne($sql);
    }
    /**
     * 添加商品参数，一个商品只能添加一次
     * @param $gopen_id
     * @param $data格式注意：键值对应数据表字段data里面也要有gopen_id这一项
     */
    public function add($data){
        //$gopen_id=(int)$gopen_id;
        // if($this->checkExist($gopen_id)){
        //     return '该商品的参数信息已存在噢，您可以选择修改';
        // }
        if(!is_array($data)||empty($data)){
            return '数据格式错误';
        }
        return $this->db->insert('parameter',$data)==1;
    }

    /**
     * 检查商品参数存在否
     * @param $gopen_id
     * @return bool
     */
    public function checkExist($gopen_id=null,$id=null){
        if(!empty($gopen_id)){
            $gopen_id=(int)$gopen_id;
            $sql="select 1 from `parameter` where `gopen_id`=$gopen_id";
        }
        if(!empty($id)){
            $id=(int)$id;
            $sql="select 1 from `parameter` where `id`=$id";
        }
        return $this->db->getExist($sql);
    }

    /**
     * @param $id
     * @param $data注意data格式：里面不要gopen_id信息
     */
    public function modify($id,$data){
        if(!is_array($data)||empty($data)){
            return false;
        }
        
        $id=(int)$id;
        return $this->db->modify('parameter',$id,$data)==1;
    }

    public function delete($id){
        $id=(int)$id;
        return $this->db->delete('parameter',$id)==1;
    }
} 