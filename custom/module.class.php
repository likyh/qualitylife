<?php
import("Custom.Data.labelMode");
import("Custom.Data.categoryMode");
import("custom.data.copywritingMode");
class module extends Activity{
	function userBarTask() {
        import("custom.data.userActionMode");
        $r['loged']=userActionMode::init()->checkLogin();
		View::displayAsHtml($r,"module/userBar.php");
	}
	function searchTask() {
        $label=labelMode::init();
        $result['label']=$label->allLabel();
		View::displayAsHtml($result,"module/search.php");
	}
	function serviceTask() {
		View::displayAsHtml(array(),"module/service.php");
	}
	function linkIconTask() {
		View::displayAsHtml(array(),"module/linkIcon.php");
	}
	function footerTask() {
		$result = copywritingMode::init()->get();
		View::displayAsHtml($result,"module/footer.php");
	}
	function headerTask() {
		View::displayAsHtml(array(),"module/header.php");
	}
}