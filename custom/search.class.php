<?php
import("custom.data.pictureMode");
import("custom.data.categoryMode");
class search extends Activity {
	function indexTask(){
		$db = SqlDB::init();
		$picture = pictureMode::init();
		$result = array();
		if(!empty($_GET['page'])){
			$page = (int)$_GET['page'];
		}else{
			$page = 1;
		}
		$page_size = 10;//单页显示10个
		$page_begin = ($page-1)*$page_size ;

		//各种排序方式
        $result['order']['method'] = 'create_time';
        $result['order']['value']  = '1';
        //默认

        //0 无要求 1 DESC default ASCd
        if(!empty($_GET['volume'])){//价格排序 
            $result['order']['method'] = 'volume';
            $result['order']['value']  = (int)$_GET['volume'];
        }
        if(!empty($_GET['create_time'])){//时间排序 
            $result['order']['method'] = 'create_time';
            $result['order']['value']  = (int)$_GET['create_time'];
        }
        if(!empty($_GET['selling_price'])){//排序方式 
            $result['order']['method'] = 'selling_price';
            $result['order']['value']  = (int)$_GET['selling_price'];
        }

		//分页显示的页面数
		$sql = "select `gopen_id`, `name`, `selling_price`, `original_price`, `picture_ids`
			from `goods` where 1=1 ";
        $countSql = "select count(*) from `goods` where 1=1 ";
        if(isset($_GET['searchInput'])&&!empty($_GET['searchInput'])) {
        	$search=htmlspecialchars(strip_tags($_GET['searchInput']));
            $sql.=" and `name` like '%".$search."%'";
            $countSql.=" and `name` like '%".$search."%'";
        }
        $sql.="order by create_time DESC LIMIT $page_begin,$page_size";
		$result['goodslist']=$db->getAll($sql);
		foreach($result['goodslist'] as &$t){
            $t['picture'] = empty($t['picture_ids'])? null: $picture->firstIdUrl($t['picture_ids']);
        }
		$result['page']['currentPage'] = $page;
		$result['page']['totalPage']   = ceil($db->getValue($countSql)/$page_size);

		//var_dump($result);
        $category=categoryMode::init();
        $result['subclasses']=$category->getSonCategory(0);
        $result['searchInput']=$_GET['searchInput'];
        View::displayAsHtml($result,"search.php");
	}
}