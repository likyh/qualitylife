<?php
import("custom.data.userActionMode");
class user extends Activity{
    /** @var  userActionMode */
    protected $userAction;
	protected function __construct() {
		$this->userAction = userActionMode::init();
	}
    function loginTask(){
        View::displayAsHtml(array(),"login.php");
    }
    function logoutTask(){
        $this->userAction->logout();
        header("Location:".WebRouter::init()->getPage("user","login"));
    }
	function loginSubmitTask(){
        if(isset($_POST['loginName'])&&isset($_POST['password'])){
            $loginName = $_POST['loginName'];
            $code  = $_POST['password'];
            $loginResult = $this->userAction->login($loginName,$code);
            //var_dump($_SESSION);
            if($loginResult=='success'){
                if(isset($_COOKIE['backUrl'])){
                    header("Location:".$_COOKIE['backUrl']);    
                }else{
                    header("Location:".WebRouter::init()->getPage("userInfo","index"));    
                }
                
            }else{
                $this->tipState($loginResult);
            }
        }else{
            $this->tipState('信息填写不完整');
        }
	}
    function registerTask(){
        View::displayAsHtml(array(),"register.php");
    }
    function registerSubmit(){
        if(!isset($_POST['regName'])|| !isset($_POST['tel'])||
            !isset($_POST['confirmCode'])|| !isset($_POST['password'])|| !isset($_POST['re_password'])){
            return '信息填写不完整';
        }
        $username = $_POST['regName'];
        $tel = $_POST['tel'];
        $confirmCode = $_POST['confirmCode'];
        $password = $_POST['password'];
        $re_password = $_POST['re_password'];

        if($password!=$re_password){
            return '两次输入的密码不一致';
        }
        return $this->userAction->register($tel,$username,$password);
    }
    // 暂未实现手机验证功能
    function registerSubmitTask(){
        $message=$this->registerSubmit();
        if($message=='success'){
            if(isset($_COOKIE['backUrl'])){
                header("Location:".$_COOKIE['backUrl']); 
            }else{
                header("Location:".WebRouter::init()->getPage("home","index"));    
            }
            
        }else{
            $this->tipState($message);
        }
    }
    function sendCodeTask(){
        $tel = isset($_GET['tel'])&&!empty($_GET['tel'])?$_GET['tel']:0;
        list($result['data']['state'],$result['data']['message']) = $this->userAction->sendCode($tel);
        View::displayAsJson($result);
    }
    function checkCodeTask(){
        $tel = isset($_GET['tel'])&&!empty($_GET['tel'])?(int)$_GET['tel']:0;
        $code = isset($_GET['code'])&&!empty($_GET['code'])?(int)$_GET['code']:0;
        $result['data'] = $this->userAction->checkCode($tel,$code);
        View::displayAsJson($result);
    }
    function tipState($message, $url=null){
        $result['message']=$message;
        $result['url']=$url;
        View::displayAsHtml($result,"state/state.php");
    }
}
