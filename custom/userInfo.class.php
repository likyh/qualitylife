<?php
import("custom.data.userActionMode");
import("custom.data.userInfoMode");
class userInfo extends Activity {
	/** @var  userInfoMode */
	protected $userInfo;
	/** @var  userActionMode */
	protected $userAction;
	protected $address;
	/** @var  SqlDB  */
	protected $db;
	protected function __construct(){
		$this->userInfo = userInfoMode::init();
		$this->userAction = userActionMode::init();
		$this->db = SqlDB::init();
	}
	function indexTask() {
		$user = $this->checkLogin();
		$result = $this->userAction->getDetailInfo();
		View::displayAsHtml($result,"tpl/user/index.php");
	}
	function detailTask() {
		$user = $this->checkLogin();
		$result = $this->userInfo->accountInfo($user['id']);
		//var_dump($_SESSION);
		View::displayAsHtml($result,"tpl/user/detail.php");
	}
	function editInfoTask(){
		$db = sqlDB::init();
		$user = $this->checkLogin();
		if(isset($_POST['newInfo'])&&!empty($_POST['newInfo'])){
			$info = $_POST['newInfo'];
		}else{
			$result = array("state"=>300,"message"=>"您还没有输入");
			View::displayAsJson($result);
			return;
		}
		if($_POST['type']=="username"){
			$type = "username";
			$info = $db->quote($info);
		}else{
			$type = "tel";
			if(!preg_match("/^1[34578]{1}\\d{9}$/", $info)){
				$result = array("state"=>302,"message"=>"请输入正确的手机号");
				View::displayAsJson($result);
				return;
			}
		}
		
		$userId = $user['id'];
		$sql = "UPDATE `user` SET $type = $info WHERE `id`=$userId";

		if($db->sqlExec($sql)){
			$result = array("state"=>200,"message"=>"修改成功");
			$sql="select `id`,`username`,`tel` from `user` where `id`=$userId";
        	$re=$db->getOne($sql);
        	$_SESSION['user']['login']=true;
        	$_SESSION['user']['info']=$re;
		}else{
			$result = array("state"=>400,"message"=>"请不要输入相同的手机号");
		}
		View::displayAsJson($result);
	}
	function alterPassTask() {
		$user = $this->checkLogin();
		$userId = $user['id'];
		//var_dump($user);
		$username = isset($_POST['username'])&&!empty($_POST['username'])?$_POST['username']:0;
		$oldPass  = isset($_POST['oldPass'])&&!empty($_POST['oldPass'])?$_POST['oldPass']:0;
		$newPass  = isset($_POST['newPass'])&&!empty($_POST['newPass'])?$_POST['newPass']:0;
		if($this->userAction->alterPass($userId,$username,$oldPass,$newPass)){
			$result = array("state"=>200,"message"=>"修改成功");
			$this->userAction->logout();
		}else{
			$result = array("state"=>400,"message"=>"您填写的用户名与旧密码有误");
		}
		View::displayAsJson($result);
	}
	function orderTask() {
		$user = $this->checkLogin();
		import("custom.data.orderMode");
		$order=orderMode::init();
		$currentPage = isset($_GET['page'])&&!empty($_GET['page'])?(int)$_GET['page']:1;
		$rows=5;
        $url=WebRouter::init()->getQuestion("page=");
		list($result['list'],$result['total'])=
			$order->userOrderManager($user['id'],$rows,($currentPage-1)*$rows);
		$result['page']=new Page($currentPage,$result['total']);
		$result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
		//var_dump($result);
		View::displayAsHtml($result,"tpl/user/order.php");
	}
	function addressTask() {
		import("custom.data.addressMode");
		$user = $this->checkLogin();
		$address=addressMode::init();
		$result['address'] = $address->userAddress($user['id']);
		View::displayAsHtml($result,"tpl/user/address.php");
	}
	function wishListTask() {
		import("custom.data.wishMode");
		$wish = wishMode::init();
		$user = $this->checkLogin();
		$rows = 5;
		$wish_offset = isset($_GET['wish_page'])&&!empty($_GET['wish_page'])?(int)$_GET['wish_page']-1:0;
		$result = $wish->wishList($user['id'],$rows,$wish_offset);
		//var_dump($result);
		//这直接用那个愿望单呗，不重复了吗
		//View::displayAsHtml($result,"userInfo.php");
	}
	function complaintTask(){
		$user = $this->checkLogin();
		$user_id = $user['id'];
		$page = isset($_GET['page'])&&!empty($_GET['page'])?(int)$_GET['page']:1;
		$row = 10;//每页显示条数
		$offset = ($page-1)*$row;
		$result['page']['current'] = $page;
		list($result['list'],$total) = $this->userInfo->userComplaints($user_id,$row,$offset);
		$result['page']['total'] = (int)ceil($total/$row);
		//var_dump($total);
		View::displayAsHtml($result,"tpl/user/complaint.php");
	}
	function complaintSubmitTask(){
		$user = $this->checkLogin();
		$user_id = $user['id'];
		if(isset($_GET['content'])&&!empty($_GET['content'])){
			$content = $_GET['content'];
			$this->userInfo->addComplaints($user_id,$content);
			$this->tipState('添加成功');
		}
	}
	function commentTask(){
		import("custom.data.commentMode");
		$comment = commentMode::init();
		$user = $this->checkLogin();
		$user_id = $user['id'];
		$page = isset($_GET['page'])&&!empty($_GET['page'])?(int)$_GET['page']:1;
		
		$row = 10;//每页显示条数
		$offset = ($page-1)*$row;
		$result = $comment->userComment($user_id,$row,$offset);
		$result['page']['current'] = $page;
		$result['page']['total'] = (int)ceil($result['total']/$row);
		//var_dump($result);
		View::displayAsHtml($result,"tpl/user/comment.php");
	}
	function addCommentTask(){
		import("custom.data.commentMode");
		$commentMode = commentMode::init();
		$db = sqlDB::init();
		$user = $this->checkLogin();

		$user_id  = (int)$user['id'];
		$goods_id = isset($_GET['goods_id'])&&!empty($_GET['goods_id'])?(int)$_GET['goods_id']:0;
		$order_id = isset($_GET['order_id'])&&!empty($_GET['order_id'])?(int)$_GET['order_id']:0;
		$product  = isset($_GET['product'])&&!empty($_GET['product'])?(int)$_GET['product']:0;
		$service  = isset($_GET['service'])&&!empty($_GET['service'])?(int)$_GET['service']:0;
		$comment  = isset($_GET['comment'])&&!empty($_GET['comment'])?$_GET['comment']:'系统自动评论';
		if($product<0||$product>5||$service<0||$service>5){
			return;
		}
		$sql = "SELECT `id` from `goods_in_order` where `order_id`=$order_id and `goods_id`=$goods_id";
		$goodsInOrderId = $db->getValue($sql);
		$sql = "SELECT `gopen_id` from `goods` where `id`=$goods_id";
		$gopen_id = $db->getValue($sql);
		//$gopen_id = $gopen_id_info['gopen_id'];
		if($commentMode->addCommentScore($order_id,$goodsInOrderId,$gopen_id,$user_id,$comment,$product,$service)){
			$result = array("state"=>200,"message"=>"success");
		}else{
			$result = array("state"=>500,"message"=>"请求超时，请重试");
		}
		View::displayAsJson($result);
	}
	function productTask(){
	}
	function couponTask(){
		import("custom.data.couponMode");
		$coupon = couponMode::init();
		$user = $this->checkLogin();
		$result = array();
		$result['list'] = $coupon->userCoupon($user['id']);
		//var_dump($result);
		View::displayAsHtml($result,"tpl/user/coupon.php");
	}

	function checkLogin(){
		$web=WebRouter::init();
		$user = $this->userAction->getUserInfo();
		if(empty($user)){
			$this->tipState('请登录',$web->getPage("user","login"));
			exit();
		}else{
			return $user;
		}
	}
	function tipState($message, $url=null){
		$result['message']=$message;
		$result['url']=$url;
		View::displayAsHtml($result,"state/state.php");
	}
}