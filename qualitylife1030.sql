CREATE DATABASE  IF NOT EXISTS `qualitylife` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `qualitylife`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: qualitylife
-- ------------------------------------------------------
-- Server version	5.5.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad`
--

DROP TABLE IF EXISTS `ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picurl` varchar(255) DEFAULT NULL COMMENT '广告图片',
  `url` varchar(255) DEFAULT NULL COMMENT '点击进入页面',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad`
--

LOCK TABLES `ad` WRITE;
/*!40000 ALTER TABLE `ad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `receiver` varchar(45) NOT NULL COMMENT '收件人',
  `province` varchar(45) NOT NULL COMMENT '省',
  `city` varchar(45) NOT NULL COMMENT '市',
  `district` varchar(45) NOT NULL COMMENT '县or区',
  `street` varchar(255) NOT NULL,
  `postal_code` varchar(45) DEFAULT NULL,
  `mobile_phone` varchar(45) DEFAULT NULL COMMENT '手机号',
  `fixed_phone` varchar(45) DEFAULT NULL COMMENT '固定电话',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='收货地址';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,1,'邓松高筠','江苏','徐州','泉山区','大学路','221116','18796222021',NULL),(2,1,'邓松高筠','湖南','岳阳','华容','城乡路口','414200','18796222021',NULL),(3,2,'何先婷','江苏','徐州','泉山区','大学路','221116','15150024719',NULL),(4,3,'程义情','江苏','徐州','泉山区','大学路','221116','18086794982',NULL),(5,4,'袁笛','江苏','徐州','泉山区','大学路','221116','13912034347',NULL),(6,1,'邓亚杰','湖南','华容','城乡路口','朝阳路','414200',NULL,NULL);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `tel` varchar(255) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `code` varchar(25) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '管理员类型',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute`
--

DROP TABLE IF EXISTS `attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gopen_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='属性S M L';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute`
--

LOCK TABLES `attribute` WRITE;
/*!40000 ALTER TABLE `attribute` DISABLE KEYS */;
INSERT INTO `attribute` VALUES (4,1,'S',10),(5,1,'M',10),(6,1,'L',10),(7,2,'M',10),(8,2,'L',10),(9,3,'M',20);
/*!40000 ALTER TABLE `attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gopen_id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `number` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='购物车';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1,1,4,4,1),(2,1,5,5,1),(4,2,1,1,6);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `limit` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '订单总价满足(不包括邮费)最低条件',
  `value` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '面值',
  `note` varchar(255) DEFAULT NULL COMMENT '特别说明',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型在什么情况下可用',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券/现金券(满多少减多少的)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon`
--

LOCK TABLES `coupon` WRITE;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gopen_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `selling_price` varchar(45) NOT NULL COMMENT '售价',
  `original_price` varchar(45) DEFAULT NULL COMMENT '原价',
  `category_id` int(11) DEFAULT NULL,
  `picture_ids` varchar(255) DEFAULT NULL,
  `label_ids` varchar(255) DEFAULT NULL,
  `detail` text COMMENT '商品详情',
  `stock` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `volume` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `recommended` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='商品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

LOCK TABLES `goods` WRITE;
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (1,1,'宝宝衣服1','149','199',5,'1,2','1,2,8,9','详情',100,9,0,'2014-10-30 14:10:52',0),(2,2,'宝宝衣服2','150','229',5,'3,4','2,3','详情',199,0,0,'2014-10-26 11:55:46',1),(3,3,'宝宝衣服3','99','199',5,'5,6','2,4','详情',20,0,0,'2014-10-26 11:55:46',1),(4,4,'宝宝衣服4','50','100',5,'7,8','1,3','详情',0,0,0,'2014-10-27 09:30:35',1),(5,5,'宝宝衣服5','60','200',5,'7','1','详情',0,0,0,'2014-10-27 09:30:35',1),(6,1,'宝宝衣服51','133','199',5,'1,2','1,2,8,9','详情',100,9,1,'2014-10-30 14:12:51',1);
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods_category`
--

DROP TABLE IF EXISTS `goods_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '1' COMMENT '层次12..',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='商品类别表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_category`
--

LOCK TABLES `goods_category` WRITE;
/*!40000 ALTER TABLE `goods_category` DISABLE KEYS */;
INSERT INTO `goods_category` VALUES (1,0,'有车族',1),(2,0,'美食汇',1),(3,0,'婴童屋',1),(4,0,'生活家',1),(5,3,'宝宝衣服',2),(6,3,'宝宝护理',2),(7,3,'奶粉',2),(8,3,'纸尿裤',2),(9,3,'喂哺',2),(10,3,'宝宝鞋子',2),(11,3,'宝宝玩具',2),(12,3,'寝具出行',2);
/*!40000 ALTER TABLE `goods_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods_in_order`
--

DROP TABLE IF EXISTS `goods_in_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_in_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `number` int(11) DEFAULT '1',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户未评论1、用户已评论2、卖家未回复3、卖家已回复4',
  `comment` text COMMENT '确认收货后评论',
  `comment_time` datetime DEFAULT NULL,
  `product_quality_score` decimal(8,2) NOT NULL DEFAULT '5.00' COMMENT '产品质量得分\n',
  `customer_service_score` decimal(8,2) NOT NULL DEFAULT '5.00' COMMENT '客户服务得分',
  `reply` text,
  `reply_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='商品-订单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_in_order`
--

LOCK TABLES `goods_in_order` WRITE;
/*!40000 ALTER TABLE `goods_in_order` DISABLE KEYS */;
INSERT INTO `goods_in_order` VALUES (1,1,1,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(2,1,2,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(3,1,3,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(4,2,1,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(5,2,2,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(6,2,3,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(7,3,1,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(8,3,2,1,1,NULL,NULL,5.00,5.00,NULL,NULL),(9,3,3,1,1,NULL,NULL,5.00,5.00,NULL,NULL);
/*!40000 ALTER TABLE `goods_in_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `label`
--

DROP TABLE IF EXISTS `label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='标签管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `label`
--

LOCK TABLES `label` WRITE;
/*!40000 ALTER TABLE `label` DISABLE KEYS */;
INSERT INTO `label` VALUES (1,'新品'),(2,'无条件包邮'),(3,'天天特价'),(4,'清仓'),(5,'免费试用'),(6,'每周二上新'),(7,'每周五上新');
/*!40000 ALTER TABLE `label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `goods_ids` varchar(255) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `order_amount` varchar(45) DEFAULT NULL COMMENT '订单金额',
  `payment_amount` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '付款金额(可能有修改',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '下单时间',
  `payment_time` datetime DEFAULT NULL COMMENT '付款时间',
  `freight` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `express_name` varchar(255) DEFAULT NULL,
  `express_num` varchar(255) DEFAULT NULL,
  `state` tinyint(4) NOT NULL COMMENT '未付款1、已付款2、未发货3、已发货4、交易完成5、订单取消6',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,1,'1,2,3',0,'398',398.00,'2014-10-23 11:00:00',NULL,0.00,NULL,NULL,1),(2,2,'1,2,3',0,'398',398.00,'2014-10-23 11:00:00',NULL,0.00,NULL,NULL,2),(3,3,'1,2,3',0,'398',398.00,'2014-10-23 11:00:00',NULL,0.00,NULL,NULL,3),(4,1,'1,2,3',0,'398',398.00,'2014-10-23 11:00:00',NULL,0.00,NULL,NULL,6);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gopen_id` int(11) NOT NULL,
  `brand` varchar(255) DEFAULT NULL COMMENT '品牌',
  `no` varchar(255) DEFAULT NULL COMMENT '货号',
  `listing_date` varchar(255) DEFAULT NULL COMMENT '上市年月',
  `material` varchar(255) DEFAULT NULL COMMENT '材质',
  `season` varchar(255) DEFAULT NULL COMMENT '季节',
  `color` varchar(255) DEFAULT NULL COMMENT '颜色',
  `storage` varchar(255) DEFAULT NULL COMMENT '存储方法',
  `guarantee` varchar(255) DEFAULT NULL COMMENT '保质期',
  `origin` varchar(255) DEFAULT NULL COMMENT '原产地',
  `packaging` varchar(255) DEFAULT NULL COMMENT '包装',
  `volume` varchar(255) DEFAULT NULL COMMENT '体积/容量',
  `usage` varchar(255) DEFAULT NULL COMMENT '用法',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='商品参数';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (1,1,'巴拉巴拉','BL134561','2014年秋','棉','秋冬','黑，蓝',NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'巴拉巴拉','BL134562','2014年秋','棉','秋冬','红，白',NULL,NULL,NULL,NULL,NULL,NULL),(3,3,'巴拉巴拉','BL134563','2014年东','棉','秋冬','绿，紫',NULL,NULL,NULL,NULL,NULL,NULL),(4,4,'巴拉巴拉','BL134564','2014年东','棉','秋冬','绿，紫',NULL,NULL,NULL,NULL,NULL,NULL),(5,5,'巴拉巴拉','BL134565','2014年东','棉','秋冬','绿，紫',NULL,NULL,NULL,NULL,NULL,NULL),(6,6,'cici-shop','CCS103452','2014summer','棉','夏','红，白，绿，紫',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `picture`
--

DROP TABLE IF EXISTS `picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='所有图片';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `picture`
--

LOCK TABLES `picture` WRITE;
/*!40000 ALTER TABLE `picture` DISABLE KEYS */;
INSERT INTO `picture` VALUES (1,'1.jpg'),(2,'2.jpg'),(3,'3.jpg'),(4,'4.jpg'),(5,'5.jpg'),(6,'6.jpg'),(7,'7.jpg'),(8,'8.jpg'),(9,'9.jpg'),(10,'10.jpg');
/*!40000 ALTER TABLE `picture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tel` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL COMMENT '用户可以修改用户称',
  `code` varchar(255) DEFAULT NULL COMMENT '密码',
  `point` int(11) NOT NULL DEFAULT '0' COMMENT '会员积分,默认为0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='会员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'18796222021','dsgy','1b2ef0a3599aa53e1575f3d24d824c58f2261449',0,'2014-10-23 12:24:06',1),(2,'15150024719','hxt','123456',0,'2014-10-23 01:43:02',1),(3,'18086794982','cyq','123456',0,'2014-10-23 01:43:02',1),(4,'13812034347','yd','123456',0,'2014-10-23 01:43:02',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_coupon`
--

DROP TABLE IF EXISTS `user_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_coupon` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '已使用2/已过期3/未使用1',
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户优惠券使用情况';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_coupon`
--

LOCK TABLES `user_coupon` WRITE;
/*!40000 ALTER TABLE `user_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wish_list`
--

DROP TABLE IF EXISTS `wish_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wish_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gopen_id` int(11) NOT NULL COMMENT '最新商品',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='愿望清单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wish_list`
--

LOCK TABLES `wish_list` WRITE;
/*!40000 ALTER TABLE `wish_list` DISABLE KEYS */;
INSERT INTO `wish_list` VALUES (1,1,1,'2014-10-23 02:22:31'),(2,1,2,'2014-10-23 02:22:31'),(3,1,3,'2014-10-23 02:22:31');
/*!40000 ALTER TABLE `wish_list` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-30 22:30:16
